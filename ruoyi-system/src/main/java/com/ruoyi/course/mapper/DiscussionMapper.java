package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.Discussion;

/**
 * 讨论信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface DiscussionMapper 
{
    /**
     * 查询讨论信息
     * 
     * @param id 讨论信息ID
     * @return 讨论信息
     */
    public Discussion selectDiscussionById(Long id);

    /**
     * 查询讨论信息列表
     * 
     * @param discussion 讨论信息
     * @return 讨论信息集合
     */
    public List<Discussion> selectDiscussionList(Discussion discussion);

    /**
     * 新增讨论信息
     * 
     * @param discussion 讨论信息
     * @return 结果
     */
    public int insertDiscussion(Discussion discussion);

    /**
     * 修改讨论信息
     * 
     * @param discussion 讨论信息
     * @return 结果
     */
    public int updateDiscussion(Discussion discussion);

    /**
     * 删除讨论信息
     * 
     * @param id 讨论信息ID
     * @return 结果
     */
    public int deleteDiscussionById(Long id);

    /**
     * 批量删除讨论信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDiscussionByIds(String[] ids);
}
