package com.ruoyi.course.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.course.domain.ClassInfo;

/**
 * 班级信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface ClassInfoMapper 
{
    /**
     * 查询班级信息
     * 
     * @param id 班级信息ID
     * @return 班级信息
     */
    public ClassInfo selectClassInfoById(Long id);

    /**
     * 查询班级信息列表
     * 
     * @param classInfo 班级信息
     * @return 班级信息集合
     */
    public List<ClassInfo> selectClassInfoList(ClassInfo classInfo);

    /**
     * 新增班级信息
     * 
     * @param classInfo 班级信息
     * @return 结果
     */
    public int insertClassInfo(ClassInfo classInfo);

    /**
     * 修改班级信息
     * 
     * @param classInfo 班级信息
     * @return 结果
     */
    public int updateClassInfo(ClassInfo classInfo);

    /**
     * 删除班级信息
     * 
     * @param id 班级信息ID
     * @return 结果
     */
    public int deleteClassInfoById(Long id);

    /**
     * 批量删除班级信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteClassInfoByIds(String[] ids);
    /**
     * 检查班级名称是否是唯一的
     * @param name 班级名称
     * @return
     */
	public int checkClassNameUnique(@Param("name")String name);
    /**
     * 修改班级的状态....
     * @param classInfo 班级的信息......
     * @return
     */
	public int updateClassStatus(ClassInfo classInfo);
    /**
     * 查询所有已用的班级的信息列表
     * @return
     */
	public List<Map<String, Object>> queryAllUsedClass();
}
