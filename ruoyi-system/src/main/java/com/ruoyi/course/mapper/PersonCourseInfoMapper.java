package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.PersonCourseInfo;

/**
 * 个人课程信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface PersonCourseInfoMapper 
{
    /**
     * 查询个人课程信息
     * 
     * @param id 个人课程信息ID
     * @return 个人课程信息
     */
    public PersonCourseInfo selectPersonCourseInfoById(Long id);

    /**
     * 查询个人课程信息列表
     * 
     * @param personCourseInfo 个人课程信息
     * @return 个人课程信息集合
     */
    public List<PersonCourseInfo> selectPersonCourseInfoList(PersonCourseInfo personCourseInfo);

    /**
     * 新增个人课程信息
     * 
     * @param personCourseInfo 个人课程信息
     * @return 结果
     */
    public int insertPersonCourseInfo(PersonCourseInfo personCourseInfo);

    /**
     * 修改个人课程信息
     * 
     * @param personCourseInfo 个人课程信息
     * @return 结果
     */
    public int updatePersonCourseInfo(PersonCourseInfo personCourseInfo);

    /**
     * 删除个人课程信息
     * 
     * @param id 个人课程信息ID
     * @return 结果
     */
    public int deletePersonCourseInfoById(Long id);

    /**
     * 批量删除个人课程信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePersonCourseInfoByIds(String[] ids);
}
