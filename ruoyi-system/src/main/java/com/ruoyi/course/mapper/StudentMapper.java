package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.Student;

/**
 * 学员信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface StudentMapper 
{
    /**
     * 查询学员信息
     * 
     * @param id 学员信息ID
     * @return 学员信息
     */
    public Student selectStudentById(Long id);

    /**
     * 查询学员信息列表
     * 
     * @param student 学员信息
     * @return 学员信息集合
     */
    public List<Student> selectStudentList(Student student);

    /**
     * 新增学员信息
     * 
     * @param student 学员信息
     * @return 结果
     */
    public int insertStudent(Student student);

    /**
     * 修改学员信息
     * 
     * @param student 学员信息
     * @return 结果
     */
    public int updateStudent(Student student);

    /**
     * 删除学员信息
     * 
     * @param id 学员信息ID
     * @return 结果
     */
    public int deleteStudentById(Long id);

    /**
     * 批量删除学员信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStudentByIds(String[] ids);
    /**
     * 改变学员的状态
     * @param student
     * @return
     */
	public int changeCourseStatus(Student student);
    /**
     * 修改学生的密码
     * @param student
     * @return
     */
	public int resetPwd(Student student);
    /**
     * 查询学生账户等信息
     * @param student
     * @return
     */
	public Student queryStudentByPassWord(Student student);

}
