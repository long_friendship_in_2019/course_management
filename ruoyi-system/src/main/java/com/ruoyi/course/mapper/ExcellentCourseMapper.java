package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.ExcellentCourse;

/**
 * 精品课程信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface ExcellentCourseMapper 
{
    /**
     * 查询精品课程信息
     * 
     * @param id 精品课程信息ID
     * @return 精品课程信息
     */
    public ExcellentCourse selectExcellentCourseById(Long id);

    /**
     * 查询精品课程信息列表
     * 
     * @param excellentCourse 精品课程信息
     * @return 精品课程信息集合
     */
    public List<ExcellentCourse> selectExcellentCourseList(ExcellentCourse excellentCourse);

    /**
     * 新增精品课程信息
     * 
     * @param excellentCourse 精品课程信息
     * @return 结果
     */
    public int insertExcellentCourse(ExcellentCourse excellentCourse);

    /**
     * 修改精品课程信息
     * 
     * @param excellentCourse 精品课程信息
     * @return 结果
     */
    public int updateExcellentCourse(ExcellentCourse excellentCourse);

    /**
     * 删除精品课程信息
     * 
     * @param id 精品课程信息ID
     * @return 结果
     */
    public int deleteExcellentCourseById(Long id);

    /**
     * 批量删除精品课程信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteExcellentCourseByIds(String[] ids);
    /**
     * 
     * @param course
     * @return
     */
	public List<ExcellentCourse> selectExcellentCourseListFront(ExcellentCourse course);
}
