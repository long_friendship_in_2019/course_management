package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.Message;

/**
 * 学员留言信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface MessageMapper 
{
    /**
     * 查询学员留言信息
     * 
     * @param id 学员留言信息ID
     * @return 学员留言信息
     */
    public Message selectMessageById(Long id);

    /**
     * 查询学员留言信息列表
     * 
     * @param message 学员留言信息
     * @return 学员留言信息集合
     */
    public List<Message> selectMessageList(Message message);

    /**
     * 新增学员留言信息
     * 
     * @param message 学员留言信息
     * @return 结果
     */
    public int insertMessage(Message message);

    /**
     * 修改学员留言信息
     * 
     * @param message 学员留言信息
     * @return 结果
     */
    public int updateMessage(Message message);

    /**
     * 删除学员留言信息
     * 
     * @param id 学员留言信息ID
     * @return 结果
     */
    public int deleteMessageById(Long id);

    /**
     * 批量删除学员留言信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMessageByIds(String[] ids);
    /**
     * 改变留言的状态
     * @param message
     * @return
     */
	public int updateMessageStatus(Message message);
    /**
     * 
     * @param message
     * @return
     */
	public List<Message> selectMessageListFront(Message message);
}
