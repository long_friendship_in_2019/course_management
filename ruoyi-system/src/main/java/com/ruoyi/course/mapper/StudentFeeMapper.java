package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.StudentFee;

/**
 * 学员缴费信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface StudentFeeMapper 
{
    /**
     * 查询学员缴费信息
     * 
     * @param id 学员缴费信息ID
     * @return 学员缴费信息
     */
    public StudentFee selectStudentFeeById(Long id);

    /**
     * 查询学员缴费信息列表
     * 
     * @param studentFee 学员缴费信息
     * @return 学员缴费信息集合
     */
    public List<StudentFee> selectStudentFeeList(StudentFee studentFee);

    /**
     * 新增学员缴费信息
     * 
     * @param studentFee 学员缴费信息
     * @return 结果
     */
    public int insertStudentFee(StudentFee studentFee);

    /**
     * 修改学员缴费信息
     * 
     * @param studentFee 学员缴费信息
     * @return 结果
     */
    public int updateStudentFee(StudentFee studentFee);

    /**
     * 删除学员缴费信息
     * 
     * @param id 学员缴费信息ID
     * @return 结果
     */
    public int deleteStudentFeeById(Long id);

    /**
     * 批量删除学员缴费信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStudentFeeByIds(String[] ids);
}
