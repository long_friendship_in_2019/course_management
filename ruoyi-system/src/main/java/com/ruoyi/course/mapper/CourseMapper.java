package com.ruoyi.course.mapper;

import java.util.List;

import com.ruoyi.course.domain.ClassInfo;
import com.ruoyi.course.domain.Course;

/**
 * 课程信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface CourseMapper {
	/**
	 * 查询课程信息
	 * 
	 * @param id
	 *            课程信息ID
	 * @return 课程信息
	 */
	public Course selectCourseById(Long id);

	/**
	 * 查询课程信息列表
	 * 
	 * @param course
	 *            课程信息
	 * @return 课程信息集合
	 */
	public List<Course> selectCourseList(Course course);

	/**
	 * 新增课程信息
	 * 
	 * @param course
	 *            课程信息
	 * @return 结果
	 */
	public int insertCourse(Course course);

	/**
	 * 修改课程信息
	 * 
	 * @param course
	 *            课程信息
	 * @return 结果
	 */
	public int updateCourse(Course course);

	/**
	 * 删除课程信息
	 * 
	 * @param id
	 *            课程信息ID
	 * @return 结果
	 */
	public int deleteCourseById(Long id);

	/**
	 * 批量删除课程信息
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteCourseByIds(String[] ids);

	/**
	 * 修改课程的信息
	 * 
	 * @param course
	 * @return
	 */
	public int changeCourseStatus(Course course);

	/**
	 * 查询 这个班级可以选择的课程列表
	 * 
	 * @param classInfo
	 * @return
	 */
	public List<Course> queryAllSelectCourseInfo(ClassInfo classInfo);

	/**
	 * 根据编号查询所有的课程信息
	 * 
	 * @param ids
	 * @return
	 */
	public List<Course> queryCoursesByIds(String[] ids);

	/**
	 * 前台课程的列表
	 * 
	 * @param course
	 * @return
	 */
	public List<Course> selectCourseListFront(Course course);
}
