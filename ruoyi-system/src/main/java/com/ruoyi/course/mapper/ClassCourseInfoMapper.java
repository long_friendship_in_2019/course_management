package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.ClassCourseInfo;

/**
 * 班级课程信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface ClassCourseInfoMapper 
{
    /**
     * 查询班级课程信息
     * 
     * @param id 班级课程信息ID
     * @return 班级课程信息
     */
    public ClassCourseInfo selectClassCourseInfoById(Long id);

    /**
     * 查询班级课程信息列表
     * 
     * @param classCourseInfo 班级课程信息
     * @return 班级课程信息集合
     */
    public List<ClassCourseInfo> selectClassCourseInfoList(ClassCourseInfo classCourseInfo);

    /**
     * 新增班级课程信息
     * 
     * @param classCourseInfo 班级课程信息
     * @return 结果
     */
    public int insertClassCourseInfo(ClassCourseInfo classCourseInfo);

    /**
     * 修改班级课程信息
     * 
     * @param classCourseInfo 班级课程信息
     * @return 结果
     */
    public int updateClassCourseInfo(ClassCourseInfo classCourseInfo);

    /**
     * 删除班级课程信息
     * 
     * @param id 班级课程信息ID
     * @return 结果
     */
    public int deleteClassCourseInfoById(Long id);

    /**
     * 批量删除班级课程信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteClassCourseInfoByIds(String[] ids);
    /**
     * 批量插入班级选课信息表
     * @param classCourseInfos
     * @return
     */
	public int insertAllClassCourseInfos(List<ClassCourseInfo> classCourseInfos);
}
