package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.TeacherStyle;

/**
 * 师生风采信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface TeacherStyleMapper 
{
    /**
     * 查询师生风采信息
     * 
     * @param id 师生风采信息ID
     * @return 师生风采信息
     */
    public TeacherStyle selectTeacherStyleById(Long id);

    /**
     * 查询师生风采信息列表
     * 
     * @param teacherStyle 师生风采信息
     * @return 师生风采信息集合
     */
    public List<TeacherStyle> selectTeacherStyleList(TeacherStyle teacherStyle);

    /**
     * 新增师生风采信息
     * 
     * @param teacherStyle 师生风采信息
     * @return 结果
     */
    public int insertTeacherStyle(TeacherStyle teacherStyle);

    /**
     * 修改师生风采信息
     * 
     * @param teacherStyle 师生风采信息
     * @return 结果
     */
    public int updateTeacherStyle(TeacherStyle teacherStyle);

    /**
     * 删除师生风采信息
     * 
     * @param id 师生风采信息ID
     * @return 结果
     */
    public int deleteTeacherStyleById(Long id);

    /**
     * 批量删除师生风采信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTeacherStyleByIds(String[] ids);
   /**
    * 师生风采的列表页面展示
    * @return
    */
	public List<TeacherStyle> selectTeacherStyleFrontList();
}
