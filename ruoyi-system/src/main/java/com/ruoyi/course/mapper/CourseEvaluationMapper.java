package com.ruoyi.course.mapper;

import java.util.List;
import com.ruoyi.course.domain.CourseEvaluation;

/**
 * 课程评价信息Mapper接口
 * 
 * @author keqiang
 * @date 2020-04-21
 */
public interface CourseEvaluationMapper {
	/**
	 * 查询课程评价信息
	 * 
	 * @param id
	 *            课程评价信息ID
	 * @return 课程评价信息
	 */
	public CourseEvaluation selectCourseEvaluationById(Long id);

	/**
	 * 查询课程评价信息列表
	 * 
	 * @param courseEvaluation
	 *            课程评价信息
	 * @return 课程评价信息集合
	 */
	public List<CourseEvaluation> selectCourseEvaluationList(CourseEvaluation courseEvaluation);

	/**
	 * 新增课程评价信息
	 * 
	 * @param courseEvaluation
	 *            课程评价信息
	 * @return 结果
	 */
	public int insertCourseEvaluation(CourseEvaluation courseEvaluation);

	/**
	 * 修改课程评价信息
	 * 
	 * @param courseEvaluation
	 *            课程评价信息
	 * @return 结果
	 */
	public int updateCourseEvaluation(CourseEvaluation courseEvaluation);

	/**
	 * 删除课程评价信息
	 * 
	 * @param id
	 *            课程评价信息ID
	 * @return 结果
	 */
	public int deleteCourseEvaluationById(Long id);

	/**
	 * 批量删除课程评价信息
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteCourseEvaluationByIds(String[] ids);
}
