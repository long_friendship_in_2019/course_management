package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 课程信息对象 wct_course
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class Course extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    @Excel(name = "课程编号")
    private Long id;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String name;

    /** 课程介绍 */
    @Excel(name = "课程介绍")
    private String introduce;

    /** 课程图片 */   
    private String picture;

    /** 课程分类编号 */    
    private Long typeId;

    /** 课程分类名称 */
    @Excel(name = "课程分类名称")
    private String typeName;

    /** 课程价格 */
    @Excel(name = "课程价格")
    private Long price;

    /** 课程状态 */
    @Excel(name = "课程状态",readConverterExp = "1=正常,2=停课,3=失效")
    private Integer status;

    /** 发布时间 */
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 课程主讲老师编号 */  
    private Long teacherId;

    /** 课程主讲老师 */
    @Excel(name = "课程主讲老师")
    private String teacherName;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 修改人 */   
    private String updateUser;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIntroduce(String introduce) 
    {
        this.introduce = introduce;
    }

    public String getIntroduce() 
    {
        return introduce;
    }
    public void setPicture(String picture) 
    {
        this.picture = picture;
    }

    public String getPicture() 
    {
        return picture;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }
    public void setTypeName(String typeName) 
    {
        this.typeName = typeName;
    }

    public String getTypeName() 
    {
        return typeName;
    }
    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setTeacherId(Long teacherId) 
    {
        this.teacherId = teacherId;
    }

    public Long getTeacherId() 
    {
        return teacherId;
    }
    public void setTeacherName(String teacherName) 
    {
        this.teacherName = teacherName;
    }

    public String getTeacherName() 
    {
        return teacherName;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }
    public void setUpdateUser(String updateUser) 
    {
        this.updateUser = updateUser;
    }

    public String getUpdateUser() 
    {
        return updateUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("introduce", getIntroduce())
            .append("picture", getPicture())
            .append("typeId", getTypeId())
            .append("typeName", getTypeName())
            .append("price", getPrice())
            .append("status", getStatus())
            .append("publishTime", getPublishTime())
            .append("teacherId", getTeacherId())
            .append("teacherName", getTeacherName())
            .append("createUser", getCreateUser())
            .append("createTime", getCreateTime())
            .append("updateUser", getUpdateUser())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
