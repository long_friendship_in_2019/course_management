package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 个人课程信息对象 wct_person_course_info
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class PersonCourseInfo extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 编号 */
	private Long id;

	/** 学生编号 */
	@Excel(name = "学生编号")
	private Long studentCode;

	/** 学生名称 */
	@Excel(name = "学生名称")
	private String studentName;

	/** 课程编号 */
	@Excel(name = "课程编号")
	private Long courseId;

	/** 课程名称 */
	@Excel(name = "课程名称")
	private String courseName;

	/** 课程状态 */
	@Excel(name = "课程状态")
	private Integer status;

	/** 报名时间 */
	@Excel(name = "报名时间", width = 30, dateFormat = "yyyy-MM-dd")
	private Date applyTime;

	@Excel(name = "课程类型", readConverterExp = "1=精品课程,2=普通课程")
	private Integer type;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setStudentCode(Long studentCode) {
		this.studentCode = studentCode;
	}

	public Long getStudentCode() {
		return studentCode;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	public Date getApplyTime() {
		return applyTime;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("studentCode", getStudentCode()).append("studentName", getStudentName())
				.append("courseId", getCourseId()).append("courseName", getCourseName()).append("status", getStatus())
				.append("applyTime", getApplyTime()).toString();
	}
}
