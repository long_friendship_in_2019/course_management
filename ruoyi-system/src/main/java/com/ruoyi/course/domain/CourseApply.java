package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 课程报名号信息对象 wct_course_apply
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class CourseApply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 学员编号 */
    @Excel(name = "学员编号")
    private Long studentId;

    /** 学员姓名 */
    @Excel(name = "学员姓名")
    private String studentName;

    /** 学员电话 */
    @Excel(name = "学员电话")
    private String phone;

    /** 班级编号 */
    @Excel(name = "班级编号")
    private Long classId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 课程编号 */
    @Excel(name = "课程编号")
    private Long courseId;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 报名的价格 */
    @Excel(name = "报名的价格")
    private Long price;

    /** 预计上课时间 */
    @Excel(name = "预计上课时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date courseTime;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentId(Long studentId) 
    {
        this.studentId = studentId;
    }

    public Long getStudentId() 
    {
        return studentId;
    }
    public void setStudentName(String studentName) 
    {
        this.studentName = studentName;
    }

    public String getStudentName() 
    {
        return studentName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setClassId(Long classId) 
    {
        this.classId = classId;
    }

    public Long getClassId() 
    {
        return classId;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setCourseId(Long courseId) 
    {
        this.courseId = courseId;
    }

    public Long getCourseId() 
    {
        return courseId;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setCourseTime(Date courseTime) 
    {
        this.courseTime = courseTime;
    }

    public Date getCourseTime() 
    {
        return courseTime;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentId", getStudentId())
            .append("studentName", getStudentName())
            .append("phone", getPhone())
            .append("classId", getClassId())
            .append("className", getClassName())
            .append("courseId", getCourseId())
            .append("courseName", getCourseName())
            .append("price", getPrice())
            .append("createTime", getCreateTime())
            .append("courseTime", getCourseTime())
            .append("status", getStatus())
            .toString();
    }
}
