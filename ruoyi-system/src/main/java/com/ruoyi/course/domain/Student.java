package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 学员信息对象 wct_student
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class Student extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 账号 */
    @Excel(name = "账号")
    private String account;

    /** 密码 */
    private String password;

    /** 邮件 */
    @Excel(name = "邮件")
    private String email;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** QQ号 */
    @Excel(name = "QQ号")
    private String qq;

    /** 微信号 */
    @Excel(name = "微信号")
    private String wechat;

    /** 性别 */
    @Excel(name = "性别")
    private Integer sex;

    /** 头像 */
    @Excel(name = "头像")
    private String img;

    /** 生日 */
    @Excel(name = "生日")
    private String birthday;

    /** 身份证号 */
    private String idcard;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 积分 */
    @Excel(name = "积分")
    private Long score;

    /** 账户余额 */
    @Excel(name = "账户余额")
    private Long money;

    /** 班级编号 */
    @Excel(name = "班级编号")
    private Long classId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 状态(1正常,2冻结,3.失效,0删除) */
    @Excel(name = "状态(1正常,2冻结,3.失效,0删除)")
    private Integer status;

    /** 注册时间 */
    @Excel(name = "注册时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registerTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setAccount(String account) 
    {
        this.account = account;
    }

    public String getAccount() 
    {
        return account;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setQq(String qq) 
    {
        this.qq = qq;
    }

    public String getQq() 
    {
        return qq;
    }
    public void setWechat(String wechat) 
    {
        this.wechat = wechat;
    }

    public String getWechat() 
    {
        return wechat;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }
    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }
    public void setBirthday(String birthday) 
    {
        this.birthday = birthday;
    }

    public String getBirthday() 
    {
        return birthday;
    }
    public void setIdcard(String idcard) 
    {
        this.idcard = idcard;
    }

    public String getIdcard() 
    {
        return idcard;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setScore(Long score) 
    {
        this.score = score;
    }

    public Long getScore() 
    {
        return score;
    }
    public void setMoney(Long money) 
    {
        this.money = money;
    }

    public Long getMoney() 
    {
        return money;
    }
    public void setClassId(Long classId) 
    {
        this.classId = classId;
    }

    public Long getClassId() 
    {
        return classId;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setRegisterTime(Date registerTime) 
    {
        this.registerTime = registerTime;
    }

    public Date getRegisterTime() 
    {
        return registerTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("account", getAccount())
            .append("password", getPassword())
            .append("email", getEmail())
            .append("phone", getPhone())
            .append("qq", getQq())
            .append("wechat", getWechat())
            .append("sex", getSex())
            .append("img", getImg())
            .append("birthday", getBirthday())
            .append("idcard", getIdcard())
            .append("address", getAddress())
            .append("score", getScore())
            .append("money", getMoney())
            .append("classId", getClassId())
            .append("className", getClassName())
            .append("status", getStatus())
            .append("registerTime", getRegisterTime())
            .toString();
    }
}
