package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 精品课程信息对象 wct_excellent_course
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class ExcellentCourse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 精品课程名称 */
    @Excel(name = "精品课程名称")
    private String courseName;

    /** 课程图片 */
    @Excel(name = "课程图片")
    private String picture;

    /** 课程视频 */
    private String video;

    /** 课程简介 */
    @Excel(name = "课程简介")
    private String introduce;

    /** 课程状态 */
    @Excel(name = "课程状态")
    private Integer status;

    /** 课程价格 */
    @Excel(name = "课程价格")
    private Long price;

    /** 课程老师 */
    @Excel(name = "课程老师")
    private String teacherName;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setPicture(String picture) 
    {
        this.picture = picture;
    }

    public String getPicture() 
    {
        return picture;
    }
    public void setVideo(String video) 
    {
        this.video = video;
    }

    public String getVideo() 
    {
        return video;
    }
    public void setIntroduce(String introduce) 
    {
        this.introduce = introduce;
    }

    public String getIntroduce() 
    {
        return introduce;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setTeacherName(String teacherName) 
    {
        this.teacherName = teacherName;
    }

    public String getTeacherName() 
    {
        return teacherName;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("courseName", getCourseName())
            .append("picture", getPicture())
            .append("video", getVideo())
            .append("introduce", getIntroduce())
            .append("status", getStatus())
            .append("price", getPrice())
            .append("teacherName", getTeacherName())
            .append("createUser", getCreateUser())
            .append("createTime", getCreateTime())
            .toString();
    }
}
