package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 课程评价信息对象 wct_course_evaluation
 * 
 * @author keqiang
 * @date 2020-04-21
 */
public class CourseEvaluation extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 编号 */
	@Excel(name = "编号")
	private Long id;

	/** 课程编号 */
	private Long courseCode;

	/** 课程名称 */
	@Excel(name = "课程名称")
	private String courseName;

	/** 授课教师 */
	@Excel(name = "授课教师")
	private String teacherName;

	/** 评价分数 */
	@Excel(name = "评价分数")
	private Long score;

	/** 学员编号 */
	private Long studentId;

	/** 学员姓名 */
	@Excel(name = "学员姓名")
	private String studentName;

	/** 评价时间 */
	@Excel(name = "评价时间", width = 30, dateFormat = "yyyy-MM-dd")
	private Date evaluationTime;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setCourseCode(Long courseCode) {
		this.courseCode = courseCode;
	}

	public Long getCourseCode() {
		return courseCode;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Long getScore() {
		return score;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setEvaluationTime(Date evaluationTime) {
		this.evaluationTime = evaluationTime;
	}

	public Date getEvaluationTime() {
		return evaluationTime;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("courseCode", getCourseCode()).append("courseName", getCourseName())
				.append("teacherName", getTeacherName()).append("score", getScore()).append("studentId", getStudentId())
				.append("studentName", getStudentName()).append("evaluationTime", getEvaluationTime()).toString();
	}
}
