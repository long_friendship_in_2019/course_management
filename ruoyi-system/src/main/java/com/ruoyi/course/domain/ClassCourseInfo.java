package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 班级课程信息对象 wct_class_course_info
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class ClassCourseInfo extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 编号 */
	private Long id;

	/** 班级编号 */
	@Excel(name = "班级编号")
	private Long classId;

	/** 班级名称 */
	@Excel(name = "班级名称")
	private String className;

	/** 课程编号 */
	@Excel(name = "课程编号")
	private Long courseId;

	/** 课程名称 */
	@Excel(name = "课程名称")
	private String courseName;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassName() {
		return className;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseName() {
		return courseName;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("classId", getClassId()).append("className", getClassName()).append("courseId", getCourseId())
				.append("courseName", getCourseName()).append("createTime", getCreateTime()).toString();
	}

	public ClassCourseInfo(Long id, Long classId, String className, Long courseId, String courseName) {
		super();
		this.id = id;
		this.classId = classId;
		this.className = className;
		this.courseId = courseId;
		this.courseName = courseName;
	}

	public ClassCourseInfo(Long classId, String className, Long courseId, String courseName) {
		super();
		this.classId = classId;
		this.className = className;
		this.courseId = courseId;
		this.courseName = courseName;
	}

	public ClassCourseInfo() {
		super();
	}
	
	
}
