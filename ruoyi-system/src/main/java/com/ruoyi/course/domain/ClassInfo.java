package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 班级信息对象 wct_class_info
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class ClassInfo extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 编号 */
	@Excel(name = "班级编号")
	private Long id;

	/** 班级名称 */
	@Excel(name = "班级名称")
	private String name;

	/** 班级描述 */
	@Excel(name = "班级描述")
	private String introduce;

	/** 辅导员编号 */
	private Long teacherId;

	/** 班级辅导员老师 */
	@Excel(name = "班级辅导员老师")
	private String teacherName;

	/** 班级状态 */
	@Excel(name = "班级状态", readConverterExp = "1=正常,2=毕业,3=停班")
	private Integer status;

	/** 班级开课程信息 */
	@Excel(name = "班级开课程信息")
	private String courseNames;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public String getCourseNames() {
		return courseNames;
	}

	public void setCourseNames(String courseNames) {
		this.courseNames = courseNames;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId()).append("name", getName())
				.append("introduce", getIntroduce()).append("teacherId", getTeacherId())
				.append("teacherName", getTeacherName()).append("createTime", getCreateTime())
				.append("status", getStatus()).toString();
	}
}
