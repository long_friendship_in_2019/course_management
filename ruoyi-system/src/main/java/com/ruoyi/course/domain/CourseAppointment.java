package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 课程预约信息对象 wct_course_appointment
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class CourseAppointment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 课程编号 */
    @Excel(name = "课程编号")
    private Long courseId;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 学员编号 */
    @Excel(name = "学员编号")
    private Long studentId;

    /** 学员名称 */
    @Excel(name = "学员名称")
    private String studentName;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 预约上课时间 */
    @Excel(name = "预约上课时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date courseTime;

    /** 实际上课时间 */
    @Excel(name = "实际上课时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date actualCourseTime;

    /** 上课状态 */
    @Excel(name = "上课状态")
    private Integer courseStatus;

    /** 上课老师 */
    @Excel(name = "上课老师")
    private String teacherName;

    /** 上课地点 */
    @Excel(name = "上课地点")
    private String address;
    
    @Excel(name = "课程类型",readConverterExp = "1=精品课程,2=普通课程")	
	private Integer type;
    
    

    public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCourseId(Long courseId) 
    {
        this.courseId = courseId;
    }

    public Long getCourseId() 
    {
        return courseId;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setStudentId(Long studentId) 
    {
        this.studentId = studentId;
    }

    public Long getStudentId() 
    {
        return studentId;
    }
    public void setStudentName(String studentName) 
    {
        this.studentName = studentName;
    }

    public String getStudentName() 
    {
        return studentName;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setCourseTime(Date courseTime) 
    {
        this.courseTime = courseTime;
    }

    public Date getCourseTime() 
    {
        return courseTime;
    }
    public void setActualCourseTime(Date actualCourseTime) 
    {
        this.actualCourseTime = actualCourseTime;
    }

    public Date getActualCourseTime() 
    {
        return actualCourseTime;
    }
    public void setCourseStatus(Integer courseStatus) 
    {
        this.courseStatus = courseStatus;
    }

    public Integer getCourseStatus() 
    {
        return courseStatus;
    }
    public void setTeacherName(String teacherName) 
    {
        this.teacherName = teacherName;
    }

    public String getTeacherName() 
    {
        return teacherName;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("courseId", getCourseId())
            .append("courseName", getCourseName())
            .append("studentId", getStudentId())
            .append("studentName", getStudentName())
            .append("status", getStatus())
            .append("courseTime", getCourseTime())
            .append("actualCourseTime", getActualCourseTime())
            .append("courseStatus", getCourseStatus())
            .append("teacherName", getTeacherName())
            .append("address", getAddress())
            .append("createTime", getCreateTime())
            .toString();
    }
}
