package com.ruoyi.course.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 学员留言信息对象 wct_message
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public class Message extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 留言信内容 */
    @Excel(name = "留言信内容")
    private String content;

    /** 学员编号 */
    @Excel(name = "学员编号")
    private Long studentId;

    /** 学员姓名 */
    @Excel(name = "学员姓名")
    private String studentName;

    /** 留言对象 */
    @Excel(name = "留言对象")
    private String messageObject;

    /** 留言时间 */
    @Excel(name = "留言时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date messageTime;

    /** 留言回复信息 */
    @Excel(name = "留言回复信息")
    private String backContent;

    /** 回复留言老师 */
    @Excel(name = "回复留言老师")
    private String teacherName;

    /** 回复时间 */
    @Excel(name = "回复时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date backTime;

    /** 留言状态 */
    @Excel(name = "留言状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setStudentId(Long studentId) 
    {
        this.studentId = studentId;
    }

    public Long getStudentId() 
    {
        return studentId;
    }
    public void setStudentName(String studentName) 
    {
        this.studentName = studentName;
    }

    public String getStudentName() 
    {
        return studentName;
    }
    public void setMessageObject(String messageObject) 
    {
        this.messageObject = messageObject;
    }

    public String getMessageObject() 
    {
        return messageObject;
    }
    public void setMessageTime(Date messageTime) 
    {
        this.messageTime = messageTime;
    }

    public Date getMessageTime() 
    {
        return messageTime;
    }
    public void setBackContent(String backContent) 
    {
        this.backContent = backContent;
    }

    public String getBackContent() 
    {
        return backContent;
    }
    public void setTeacherName(String teacherName) 
    {
        this.teacherName = teacherName;
    }

    public String getTeacherName() 
    {
        return teacherName;
    }
    public void setBackTime(Date backTime) 
    {
        this.backTime = backTime;
    }

    public Date getBackTime() 
    {
        return backTime;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("content", getContent())
            .append("studentId", getStudentId())
            .append("studentName", getStudentName())
            .append("messageObject", getMessageObject())
            .append("messageTime", getMessageTime())
            .append("backContent", getBackContent())
            .append("teacherName", getTeacherName())
            .append("backTime", getBackTime())
            .append("status", getStatus())
            .toString();
    }
}
