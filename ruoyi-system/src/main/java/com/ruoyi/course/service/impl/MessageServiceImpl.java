package com.ruoyi.course.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.MessageMapper;
import com.ruoyi.course.domain.Message;
import com.ruoyi.course.service.IMessageService;
import com.ruoyi.common.core.text.Convert;

/**
 * 学员留言信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class MessageServiceImpl implements IMessageService {
	@Autowired
	private MessageMapper messageMapper;

	/**
	 * 查询学员留言信息
	 * 
	 * @param id
	 *            学员留言信息ID
	 * @return 学员留言信息
	 */
	@Override
	public Message selectMessageById(Long id) {
		return messageMapper.selectMessageById(id);
	}

	/**
	 * 查询学员留言信息列表
	 * 
	 * @param message
	 *            学员留言信息
	 * @return 学员留言信息
	 */
	@Override
	public List<Message> selectMessageList(Message message) {
		return messageMapper.selectMessageList(message);
	}

	/**
	 * 新增学员留言信息
	 * 
	 * @param message
	 *            学员留言信息
	 * @return 结果
	 */
	@Override
	public int insertMessage(Message message) {
		return messageMapper.insertMessage(message);
	}

	/**
	 * 修改学员留言信息
	 * 
	 * @param message
	 *            学员留言信息
	 * @return 结果
	 */
	@Override
	public int updateMessage(Message message) {
		return messageMapper.updateMessage(message);
	}

	/**
	 * 删除学员留言信息对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteMessageByIds(String ids) {
		return messageMapper.deleteMessageByIds(Convert.toStrArray(ids));
	}

	/**
	 * 删除学员留言信息信息
	 * 
	 * @param id
	 *            学员留言信息ID
	 * @return 结果
	 */
	@Override
	public int deleteMessageById(Long id) {
		return messageMapper.deleteMessageById(id);
	}

	@Override
	public int updateMessageStatus(Message message) {
		
		return messageMapper.updateMessageStatus(message);
	}

	@Override
	public List<Message> selectMessageListFront(Message message) {
		
		return messageMapper.selectMessageListFront(message);
	}
}
