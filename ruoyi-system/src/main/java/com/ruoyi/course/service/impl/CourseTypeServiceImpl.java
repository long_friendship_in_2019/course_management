package com.ruoyi.course.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.CourseTypeMapper;
import com.ruoyi.course.domain.CourseType;
import com.ruoyi.course.service.ICourseTypeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 课程类别信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class CourseTypeServiceImpl implements ICourseTypeService {
	@Autowired
	private CourseTypeMapper courseTypeMapper;

	/**
	 * 查询课程类别信息
	 * 
	 * @param id
	 *            课程类别信息ID
	 * @return 课程类别信息
	 */
	@Override
	public CourseType selectCourseTypeById(Long id) {
		return courseTypeMapper.selectCourseTypeById(id);
	}

	/**
	 * 查询课程类别信息列表
	 * 
	 * @param courseType
	 *            课程类别信息
	 * @return 课程类别信息
	 */
	@Override
	public List<CourseType> selectCourseTypeList(CourseType courseType) {
		return courseTypeMapper.selectCourseTypeList(courseType);
	}

	/**
	 * 新增课程类别信息 
	 * @param courseType  课程类别信息
	 * @return 结果
	 */
	@Override
	public int insertCourseType(CourseType courseType) {
		courseType.setCreateTime(DateUtils.getNowDate());
		return courseTypeMapper.insertCourseType(courseType);
	}

	/**
	 * 修改课程类别信息
	 * @param courseType    课程类别信息
	 * @return 结果
	 */
	@Override
	public int updateCourseType(CourseType courseType) {
		courseType.setUpdateTime(DateUtils.getNowDate());
		return courseTypeMapper.updateCourseType(courseType);
	}

	/**
	 * 删除课程类别信息对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteCourseTypeByIds(String ids) {
		return courseTypeMapper.deleteCourseTypeByIds(Convert.toStrArray(ids));
	}

	/**
	 * 删除课程类别信息信息
	 * 
	 * @param id
	 *            课程类别信息ID
	 * @return 结果
	 */
	@Override
	public int deleteCourseTypeById(Long id) {
		return courseTypeMapper.deleteCourseTypeById(id);
	}
}
