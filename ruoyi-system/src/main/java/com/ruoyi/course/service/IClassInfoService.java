package com.ruoyi.course.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.course.domain.ClassCourseInfo;
import com.ruoyi.course.domain.ClassInfo;

/**
 * 班级信息Service接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface IClassInfoService {
	
	
	/**
     * 查询班级课程信息
     * 
     * @param id 班级课程信息ID
     * @return 班级课程信息
     */
    public ClassCourseInfo selectClassCourseInfoById(Long id);

    /**
     * 查询班级课程信息列表
     * 
     * @param classCourseInfo 班级课程信息
     * @return 班级课程信息集合
     */
    public List<ClassCourseInfo> selectClassCourseInfoList(ClassCourseInfo classCourseInfo);

    /**
     * 新增班级课程信息
     * 
     * @param classCourseInfo 班级课程信息
     * @return 结果
     */
    public int insertClassCourseInfo(ClassCourseInfo classCourseInfo);

    /**
     * 修改班级课程信息
     * 
     * @param classCourseInfo 班级课程信息
     * @return 结果
     */
    public int updateClassCourseInfo(ClassCourseInfo classCourseInfo);

    /**
     * 批量删除班级课程信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteClassCourseInfoByIds(String ids);

    /**
     * 删除班级课程信息信息
     * 
     * @param id 班级课程信息ID
     * @return 结果
     */
    public int deleteClassCourseInfoById(Long id);
    
	
	/**
	 * 查询班级信息
	 * 
	 * @param id 班级信息ID
	 * @return 班级信息
	 */
	public ClassInfo selectClassInfoById(Long id);

	/**
	 * 查询班级信息列表
	 * 
	 * @param classInfo 班级信息
	 * @return 班级信息集合
	 */
	public List<ClassInfo> selectClassInfoList(ClassInfo classInfo);

	/**
	 * 新增班级信息	 
	 * @param classInfo  班级信息
	 * @return 结果
	 */
	public int insertClassInfo(ClassInfo classInfo);

	/**
	 * 修改班级信息
	 * 
	 * @param classInfo  班级信息
	 * @return 结果
	 */
	public int updateClassInfo(ClassInfo classInfo);

	/**
	 * 批量删除班级信息
	 * 
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteClassInfoByIds(String ids);

	/**
	 * 删除班级信息信息
	 * 
	 * @param id 班级信息ID
	 * @return 结果
	 */
	public int deleteClassInfoById(Long id);

	/**
	 * 根据岗位名称查询下面老师的列表
	 * @param jobName
	 * @return
	 */
	public List<Map<String, Object>> queryAllTeacherInfo(String jobName);
    /**
     * 校验班级名称是否重复
     * @param name
     * @return
     */
	public String checkClassNameUnique(String name);
    
	/**
	 * 修改班级的状态......毕业,停班等信息......
	 * @param classInfo
	 * @return
	 */
	public int updateClassStatus(ClassInfo classInfo);
    /**
     * 插入班级课表
     * @param classCourseInfos
     * @return
     */
	public int insertAllClassCourseInfos(List<ClassCourseInfo> classCourseInfos);
    /**
     * 查询所有的班级的列表信息
     * @return
     */
	public List<Map<String, Object>> queryAllUsedClass();
}
