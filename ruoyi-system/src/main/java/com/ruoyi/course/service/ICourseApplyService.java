package com.ruoyi.course.service;

import java.util.List;
import com.ruoyi.course.domain.CourseApply;

/**
 * 课程报名号信息Service接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface ICourseApplyService 
{
    /**
     * 查询课程报名号信息
     * 
     * @param id 课程报名号信息ID
     * @return 课程报名号信息
     */
    public CourseApply selectCourseApplyById(Long id);

    /**
     * 查询课程报名号信息列表
     * 
     * @param courseApply 课程报名号信息
     * @return 课程报名号信息集合
     */
    public List<CourseApply> selectCourseApplyList(CourseApply courseApply);

    /**
     * 新增课程报名号信息
     * 
     * @param courseApply 课程报名号信息
     * @return 结果
     */
    public int insertCourseApply(CourseApply courseApply);

    /**
     * 修改课程报名号信息
     * 
     * @param courseApply 课程报名号信息
     * @return 结果
     */
    public int updateCourseApply(CourseApply courseApply);

    /**
     * 批量删除课程报名号信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCourseApplyByIds(String ids);

    /**
     * 删除课程报名号信息信息
     * 
     * @param id 课程报名号信息ID
     * @return 结果
     */
    public int deleteCourseApplyById(Long id);
}
