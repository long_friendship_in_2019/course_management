package com.ruoyi.course.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.CourseApplyMapper;
import com.ruoyi.course.domain.CourseApply;
import com.ruoyi.course.service.ICourseApplyService;
import com.ruoyi.common.core.text.Convert;

/**
 * 课程报名号信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class CourseApplyServiceImpl implements ICourseApplyService 
{
    @Autowired
    private CourseApplyMapper courseApplyMapper;

    /**
     * 查询课程报名号信息
     * 
     * @param id 课程报名号信息ID
     * @return 课程报名号信息
     */
    @Override
    public CourseApply selectCourseApplyById(Long id)
    {
        return courseApplyMapper.selectCourseApplyById(id);
    }

    /**
     * 查询课程报名号信息列表
     * 
     * @param courseApply 课程报名号信息
     * @return 课程报名号信息
     */
    @Override
    public List<CourseApply> selectCourseApplyList(CourseApply courseApply)
    {
        return courseApplyMapper.selectCourseApplyList(courseApply);
    }

    /**
     * 新增课程报名号信息
     * 
     * @param courseApply 课程报名号信息
     * @return 结果
     */
    @Override
    public int insertCourseApply(CourseApply courseApply)
    {
        courseApply.setCreateTime(DateUtils.getNowDate());
        return courseApplyMapper.insertCourseApply(courseApply);
    }

    /**
     * 修改课程报名号信息
     * 
     * @param courseApply 课程报名号信息
     * @return 结果
     */
    @Override
    public int updateCourseApply(CourseApply courseApply)
    {
        return courseApplyMapper.updateCourseApply(courseApply);
    }

    /**
     * 删除课程报名号信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCourseApplyByIds(String ids)
    {
        return courseApplyMapper.deleteCourseApplyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除课程报名号信息信息
     * 
     * @param id 课程报名号信息ID
     * @return 结果
     */
    @Override
    public int deleteCourseApplyById(Long id)
    {
        return courseApplyMapper.deleteCourseApplyById(id);
    }
}
