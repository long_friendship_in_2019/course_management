package com.ruoyi.course.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.StudentFeeMapper;
import com.ruoyi.course.domain.StudentFee;
import com.ruoyi.course.service.IStudentFeeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 学员缴费信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class StudentFeeServiceImpl implements IStudentFeeService 
{
    @Autowired
    private StudentFeeMapper studentFeeMapper;

    /**
     * 查询学员缴费信息
     * 
     * @param id 学员缴费信息ID
     * @return 学员缴费信息
     */
    @Override
    public StudentFee selectStudentFeeById(Long id)
    {
        return studentFeeMapper.selectStudentFeeById(id);
    }

    /**
     * 查询学员缴费信息列表
     * 
     * @param studentFee 学员缴费信息
     * @return 学员缴费信息
     */
    @Override
    public List<StudentFee> selectStudentFeeList(StudentFee studentFee)
    {
        return studentFeeMapper.selectStudentFeeList(studentFee);
    }

    /**
     * 新增学员缴费信息
     * 
     * @param studentFee 学员缴费信息
     * @return 结果
     */
    @Override
    public int insertStudentFee(StudentFee studentFee)
    {
        return studentFeeMapper.insertStudentFee(studentFee);
    }

    /**
     * 修改学员缴费信息
     * 
     * @param studentFee 学员缴费信息
     * @return 结果
     */
    @Override
    public int updateStudentFee(StudentFee studentFee)
    {
        return studentFeeMapper.updateStudentFee(studentFee);
    }

    /**
     * 删除学员缴费信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStudentFeeByIds(String ids)
    {
        return studentFeeMapper.deleteStudentFeeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学员缴费信息信息
     * 
     * @param id 学员缴费信息ID
     * @return 结果
     */
    @Override
    public int deleteStudentFeeById(Long id)
    {
        return studentFeeMapper.deleteStudentFeeById(id);
    }
}
