package com.ruoyi.course.service;

import java.util.List;
import com.ruoyi.course.domain.CourseType;

/**
 * 课程类别信息Service接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface ICourseTypeService 
{
    /**
     * 查询课程类别信息
     * 
     * @param id 课程类别信息ID
     * @return 课程类别信息
     */
    public CourseType selectCourseTypeById(Long id);

    /**
     * 查询课程类别信息列表
     * 
     * @param courseType 课程类别信息
     * @return 课程类别信息集合
     */
    public List<CourseType> selectCourseTypeList(CourseType courseType);

    /**
     * 新增课程类别信息
     * 
     * @param courseType 课程类别信息
     * @return 结果
     */
    public int insertCourseType(CourseType courseType);

    /**
     * 修改课程类别信息
     * 
     * @param courseType 课程类别信息
     * @return 结果
     */
    public int updateCourseType(CourseType courseType);

    /**
     * 批量删除课程类别信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCourseTypeByIds(String ids);

    /**
     * 删除课程类别信息信息
     * 
     * @param id 课程类别信息ID
     * @return 结果
     */
    public int deleteCourseTypeById(Long id);
}
