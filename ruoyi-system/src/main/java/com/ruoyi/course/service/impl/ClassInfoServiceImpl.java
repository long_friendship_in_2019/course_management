package com.ruoyi.course.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.course.mapper.ClassCourseInfoMapper;
import com.ruoyi.course.mapper.ClassInfoMapper;
import com.ruoyi.course.domain.ClassCourseInfo;
import com.ruoyi.course.domain.ClassInfo;
import com.ruoyi.course.service.IClassInfoService;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.text.Convert;

/**
 * 班级信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class ClassInfoServiceImpl implements IClassInfoService {
	@Autowired
	private ClassInfoMapper classInfoMapper;
	
	@Autowired
	private SysUserMapper userMapper;
	
	@Autowired
	private ClassCourseInfoMapper classCourseMapper;

	/**
	 * 查询班级信息
	 * 
	 * @param id
	 *            班级信息ID
	 * @return 班级信息
	 */
	@Override
	public ClassInfo selectClassInfoById(Long id) {
		return classInfoMapper.selectClassInfoById(id);
	}

	/**
	 * 查询班级信息列表
	 * 
	 * @param classInfo
	 *            班级信息
	 * @return 班级信息
	 */
	@Override
	public List<ClassInfo> selectClassInfoList(ClassInfo classInfo) {
		return classInfoMapper.selectClassInfoList(classInfo);
	}

	/**
	 * 新增班级信息
	 * 
	 * @param classInfo
	 *            班级信息
	 * @return 结果
	 */
	@Override
	public int insertClassInfo(ClassInfo classInfo) {
		classInfo.setCreateTime(DateUtils.getNowDate());
		return classInfoMapper.insertClassInfo(classInfo);
	}

	/**
	 * 修改班级信息
	 * 
	 * @param classInfo
	 *            班级信息
	 * @return 结果
	 */
	@Override
	public int updateClassInfo(ClassInfo classInfo) {
		return classInfoMapper.updateClassInfo(classInfo);
	}

	/**
	 * 删除班级信息对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteClassInfoByIds(String ids) {
		return classInfoMapper.deleteClassInfoByIds(Convert.toStrArray(ids));
	}

	/**
	 * 删除班级信息信息
	 * 
	 * @param id
	 *            班级信息ID
	 * @return 结果
	 */
	@Override
	public int deleteClassInfoById(Long id) {
		return classInfoMapper.deleteClassInfoById(id);
	}
	
	@Override
	public List<Map<String, Object>> queryAllTeacherInfo(String jobName) {
		
		return userMapper.queryInstructorTeacher(jobName);
	}

	@Override
	public String checkClassNameUnique(String name) {
		int count=classInfoMapper.checkClassNameUnique(name);
		return count>0?UserConstants.USER_NAME_NOT_UNIQUE:UserConstants.USER_NAME_UNIQUE;
	}

	@Override
	public int updateClassStatus(ClassInfo classInfo) {
		
		return classInfoMapper.updateClassStatus(classInfo);
	}

	@Override
	public int insertAllClassCourseInfos(List<ClassCourseInfo> classCourseInfos) {
		
		return classCourseMapper.insertAllClassCourseInfos(classCourseInfos);
	}

	@Override
	public ClassCourseInfo selectClassCourseInfoById(Long id) {
		
		return classCourseMapper.selectClassCourseInfoById(id);
	}

	@Override
	public List<ClassCourseInfo> selectClassCourseInfoList(ClassCourseInfo classCourseInfo) {
	
		return classCourseMapper.selectClassCourseInfoList(classCourseInfo);
	}

	@Override
	public int insertClassCourseInfo(ClassCourseInfo classCourseInfo) {
		
		return classCourseMapper.insertClassCourseInfo(classCourseInfo);
	}

	@Override
	public int updateClassCourseInfo(ClassCourseInfo classCourseInfo) {		
		return classCourseMapper.updateClassCourseInfo(classCourseInfo);
	}

	@Override
	public int deleteClassCourseInfoByIds(String ids) {		
		return classCourseMapper.deleteClassCourseInfoByIds(Convert.toStrArray(ids));
	}

	@Override
	public int deleteClassCourseInfoById(Long id) {		
		return classCourseMapper.deleteClassCourseInfoById(id);
	}

	@Override
	public List<Map<String, Object>> queryAllUsedClass() {		
		return classInfoMapper.queryAllUsedClass();
	}
}
