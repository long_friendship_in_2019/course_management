package com.ruoyi.course.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.PersonCourseInfoMapper;
import com.ruoyi.course.domain.PersonCourseInfo;
import com.ruoyi.course.service.IPersonCourseInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 个人课程信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class PersonCourseInfoServiceImpl implements IPersonCourseInfoService 
{
    @Autowired
    private PersonCourseInfoMapper personCourseInfoMapper;

    /**
     * 查询个人课程信息
     * 
     * @param id 个人课程信息ID
     * @return 个人课程信息
     */
    @Override
    public PersonCourseInfo selectPersonCourseInfoById(Long id)
    {
        return personCourseInfoMapper.selectPersonCourseInfoById(id);
    }

    /**
     * 查询个人课程信息列表
     * 
     * @param personCourseInfo 个人课程信息
     * @return 个人课程信息
     */
    @Override
    public List<PersonCourseInfo> selectPersonCourseInfoList(PersonCourseInfo personCourseInfo)
    {
        return personCourseInfoMapper.selectPersonCourseInfoList(personCourseInfo);
    }

    /**
     * 新增个人课程信息
     * 
     * @param personCourseInfo 个人课程信息
     * @return 结果
     */
    @Override
    public int insertPersonCourseInfo(PersonCourseInfo personCourseInfo)
    {
        return personCourseInfoMapper.insertPersonCourseInfo(personCourseInfo);
    }

    /**
     * 修改个人课程信息
     * 
     * @param personCourseInfo 个人课程信息
     * @return 结果
     */
    @Override
    public int updatePersonCourseInfo(PersonCourseInfo personCourseInfo)
    {
        return personCourseInfoMapper.updatePersonCourseInfo(personCourseInfo);
    }

    /**
     * 删除个人课程信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePersonCourseInfoByIds(String ids)
    {
        return personCourseInfoMapper.deletePersonCourseInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除个人课程信息信息
     * 
     * @param id 个人课程信息ID
     * @return 结果
     */
    @Override
    public int deletePersonCourseInfoById(Long id)
    {
        return personCourseInfoMapper.deletePersonCourseInfoById(id);
    }
}
