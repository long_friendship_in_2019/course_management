package com.ruoyi.course.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.TeacherStyleMapper;
import com.ruoyi.course.domain.TeacherStyle;
import com.ruoyi.course.service.ITeacherStyleService;
import com.ruoyi.common.core.text.Convert;

/**
 * 师生风采信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class TeacherStyleServiceImpl implements ITeacherStyleService {
	@Autowired
	private TeacherStyleMapper teacherStyleMapper;

	/**
	 * 查询师生风采信息
	 * 
	 * @param id
	 *            师生风采信息ID
	 * @return 师生风采信息
	 */
	@Override
	public TeacherStyle selectTeacherStyleById(Long id) {
		return teacherStyleMapper.selectTeacherStyleById(id);
	}

	/**
	 * 查询师生风采信息列表
	 * 
	 * @param teacherStyle
	 *            师生风采信息
	 * @return 师生风采信息
	 */
	@Override
	public List<TeacherStyle> selectTeacherStyleList(TeacherStyle teacherStyle) {
		return teacherStyleMapper.selectTeacherStyleList(teacherStyle);
	}

	/**
	 * 新增师生风采信息
	 * 
	 * @param teacherStyle
	 *            师生风采信息
	 * @return 结果
	 */
	@Override
	public int insertTeacherStyle(TeacherStyle teacherStyle) {
		teacherStyle.setCreateTime(DateUtils.getNowDate());
		return teacherStyleMapper.insertTeacherStyle(teacherStyle);
	}

	/**
	 * 修改师生风采信息
	 * 
	 * @param teacherStyle
	 *            师生风采信息
	 * @return 结果
	 */
	@Override
	public int updateTeacherStyle(TeacherStyle teacherStyle) {
		teacherStyle.setUpdateTime(DateUtils.getNowDate());
		return teacherStyleMapper.updateTeacherStyle(teacherStyle);
	}

	/**
	 * 删除师生风采信息对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteTeacherStyleByIds(String ids) {
		return teacherStyleMapper.deleteTeacherStyleByIds(Convert.toStrArray(ids));
	}

	/**
	 * 删除师生风采信息信息
	 * 
	 * @param id
	 *            师生风采信息ID
	 * @return 结果
	 */
	@Override
	public int deleteTeacherStyleById(Long id) {
		return teacherStyleMapper.deleteTeacherStyleById(id);
	}

	@Override
	public List<TeacherStyle> selectTeacherStyleFrontList() {
		
		return teacherStyleMapper.selectTeacherStyleFrontList();
	}
}
