package com.ruoyi.course.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.ExcellentCourseMapper;
import com.ruoyi.course.domain.ExcellentCourse;
import com.ruoyi.course.service.IExcellentCourseService;
import com.ruoyi.common.core.text.Convert;

/**
 * 精品课程信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class ExcellentCourseServiceImpl implements IExcellentCourseService 
{
    @Autowired
    private ExcellentCourseMapper excellentCourseMapper;

    /**
     * 查询精品课程信息
     * 
     * @param id 精品课程信息ID
     * @return 精品课程信息
     */
    @Override
    public ExcellentCourse selectExcellentCourseById(Long id)
    {
        return excellentCourseMapper.selectExcellentCourseById(id);
    }

    /**
     * 查询精品课程信息列表
     * 
     * @param excellentCourse 精品课程信息
     * @return 精品课程信息
     */
    @Override
    public List<ExcellentCourse> selectExcellentCourseList(ExcellentCourse excellentCourse)
    {
        return excellentCourseMapper.selectExcellentCourseList(excellentCourse);
    }

    /**
     * 新增精品课程信息
     * 
     * @param excellentCourse 精品课程信息
     * @return 结果
     */
    @Override
    public int insertExcellentCourse(ExcellentCourse excellentCourse)
    {
        excellentCourse.setCreateTime(DateUtils.getNowDate());
        return excellentCourseMapper.insertExcellentCourse(excellentCourse);
    }

    /**
     * 修改精品课程信息
     * 
     * @param excellentCourse 精品课程信息
     * @return 结果
     */
    @Override
    public int updateExcellentCourse(ExcellentCourse excellentCourse)
    {
        return excellentCourseMapper.updateExcellentCourse(excellentCourse);
    }

    /**
     * 删除精品课程信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteExcellentCourseByIds(String ids)
    {
        return excellentCourseMapper.deleteExcellentCourseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除精品课程信息信息
     * 
     * @param id 精品课程信息ID
     * @return 结果
     */
    @Override
    public int deleteExcellentCourseById(Long id)
    {
        return excellentCourseMapper.deleteExcellentCourseById(id);
    }

	@Override
	public List<ExcellentCourse> selectExcellentCourseListFront(ExcellentCourse course) {


		return excellentCourseMapper.selectExcellentCourseListFront(course);
	}
}
