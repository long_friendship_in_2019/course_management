package com.ruoyi.course.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.StudentMapper;
import com.ruoyi.course.domain.Student;
import com.ruoyi.course.service.IStudentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 学员信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class StudentServiceImpl implements IStudentService {
	@Autowired
	private StudentMapper studentMapper;

	/**
	 * 查询学员信息
	 * 
	 * @param id
	 *            学员信息ID
	 * @return 学员信息
	 */
	@Override
	public Student selectStudentById(Long id) {
		return studentMapper.selectStudentById(id);
	}

	/**
	 * 查询学员信息列表
	 * 
	 * @param student
	 *            学员信息
	 * @return 学员信息
	 */
	@Override
	public List<Student> selectStudentList(Student student) {
		return studentMapper.selectStudentList(student);
	}

	/**
	 * 新增学员信息
	 * 
	 * @param student
	 *            学员信息
	 * @return 结果
	 */
	@Override
	public int insertStudent(Student student) {
		return studentMapper.insertStudent(student);
	}

	/**
	 * 修改学员信息
	 * 
	 * @param student
	 *            学员信息
	 * @return 结果
	 */
	@Override
	public int updateStudent(Student student) {
		return studentMapper.updateStudent(student);
	}

	/**
	 * 删除学员信息对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteStudentByIds(String ids) {
		return studentMapper.deleteStudentByIds(Convert.toStrArray(ids));
	}

	/**
	 * 删除学员信息信息
	 * 
	 * @param id
	 *            学员信息ID
	 * @return 结果
	 */
	@Override
	public int deleteStudentById(Long id) {
		return studentMapper.deleteStudentById(id);
	}

	@Override
	public int changeCourseStatus(Student student) {		
		return studentMapper.changeCourseStatus(student);
	}

	@Override
	public int resetPwd(Student student) {
		return studentMapper.resetPwd(student);
	}

	@Override
	public Student queryStudentByPassWord(Student student) {
	
		return studentMapper.queryStudentByPassWord(student);
	}
}
