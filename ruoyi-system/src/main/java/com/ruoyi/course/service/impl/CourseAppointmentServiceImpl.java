package com.ruoyi.course.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.CourseAppointmentMapper;
import com.ruoyi.course.domain.CourseAppointment;
import com.ruoyi.course.service.ICourseAppointmentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 课程预约信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class CourseAppointmentServiceImpl implements ICourseAppointmentService 
{
    @Autowired
    private CourseAppointmentMapper courseAppointmentMapper;

    /**
     * 查询课程预约信息
     * 
     * @param id 课程预约信息ID
     * @return 课程预约信息
     */
    @Override
    public CourseAppointment selectCourseAppointmentById(Long id)
    {
        return courseAppointmentMapper.selectCourseAppointmentById(id);
    }

    /**
     * 查询课程预约信息列表
     * 
     * @param courseAppointment 课程预约信息
     * @return 课程预约信息
     */
    @Override
    public List<CourseAppointment> selectCourseAppointmentList(CourseAppointment courseAppointment)
    {
        return courseAppointmentMapper.selectCourseAppointmentList(courseAppointment);
    }

    /**
     * 新增课程预约信息
     * 
     * @param courseAppointment 课程预约信息
     * @return 结果
     */
    @Override
    public int insertCourseAppointment(CourseAppointment courseAppointment)
    {
        courseAppointment.setCreateTime(DateUtils.getNowDate());
        return courseAppointmentMapper.insertCourseAppointment(courseAppointment);
    }

    /**
     * 修改课程预约信息
     * 
     * @param courseAppointment 课程预约信息
     * @return 结果
     */
    @Override
    public int updateCourseAppointment(CourseAppointment courseAppointment)
    {
        return courseAppointmentMapper.updateCourseAppointment(courseAppointment);
    }

    /**
     * 删除课程预约信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCourseAppointmentByIds(String ids)
    {
        return courseAppointmentMapper.deleteCourseAppointmentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除课程预约信息信息
     * 
     * @param id 课程预约信息ID
     * @return 结果
     */
    @Override
    public int deleteCourseAppointmentById(Long id)
    {
        return courseAppointmentMapper.deleteCourseAppointmentById(id);
    }
}
