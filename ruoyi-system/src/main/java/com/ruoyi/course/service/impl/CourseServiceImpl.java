package com.ruoyi.course.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.CourseMapper;
import com.ruoyi.course.mapper.CourseTypeMapper;
import com.ruoyi.course.domain.ClassInfo;
import com.ruoyi.course.domain.Course;
import com.ruoyi.course.service.ICourseService;
import com.ruoyi.common.core.text.Convert;

/**
 * 课程信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class CourseServiceImpl implements ICourseService {
	@Autowired
	private CourseMapper courseMapper;
	
	@Autowired
	private CourseTypeMapper courseTypeMapper;

	/**
	 * 查询课程信息
	 * 
	 * @param id
	 *            课程信息ID
	 * @return 课程信息
	 */
	@Override
	public Course selectCourseById(Long id) {
		return courseMapper.selectCourseById(id);
	}

	/**
	 * 查询课程信息列表
	 * 
	 * @param course
	 *            课程信息
	 * @return 课程信息
	 */
	@Override
	public List<Course> selectCourseList(Course course) {
		return courseMapper.selectCourseList(course);
	}

	/**
	 * 新增课程信息
	 * 
	 * @param course
	 *            课程信息
	 * @return 结果
	 */
	@Override
	public int insertCourse(Course course) {
		course.setCreateTime(DateUtils.getNowDate());
		return courseMapper.insertCourse(course);
	}

	/**
	 * 修改课程信息
	 * 
	 * @param course
	 *            课程信息
	 * @return 结果
	 */
	@Override
	public int updateCourse(Course course) {
		course.setUpdateTime(DateUtils.getNowDate());
		return courseMapper.updateCourse(course);
	}

	/**
	 * 删除课程信息对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteCourseByIds(String ids) {
		return courseMapper.deleteCourseByIds(Convert.toStrArray(ids));
	}

	/**
	 * 删除课程信息信息
	 * 
	 * @param id
	 *            课程信息ID
	 * @return 结果
	 */
	@Override
	public int deleteCourseById(Long id) {
		return courseMapper.deleteCourseById(id);
	}

	@Override
	public int changeCourseStatus(Course course) {
		
		return courseMapper.changeCourseStatus(course);
	}

	@Override
	public List<Map<String, Object>> querAllCourseTypes() {
		
		return courseTypeMapper.querAllCourseTypes();
	}
    
	
	@Override
	public List<Course> selectClassCourseList(ClassInfo classInfo) {
		
		return courseMapper.queryAllSelectCourseInfo(classInfo);
	}

	@Override
	public List<Course> queryCoursesByIds(String[] ids) {
		
		return courseMapper.queryCoursesByIds(ids);
	}

	@Override
	public List<Course> selectCourseListFront(Course course) {
		
		return courseMapper.selectCourseListFront(course);
	}
}
