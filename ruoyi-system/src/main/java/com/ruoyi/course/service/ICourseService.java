package com.ruoyi.course.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.course.domain.ClassInfo;
import com.ruoyi.course.domain.Course;

/**
 * 课程信息Service接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface ICourseService 
{
    /**
     * 查询课程信息
     * 
     * @param id 课程信息ID
     * @return 课程信息
     */
    public Course selectCourseById(Long id);

    /**
     * 查询课程信息列表
     * 
     * @param course 课程信息
     * @return 课程信息集合
     */
    public List<Course> selectCourseList(Course course);

    /**
     * 新增课程信息
     * 
     * @param course 课程信息
     * @return 结果
     */
    public int insertCourse(Course course);

    /**
     * 修改课程信息
     * 
     * @param course 课程信息
     * @return 结果
     */
    public int updateCourse(Course course);

    /**
     * 批量删除课程信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCourseByIds(String ids);

    /**
     * 删除课程信息信息
     * 
     * @param id 课程信息ID
     * @return 结果
     */
    public int deleteCourseById(Long id);
    /**
     * 改变课程的状态  停课,失效等信息
     * @param course
     * @return
     */
	public int changeCourseStatus(Course course);
    /**
     * 查询所有的课程的类型的信息列表
     * @return
     */
	public List<Map<String, Object>> querAllCourseTypes();
    /**
     * 查询班级选择课程的列表信息....
     * @param classInfo
     * @return
     */
	public List<Course> selectClassCourseList(ClassInfo classInfo);
    /**
     * 通过课程编号查询所有的课程的信息
     * @param ids 编号数组
     * @return
     */
	public List<Course> queryCoursesByIds(String[] ids);
    /**
     * 
     * @param course
     * @return
     */
	public List<Course> selectCourseListFront(Course course);
}
