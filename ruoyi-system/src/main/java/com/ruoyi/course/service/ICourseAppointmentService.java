package com.ruoyi.course.service;

import java.util.List;
import com.ruoyi.course.domain.CourseAppointment;

/**
 * 课程预约信息Service接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface ICourseAppointmentService 
{
    /**
     * 查询课程预约信息
     * 
     * @param id 课程预约信息ID
     * @return 课程预约信息
     */
    public CourseAppointment selectCourseAppointmentById(Long id);

    /**
     * 查询课程预约信息列表
     * 
     * @param courseAppointment 课程预约信息
     * @return 课程预约信息集合
     */
    public List<CourseAppointment> selectCourseAppointmentList(CourseAppointment courseAppointment);

    /**
     * 新增课程预约信息
     * 
     * @param courseAppointment 课程预约信息
     * @return 结果
     */
    public int insertCourseAppointment(CourseAppointment courseAppointment);

    /**
     * 修改课程预约信息
     * 
     * @param courseAppointment 课程预约信息
     * @return 结果
     */
    public int updateCourseAppointment(CourseAppointment courseAppointment);

    /**
     * 批量删除课程预约信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCourseAppointmentByIds(String ids);

    /**
     * 删除课程预约信息信息
     * 
     * @param id 课程预约信息ID
     * @return 结果
     */
    public int deleteCourseAppointmentById(Long id);
}
