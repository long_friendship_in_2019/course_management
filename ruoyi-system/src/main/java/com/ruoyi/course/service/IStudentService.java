package com.ruoyi.course.service;

import java.util.List;
import com.ruoyi.course.domain.Student;

/**
 * 学员信息Service接口
 * 
 * @author keqiang
 * @date 2020-04-19
 */
public interface IStudentService 
{
    /**
     * 查询学员信息
     * 
     * @param id 学员信息ID
     * @return 学员信息
     */
    public Student selectStudentById(Long id);

    /**
     * 查询学员信息列表
     * 
     * @param student 学员信息
     * @return 学员信息集合
     */
    public List<Student> selectStudentList(Student student);

    /**
     * 新增学员信息
     * 
     * @param student 学员信息
     * @return 结果
     */
    public int insertStudent(Student student);

    /**
     * 修改学员信息
     * 
     * @param student 学员信息
     * @return 结果
     */
    public int updateStudent(Student student);

    /**
     * 批量删除学员信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStudentByIds(String ids);

    /**
     * 删除学员信息信息
     * 
     * @param id 学员信息ID
     * @return 结果
     */
    public int deleteStudentById(Long id);
    /**
     * 修改学生的状态
     * @param student
     * @return
     */
	public int changeCourseStatus(Student student);
    /**
     * 重置学生密码,密码改为123456
     * @param student
     * @return
     */
	public int resetPwd(Student student);
    /**
     * 通过账户登陆学生个人的信息
     * @param student
     * @return
     */
	public Student queryStudentByPassWord(Student student);
}
