package com.ruoyi.course.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.DiscussionMapper;
import com.ruoyi.course.domain.Discussion;
import com.ruoyi.course.service.IDiscussionService;
import com.ruoyi.common.core.text.Convert;

/**
 * 讨论信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class DiscussionServiceImpl implements IDiscussionService {
	@Autowired
	private DiscussionMapper discussionMapper;

	/**
	 * 查询讨论信息
	 * 
	 * @param id
	 *            讨论信息ID
	 * @return 讨论信息
	 */
	@Override
	public Discussion selectDiscussionById(Long id) {
		return discussionMapper.selectDiscussionById(id);
	}

	/**
	 * 查询讨论信息列表
	 * 
	 * @param discussion
	 *            讨论信息
	 * @return 讨论信息
	 */
	@Override
	public List<Discussion> selectDiscussionList(Discussion discussion) {
		return discussionMapper.selectDiscussionList(discussion);
	}

	/**
	 * 新增讨论信息
	 * 
	 * @param discussion
	 *            讨论信息
	 * @return 结果
	 */
	@Override
	public int insertDiscussion(Discussion discussion) {
		discussion.setCreateTime(DateUtils.getNowDate());
		return discussionMapper.insertDiscussion(discussion);
	}

	/**
	 * 修改讨论信息
	 * 
	 * @param discussion
	 *            讨论信息
	 * @return 结果
	 */
	@Override
	public int updateDiscussion(Discussion discussion) {
		return discussionMapper.updateDiscussion(discussion);
	}

	/**
	 * 删除讨论信息对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteDiscussionByIds(String ids) {
		return discussionMapper.deleteDiscussionByIds(Convert.toStrArray(ids));
	}

	/**
	 * 删除讨论信息信息
	 * 
	 * @param id
	 *            讨论信息ID
	 * @return 结果
	 */
	@Override
	public int deleteDiscussionById(Long id) {
		return discussionMapper.deleteDiscussionById(id);
	}
}
