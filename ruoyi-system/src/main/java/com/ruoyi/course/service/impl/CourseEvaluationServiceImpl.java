package com.ruoyi.course.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.course.mapper.CourseEvaluationMapper;
import com.ruoyi.course.domain.CourseEvaluation;
import com.ruoyi.course.service.ICourseEvaluationService;
import com.ruoyi.common.core.text.Convert;

/**
 * 课程评价信息Service业务层处理
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Service
public class CourseEvaluationServiceImpl implements ICourseEvaluationService 
{
    @Autowired
    private CourseEvaluationMapper courseEvaluationMapper;

    /**
     * 查询课程评价信息
     * 
     * @param id 课程评价信息ID
     * @return 课程评价信息
     */
    @Override
    public CourseEvaluation selectCourseEvaluationById(Long id)
    {
        return courseEvaluationMapper.selectCourseEvaluationById(id);
    }

    /**
     * 查询课程评价信息列表
     * 
     * @param courseEvaluation 课程评价信息
     * @return 课程评价信息
     */
    @Override
    public List<CourseEvaluation> selectCourseEvaluationList(CourseEvaluation courseEvaluation)
    {
        return courseEvaluationMapper.selectCourseEvaluationList(courseEvaluation);
    }

    /**
     * 新增课程评价信息
     * 
     * @param courseEvaluation 课程评价信息
     * @return 结果
     */
    @Override
    public int insertCourseEvaluation(CourseEvaluation courseEvaluation)
    {
        return courseEvaluationMapper.insertCourseEvaluation(courseEvaluation);
    }

    /**
     * 修改课程评价信息
     * 
     * @param courseEvaluation 课程评价信息
     * @return 结果
     */
    @Override
    public int updateCourseEvaluation(CourseEvaluation courseEvaluation)
    {
        return courseEvaluationMapper.updateCourseEvaluation(courseEvaluation);
    }

    /**
     * 删除课程评价信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCourseEvaluationByIds(String ids)
    {
        return courseEvaluationMapper.deleteCourseEvaluationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除课程评价信息信息
     * 
     * @param id 课程评价信息ID
     * @return 结果
     */
    @Override
    public int deleteCourseEvaluationById(Long id)
    {
        return courseEvaluationMapper.deleteCourseEvaluationById(id);
    }
}
