package com.ruoyi.web.controller.course;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.ClassCourseInfo;
import com.ruoyi.course.domain.ClassInfo;
import com.ruoyi.course.domain.Course;
import com.ruoyi.course.service.IClassInfoService;
import com.ruoyi.course.service.ICourseService;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 班级信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/classInfo")
public class ClassInfoController extends BaseController {
	private String prefix = "course/classInfo";

	@Autowired
	private IClassInfoService classInfoService;
	
	@Autowired
	private ICourseService  courseService;

	
	@RequiresPermissions("course:classInfo:view")
	@GetMapping()
	public String classInfo() {
		return prefix + "/classInfo";
	}

	/**
	 * 查询班级信息列表
	 */
	@RequiresPermissions("course:classInfo:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(ClassInfo classInfo) {
		startPage();
		List<ClassInfo> list = classInfoService.selectClassInfoList(classInfo);
		return getDataTable(list);
	}

	/**
	 * 导出班级信息列表
	 */
	@RequiresPermissions("course:classInfo:export")
	@Log(title = "班级信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(ClassInfo classInfo) {
		List<ClassInfo> list = classInfoService.selectClassInfoList(classInfo);
		ExcelUtil<ClassInfo> util = new ExcelUtil<ClassInfo>(ClassInfo.class);
		return util.exportExcel(list, "classInfo");
	}

	/**
	 * 新增班级信息
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap) {
		String jobName="辅导员";
		List<Map<String, Object>> teachers=classInfoService.queryAllTeacherInfo(jobName);
		mmap.put("teachers", teachers);
		return prefix + "/add";
	}
    
	/**
	 * 校验班级名称是否唯一用户名
	 */
	@PostMapping("/checkClassNameUnique")
	@ResponseBody
	public String checkClassNameUnique(ClassInfo classInfo) {
		
		return classInfoService.checkClassNameUnique(classInfo.getName());
	}

	
	/**
	 * 新增保存班级信息
	 */
	@RequiresPermissions("course:classInfo:add")
	@Log(title = "班级信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(ClassInfo classInfo) {
		//默认新增的班级是开班状态
		classInfo.setStatus(1);
		return toAjax(classInfoService.insertClassInfo(classInfo));
	}

	/**
	 * 修改班级信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		ClassInfo classInfo = classInfoService.selectClassInfoById(id);
		String jobName="辅导员";
		List<Map<String, Object>> teachers=classInfoService.queryAllTeacherInfo(jobName);
		mmap.put("teachers", teachers);
		mmap.put("classInfo", classInfo);
		return prefix + "/edit";
	}

	/**
	 * 修改保存班级信息
	 */
	@RequiresPermissions("course:classInfo:edit")
	@Log(title = "班级信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(ClassInfo classInfo) {
		return toAjax(classInfoService.updateClassInfo(classInfo));
	}

	/**
	 * 删除班级信息修改毕业
	 */
	@RequiresPermissions("course:classInfo:remove")
	@Log(title = "班级信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(classInfoService.deleteClassInfoByIds(ids));
	}
	
	
	/**
	 * 改变班级的状态
	 * @param classInfo 班级的信息表
	 * @return
	 */
	@RequiresPermissions("course:classInfo:remove")
	@Log(title = "班级信息", businessType = BusinessType.DELETE)
	@PostMapping("/changeStatus")
	@ResponseBody
	public  AjaxResult changeStatus(ClassInfo classInfo){		
		return toAjax(classInfoService.updateClassStatus(classInfo));
	}
	
	/**
	 * 查询班级可以选择的课程列表 
	 * @param id
	 * @param mmap
	 * @return
	 */
	@GetMapping("/setClassCourse/{id}")
	public String setClassCourse(@PathVariable("id") Long id, ModelMap mmap){
		//查询班级中没有的开课课程列表.....		
		ClassInfo classInfo = classInfoService.selectClassInfoById(id);
		mmap.put("classInfo", classInfo);
		return prefix + "/selectCourse";
	}
	
	
	/**
	 * 查询班级信息列表
	 */
	@RequiresPermissions("course:classInfo:list")
	@PostMapping("/classCourseList/{id}")
	@ResponseBody
	public TableDataInfo courseList(ClassInfo classInfo) {		
		startPage();
		List<Course> list = courseService.selectClassCourseList(classInfo);	
		return getDataTable(list);
	}
	
	
	/**
	 * 执行选择课程的信息
	 */
	@RequiresPermissions("course:classInfo:list")
	@PostMapping("/selectClassCourse")
	@ResponseBody
	public AjaxResult CourseList(ClassInfo classInfo,String courseIds) {		
		Long classId=classInfo.getId();
		String  className=classInfo.getName();
		String[] ids=courseIds.split(",");
		List<ClassCourseInfo> classCourseInfos=new ArrayList<>(ids.length);
		List<Course> courses=courseService.queryCoursesByIds(ids);
		for(Course c:courses){
			classCourseInfos.add(new ClassCourseInfo(classId,className,c.getId(),c.getName()));
		}		
		return toAjax(classInfoService.insertAllClassCourseInfos(classCourseInfos));
	}
	
	
}
