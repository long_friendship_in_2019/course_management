package com.ruoyi.web.controller.front;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.course.domain.TeacherStyle;
import com.ruoyi.course.service.ITeacherStyleService;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;


/**
 * 查询正常的师生风采信息....
 * @author lenovo
 *
 */
@Controller
@RequestMapping("/front/teacher")
public class FrontTeacherController extends BaseController {
	private String prefix = "front";
	
	@Autowired
	private ISysUserService userService;
	@Autowired
	private ITeacherStyleService teacherStyle;
	
	/**
	 * 前台主页
	 * 
	 * @return
	 */
	@GetMapping("")
	public String info(ModelMap mmap) {	
		List<TeacherStyle> list=teacherStyle.selectTeacherStyleFrontList();
		mmap.put("list",list);		
		return prefix + "/teacher";
	}
	
	
	
	
	
	
	
	@GetMapping("/detail")
	public String detail(Long id,ModelMap mmap) {		
		SysUser user=userService.selectUserById(id);
		mmap.put("teacher",user);
		return prefix + "/coursedetail";
	}
	
	
	
	
}
