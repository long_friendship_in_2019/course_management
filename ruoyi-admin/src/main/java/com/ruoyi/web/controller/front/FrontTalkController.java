package com.ruoyi.web.controller.front;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.course.domain.Message;
import com.ruoyi.course.domain.Student;
import com.ruoyi.course.service.IMessageService;


@Controller
@RequestMapping("/front/talk")
public class FrontTalkController extends BaseController {
	
	private String prefix = "front";
	
	@Autowired
	private IMessageService messageService;

	/**
	 * 查询用户所有的留言信息.....
	 * @return
	 */
	@GetMapping("")
	public String info(HttpSession session,ModelMap mmap,Message message) {
		Object obj	=session.getAttribute("student");
		if(obj ==null){
			  return "redirect:/front/login";
		}
		Student student=(Student) obj;
		message.setStudentId(student.getId());
		
		List<Message> list=messageService.selectMessageListFront(message);
		mmap.put("list", list);		
		return prefix + "/talk";
	}
	
	
	/**
	 * 发表留言信息....
	 * @return
	 */
	@PostMapping("/add")
	public String add(Message message,HttpSession session) {		
		message.setMessageTime(new Date());
		message.setStatus(1);				
		Student student=(Student) session.getAttribute("student");		
		message.setStudentId(student.getId());
		message.setStudentName(student.getName());		
	    messageService.insertMessage(message);
	    return "redirect:/front/talk";
	}
	
	

}
