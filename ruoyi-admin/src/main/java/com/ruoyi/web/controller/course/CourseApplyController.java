package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.CourseApply;
import com.ruoyi.course.service.ICourseApplyService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程报名号信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/apply")
public class CourseApplyController extends BaseController
{
    private String prefix = "course/apply";

    @Autowired
    private ICourseApplyService courseApplyService;

    @RequiresPermissions("course:apply:view")
    @GetMapping()
    public String apply()
    {
        return prefix + "/apply";
    }

    /**
     * 查询课程报名号信息列表
     */
    @RequiresPermissions("course:apply:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CourseApply courseApply)
    {
        startPage();
        List<CourseApply> list = courseApplyService.selectCourseApplyList(courseApply);
        return getDataTable(list);
    }

    /**
     * 导出课程报名号信息列表
     */
    @RequiresPermissions("course:apply:export")
    @Log(title = "课程报名号信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CourseApply courseApply)
    {
        List<CourseApply> list = courseApplyService.selectCourseApplyList(courseApply);
        ExcelUtil<CourseApply> util = new ExcelUtil<CourseApply>(CourseApply.class);
        return util.exportExcel(list, "apply");
    }

    /**
     * 新增课程报名号信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存课程报名号信息
     */
    @RequiresPermissions("course:apply:add")
    @Log(title = "课程报名号信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CourseApply courseApply)
    {
        return toAjax(courseApplyService.insertCourseApply(courseApply));
    }

    /**
     * 修改课程报名号信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CourseApply courseApply = courseApplyService.selectCourseApplyById(id);
        mmap.put("courseApply", courseApply);
        return prefix + "/edit";
    }

    /**
     * 修改保存课程报名号信息
     */
    @RequiresPermissions("course:apply:edit")
    @Log(title = "课程报名号信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CourseApply courseApply)
    {
        return toAjax(courseApplyService.updateCourseApply(courseApply));
    }

    /**
     * 删除课程报名号信息
     */
    @RequiresPermissions("course:apply:remove")
    @Log(title = "课程报名号信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(courseApplyService.deleteCourseApplyByIds(ids));
    }
}
