package com.ruoyi.web.controller.course;

import java.util.Date;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.Message;
import com.ruoyi.course.service.IMessageService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学员留言信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/message")
public class MessageController extends BaseController {
	private String prefix = "course/message";

	@Autowired
	private IMessageService messageService;

	@RequiresPermissions("course:message:view")
	@GetMapping()
	public String message() {
		return prefix + "/message";
	}

	/**
	 * 查询学员留言信息列表
	 */
	@RequiresPermissions("course:message:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Message message) {
		startPage();
		List<Message> list = messageService.selectMessageList(message);
		return getDataTable(list);
	}

	/**
	 * 导出学员留言信息列表
	 */
	@RequiresPermissions("course:message:export")
	@Log(title = "学员留言信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(Message message) {
		List<Message> list = messageService.selectMessageList(message);
		ExcelUtil<Message> util = new ExcelUtil<Message>(Message.class);
		return util.exportExcel(list, "message");
	}

	/**
	 * 新增学员留言信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存学员留言信息
	 */
	@Log(title = "学员留言信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Message message) {
		message.setMessageTime(new Date());
		message.setStatus(1);
		return toAjax(messageService.insertMessage(message));
	}

	/**
	 * 修改学员留言信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		Message message = messageService.selectMessageById(id);
		mmap.put("message", message);
		return prefix + "/edit";
	}

	/**
	 * 修改保存学员留言信息
	 */
	@RequiresPermissions("course:message:edit")
	@Log(title = "学员留言信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Message message) {
		return toAjax(messageService.updateMessage(message));
	}

	/**
	 * 删除学员留言信息 ,将留言的状态改为4不可见的状态
	 */
	@RequiresPermissions("course:message:remove")
	@Log(title = "学员留言信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(messageService.deleteMessageByIds(ids));
	}
	
	
	/**
	 * 回复学员留言信息
	 */
	@GetMapping("/answering/{id}")
	public String answering(@PathVariable("id") Long id, ModelMap mmap) {
		Message message = messageService.selectMessageById(id);
		mmap.put("message", message);
		return prefix + "/answering";
	}
	
	
	/**
	 * 修改回复的留言信息
	 */	
	@Log(title = "学员留言信息", businessType = BusinessType.UPDATE)
	@PostMapping("/answeringSaved")
	@ResponseBody
	public AjaxResult answeringSaved(Message message) {
		SysUser user=ShiroUtils.getSysUser();
		message.setTeacherName(user.getUserName());
		message.setBackTime(new Date());
		message.setStatus(3);
		return toAjax(messageService.updateMessage(message));
	}
	
	/**
	 * 改变的留言的状态
	 */
	@RequiresPermissions("course:message:edit")
	@Log(title = "学员留言信息", businessType = BusinessType.UPDATE)
	@PostMapping("/changeStatus")
	@ResponseBody
	public AjaxResult changeStatus(Message message) {			
		return toAjax(messageService.updateMessageStatus(message));
	}
	
	
}
