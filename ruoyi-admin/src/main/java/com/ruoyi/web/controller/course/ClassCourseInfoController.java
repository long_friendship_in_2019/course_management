package com.ruoyi.web.controller.course;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.ClassCourseInfo;
import com.ruoyi.course.service.IClassInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 班级课程信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/info")
public class ClassCourseInfoController extends BaseController {
	private String prefix = "course/info";

	@Autowired
	private IClassInfoService classCourseInfoService;
	
	
	

	@RequiresPermissions("course:info:view")
	@GetMapping()
	public String info() {
		return prefix + "/info";
	}

	/**
	 * 查询班级课程信息列表
	 */
	@RequiresPermissions("course:info:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(ClassCourseInfo classCourseInfo) {
		startPage();
		List<ClassCourseInfo> list = classCourseInfoService.selectClassCourseInfoList(classCourseInfo);
		return getDataTable(list);
	}

	/**
	 * 导出班级课程信息列表
	 */
	@RequiresPermissions("course:info:export")
	@Log(title = "班级课程信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(ClassCourseInfo classCourseInfo) {
		List<ClassCourseInfo> list = classCourseInfoService.selectClassCourseInfoList(classCourseInfo);
		ExcelUtil<ClassCourseInfo> util = new ExcelUtil<ClassCourseInfo>(ClassCourseInfo.class);
		return util.exportExcel(list, "info");
	}

	/**
	 * 新增班级课程信息
	 */
	@GetMapping("/add")
	public String add() {
		
		return prefix + "/add";
	}

	/**
	 * 新增保存班级课程信息
	 */
	@RequiresPermissions("course:info:add")
	@Log(title = "班级课程信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(ClassCourseInfo classCourseInfo) {		
		return toAjax(classCourseInfoService.insertClassCourseInfo(classCourseInfo));
	}
    
	
	
	
	/**
	 * 修改班级课程信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		ClassCourseInfo classCourseInfo = classCourseInfoService.selectClassCourseInfoById(id);
		mmap.put("classCourseInfo", classCourseInfo);
		return prefix + "/edit";
	}
	
	

	/**
	 * 修改保存班级课程信息
	 */
	@RequiresPermissions("course:info:edit")
	@Log(title = "班级课程信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(ClassCourseInfo classCourseInfo) {
		return toAjax(classCourseInfoService.updateClassCourseInfo(classCourseInfo));
	}

	/**
	 * 删除班级课程信息
	 */
	@RequiresPermissions("course:info:remove")
	@Log(title = "班级课程信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(classCourseInfoService.deleteClassCourseInfoByIds(ids));
	}
}
