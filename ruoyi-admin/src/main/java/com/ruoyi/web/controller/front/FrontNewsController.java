package com.ruoyi.web.controller.front;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.service.ISysNoticeService;

@Controller
@RequestMapping("/front/news")
public class FrontNewsController  extends BaseController{
	private String prefix = "front";
	
	@Autowired
	private ISysNoticeService noticeService; 
	
	/**
	 * 查询公告信息  就是查看
	 * @return
	 */
	@GetMapping("")
	public String info(ModelMap mmap,SysNotice notice) {			
		List<SysNotice> list = noticeService.selectNoticeListFront(notice);
		mmap.addAttribute("list",list);
		return prefix + "/news";
	}
	
	@GetMapping("/detail")
	public String detail(Long id,ModelMap mmap) {	
		SysNotice news = noticeService.selectNoticeById(id);
		mmap.addAttribute("news",news);
		return prefix + "/newsdetail";
	}
	
	
	
	
	
	
	
	
	
}
