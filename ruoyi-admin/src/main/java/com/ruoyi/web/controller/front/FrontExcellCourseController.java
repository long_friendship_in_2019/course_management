package com.ruoyi.web.controller.front;

import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.course.domain.ExcellentCourse;
import com.ruoyi.course.service.IExcellentCourseService;

@Controller
@RequestMapping("/front/excellCourse")
public class FrontExcellCourseController extends BaseController {
	
	@Autowired
	private IExcellentCourseService excellentCourseService;
	
	private String prefix = "front";
	
	/**
	 * 就是查询展示所有的精品课程信息
	 * 
	 * @return
	 */
	@GetMapping("")
	public String info(ModelMap mmap,ExcellentCourse  course) {				
		List<ExcellentCourse> list=excellentCourseService.selectExcellentCourseListFront(course);
		mmap.put("list",list);		
		return prefix + "/excellCourse.html";		
	}
	
	
	
	@GetMapping("/detail")
	public String detail(Long id,ModelMap mmap,HttpSession session) {
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}
		ExcellentCourse course=excellentCourseService.selectExcellentCourseById(id);
		mmap.put("course",course);
		return prefix + "/coursedetail2";
	}
	
	
	

}
