package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.CourseAppointment;
import com.ruoyi.course.service.ICourseAppointmentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程预约信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/appointment")
public class CourseAppointmentController extends BaseController {
	private String prefix = "course/appointment";

	@Autowired
	private ICourseAppointmentService courseAppointmentService;

	@RequiresPermissions("course:appointment:view")
	@GetMapping()
	public String appointment() {
		return prefix + "/appointment";
	}

	/**
	 * 查询课程预约信息列表
	 */
	@RequiresPermissions("course:appointment:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(CourseAppointment courseAppointment) {
		startPage();
		List<CourseAppointment> list = courseAppointmentService.selectCourseAppointmentList(courseAppointment);
		return getDataTable(list);
	}

	/**
	 * 导出课程预约信息列表
	 */
	@RequiresPermissions("course:appointment:export")
	@Log(title = "课程预约信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(CourseAppointment courseAppointment) {
		List<CourseAppointment> list = courseAppointmentService.selectCourseAppointmentList(courseAppointment);
		ExcelUtil<CourseAppointment> util = new ExcelUtil<CourseAppointment>(CourseAppointment.class);
		return util.exportExcel(list, "appointment");
	}

	/**
	 * 新增课程预约信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存课程预约信息
	 */
	@RequiresPermissions("course:appointment:add")
	@Log(title = "课程预约信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(CourseAppointment courseAppointment) {
		return toAjax(courseAppointmentService.insertCourseAppointment(courseAppointment));
	}

	/**
	 * 修改课程预约信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		CourseAppointment courseAppointment = courseAppointmentService.selectCourseAppointmentById(id);
		mmap.put("courseAppointment", courseAppointment);
		return prefix + "/edit";
	}

	/**
	 * 修改保存课程预约信息
	 */
	@RequiresPermissions("course:appointment:edit")
	@Log(title = "课程预约信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(CourseAppointment courseAppointment) {
		return toAjax(courseAppointmentService.updateCourseAppointment(courseAppointment));
	}

	/**
	 * 删除课程预约信息
	 */
	@RequiresPermissions("course:appointment:remove")
	@Log(title = "课程预约信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(courseAppointmentService.deleteCourseAppointmentByIds(ids));
	}
}
