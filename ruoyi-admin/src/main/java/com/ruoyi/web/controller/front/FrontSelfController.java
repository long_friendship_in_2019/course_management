package com.ruoyi.web.controller.front;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.course.domain.Course;
import com.ruoyi.course.domain.CourseAppointment;
import com.ruoyi.course.domain.PersonCourseInfo;
import com.ruoyi.course.domain.Student;
import com.ruoyi.course.service.ICourseApplyService;
import com.ruoyi.course.service.ICourseAppointmentService;
import com.ruoyi.course.service.ICourseService;
import com.ruoyi.course.service.IExcellentCourseService;
import com.ruoyi.course.service.IPersonCourseInfoService;
import com.ruoyi.course.service.IStudentService;

@Controller
@RequestMapping("/front/self")
public class FrontSelfController extends BaseController {
	private String prefix = "front";

	@Autowired
	private IStudentService studentService;
	@Autowired
	private ICourseService courseService;
	@Autowired
	private IExcellentCourseService excellCourseService;

	@Autowired
	private IPersonCourseInfoService personCourseInfoService;
	@Autowired
	private ICourseAppointmentService courseAppointmentService;

	/**
	 * 前台主页
	 * 
	 * @return
	 */
	@GetMapping("")
	public String info(HttpSession session, ModelMap mmap) {
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}
		Student student = (Student) obj;
		PersonCourseInfo personCourseInfo = new PersonCourseInfo();
		personCourseInfo.setStatus(1);
		personCourseInfo.setStudentCode(student.getId());
		personCourseInfo.setStudentName(student.getName());
		List<PersonCourseInfo> courses = personCourseInfoService.selectPersonCourseInfoList(personCourseInfo);

		CourseAppointment courseAppointment = new CourseAppointment();
		courseAppointment.setStudentId(student.getId());
		courseAppointment.setStatus(1);
		courseAppointment.setStudentName(student.getName());
		List<CourseAppointment> appointments = courseAppointmentService.selectCourseAppointmentList(courseAppointment);

		mmap.put("courses", courses);
		mmap.put("appointments", appointments);

		return prefix + "/self";
	}

	/**
	 * 报名
	 * 
	 * @param session
	 * @return
	 */
	@GetMapping("/apply")
	public String apply(HttpSession session, PersonCourseInfo personCourseInfo) {
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}
		Student student = (Student) obj;

		if (personCourseInfo.getType() == 2) {
			personCourseInfo.setCourseName(courseService.selectCourseById(personCourseInfo.getCourseId()).getName());
		} else {
			personCourseInfo.setCourseName(
					excellCourseService.selectExcellentCourseById(personCourseInfo.getCourseId()).getCourseName());
		}
		personCourseInfo.setStudentCode(student.getId());
		personCourseInfo.setStudentName(student.getName());
		personCourseInfo.setApplyTime(new Date());
		personCourseInfo.setStatus(1);
		personCourseInfoService.insertPersonCourseInfo(personCourseInfo);
		return prefix + "/self";
	}

	/**
	 * 预约
	 * 
	 * @param session
	 * @return
	 */
	@GetMapping("/appointment")
	public String appointment(HttpSession session, CourseAppointment appointment) {
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}
		Student student = (Student) obj;

		if (appointment.getType() == 2) {
			Course c = courseService.selectCourseById(appointment.getCourseId());
			appointment.setCourseName(c.getName());
			appointment.setCourseTime(c.getPublishTime());
		} else {
			appointment.setCourseName(
					excellCourseService.selectExcellentCourseById(appointment.getCourseId()).getCourseName());
			// 默认两天后开课
			appointment.setCourseTime(DateUtils.addDays(new Date(), 2));
		}
		appointment.setStudentId(student.getId());
		appointment.setStudentName(student.getName());
		appointment.setCreateTime(new Date());
		appointment.setStatus(1);
		appointment.setCourseStatus(1);
		courseAppointmentService.insertCourseAppointment(appointment);
		return prefix + "/self";
	}

	/**
	 * 修改密码
	 * 
	 * @param session
	 * @return
	 */
	@PostMapping("/updatePass")
	public String updatePass(HttpSession session, String oldPassword, String newPassword) {
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}
		Student student = (Student) obj;
		if (student.getPassword().equals(oldPassword)) {
			student.setPassword(newPassword);
		}
		studentService.updateStudent(student);
		session.removeAttribute("student");
		return "redirect:/front/login";
	}

	@GetMapping("/cancel")
	public String cancel(CourseAppointment appointment) {
		appointment.setStatus(2);
		courseAppointmentService.updateCourseAppointment(appointment);
		return "redirect:/front/self";
	}

	/**
	 * 修改个人信息
	 * 
	 * @param session
	 * @return
	 */
	@GetMapping("/updateStudent")
	public String updateStudent(HttpSession session, ModelMap mmap) {
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}
		Student student=(Student) obj;
		student=studentService.queryStudentByPassWord(student);
		mmap.put("student", student);
		return prefix + "/update";
	}

	/**
	 * 修改个人信息
	 * 
	 * @param session
	 * @return
	 */
	@PostMapping("/updateStudent")
	public String doUpdateStudent(HttpSession session, Student stu) {
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}
		stu.setId(((Student) obj).getId());
		studentService.updateStudent(stu);
		return prefix + "/self";
	}

}
