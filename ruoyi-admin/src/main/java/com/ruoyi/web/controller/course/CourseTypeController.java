package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.CourseType;
import com.ruoyi.course.service.ICourseTypeService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程类别信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/courseType")
public class CourseTypeController extends BaseController {
	private String prefix = "course/courseType";

	@Autowired
	private ICourseTypeService courseTypeService;

	@RequiresPermissions("course:courseType:view")
	@GetMapping()
	public String courseType() {
		return prefix + "/courseType";
	}

	/**
	 * 查询课程类别信息列表
	 */
	@RequiresPermissions("course:courseType:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(CourseType courseType) {
		startPage();
		List<CourseType> list = courseTypeService.selectCourseTypeList(courseType);
		return getDataTable(list);
	}

	/**
	 * 导出课程类别信息列表
	 */
	@RequiresPermissions("course:courseType:export")
	@Log(title = "课程类别信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(CourseType courseType) {
		List<CourseType> list = courseTypeService.selectCourseTypeList(courseType);
		ExcelUtil<CourseType> util = new ExcelUtil<CourseType>(CourseType.class);
		return util.exportExcel(list, "courseType");
	}

	/**
	 * 新增课程类别信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存课程类别信息
	 */
	@RequiresPermissions("course:courseType:add")
	@Log(title = "课程类别信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(CourseType courseType) {
		courseType.setCreateUser(ShiroUtils.getSysUser().getUserName());		
		return toAjax(courseTypeService.insertCourseType(courseType));
	}

	/**
	 * 修改课程类别信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		CourseType courseType = courseTypeService.selectCourseTypeById(id);
		mmap.put("courseType", courseType);
		return prefix + "/edit";
	}

	/**
	 * 修改保存课程类别信息
	 */
	@RequiresPermissions("course:courseType:edit")
	@Log(title = "课程类别信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(CourseType courseType) {
		courseType.setUpdateUser(ShiroUtils.getSysUser().getUserName());
		return toAjax(courseTypeService.updateCourseType(courseType));
	}

	/**
	 * 删除课程类别信息
	 */
	@RequiresPermissions("course:courseType:remove")
	@Log(title = "课程类别信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(courseTypeService.deleteCourseTypeByIds(ids));
	}
}
