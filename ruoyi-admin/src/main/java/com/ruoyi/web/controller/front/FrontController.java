package com.ruoyi.web.controller.front;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.course.domain.Student;
import com.ruoyi.course.service.IStudentService;

@Controller
@RequestMapping("/front")
public class FrontController extends BaseController{
	
	private String prefix = "front";
	
	@Autowired
	private IStudentService studentService;
	
	/**
	 * 前台主页
	 * @return
	 */
	@GetMapping("")
	public String info() {
		return prefix + "/index";
	}
	


	
	/**
	 * 学员注册....
	 * @return
	 */
	@GetMapping("/register")
	public String register() {			
		return prefix + "/register";
	}
	
	
	
	/**
	 * 学员注册....
	 * @return
	 */
	@PostMapping("/register")
	public String doRegister(Student student) {			
		studentService.insertStudent(student);
		return "redirect:/front/login";
	}
	
	
	
	/**
	 * 登陆信息....
	 * @return
	 */
	@GetMapping("/login")
	public String login() {			
		return prefix + "/login";
	}
	
	
	@PostMapping("/login")
	public String doLogin(Student student,HttpSession session) {
	    //进行登陆的验证.....		
	    Student stu=studentService.queryStudentByPassWord(student);
		if(stu==null || stu.getStatus()!=1){		
			return "redirect:/front/login";
		}			
		session.setAttribute("student", stu);			
		return "redirect:/front/self";
	}
	
	
	@RequestMapping("/loginOut")
	public String loginOut(HttpSession session) {
	    //进行登陆的验证.....	   			
		session.removeAttribute("student");			
		return "redirect:/front/login";
	}
	
	
	
	
	
	
	

}
