package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.CourseEvaluation;
import com.ruoyi.course.service.ICourseEvaluationService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程评价信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/evaluation")
public class CourseEvaluationController extends BaseController {
	private String prefix = "course/evaluation";

	@Autowired
	private ICourseEvaluationService courseEvaluationService;

	@RequiresPermissions("course:evaluation:view")
	@GetMapping()
	public String evaluation() {
		return prefix + "/evaluation";
	}

	/**
	 * 查询课程评价信息列表
	 */
	@RequiresPermissions("course:evaluation:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(CourseEvaluation courseEvaluation) {
		startPage();
		List<CourseEvaluation> list = courseEvaluationService.selectCourseEvaluationList(courseEvaluation);
		return getDataTable(list);
	}

	/**
	 * 导出课程评价信息列表
	 */
	@RequiresPermissions("course:evaluation:export")
	@Log(title = "课程评价信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(CourseEvaluation courseEvaluation) {
		List<CourseEvaluation> list = courseEvaluationService.selectCourseEvaluationList(courseEvaluation);
		ExcelUtil<CourseEvaluation> util = new ExcelUtil<CourseEvaluation>(CourseEvaluation.class);
		return util.exportExcel(list, "evaluation");
	}

	/**
	 * 新增课程评价信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存课程评价信息
	 */
	@RequiresPermissions("course:evaluation:add")
	@Log(title = "课程评价信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(CourseEvaluation courseEvaluation) {
		return toAjax(courseEvaluationService.insertCourseEvaluation(courseEvaluation));
	}

	/**
	 * 修改课程评价信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		CourseEvaluation courseEvaluation = courseEvaluationService.selectCourseEvaluationById(id);
		mmap.put("courseEvaluation", courseEvaluation);
		return prefix + "/edit";
	}

	/**
	 * 修改保存课程评价信息
	 */
	@RequiresPermissions("course:evaluation:edit")
	@Log(title = "课程评价信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(CourseEvaluation courseEvaluation) {
		return toAjax(courseEvaluationService.updateCourseEvaluation(courseEvaluation));
	}

	/**
	 * 删除课程评价信息
	 */
	@RequiresPermissions("course:evaluation:remove")
	@Log(title = "课程评价信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(courseEvaluationService.deleteCourseEvaluationByIds(ids));
	}
}
