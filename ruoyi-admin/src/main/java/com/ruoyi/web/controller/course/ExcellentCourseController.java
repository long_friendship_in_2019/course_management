package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.ExcellentCourse;
import com.ruoyi.course.service.IExcellentCourseService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 精品课程信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/excellentCourse")
public class ExcellentCourseController extends BaseController {
	private static final Logger log = LoggerFactory.getLogger(ExcellentCourseController.class);
	private String prefix = "course/excellentCourse";

	@Autowired
	private IExcellentCourseService excellentCourseService;

	@RequiresPermissions("course:excellentCourse:view")
	@GetMapping()
	public String excellentCourse() {
		return prefix + "/excellentCourse";
	}

	/**
	 * 查询精品课程信息列表
	 */
	@RequiresPermissions("course:excellentCourse:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(ExcellentCourse excellentCourse) {
		startPage();
		List<ExcellentCourse> list = excellentCourseService.selectExcellentCourseList(excellentCourse);
		return getDataTable(list);
	}

	/**
	 * 导出精品课程信息列表
	 */
	@RequiresPermissions("course:excellentCourse:export")
	@Log(title = "精品课程信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(ExcellentCourse excellentCourse) {
		List<ExcellentCourse> list = excellentCourseService.selectExcellentCourseList(excellentCourse);
		ExcelUtil<ExcellentCourse> util = new ExcelUtil<ExcellentCourse>(ExcellentCourse.class);
		return util.exportExcel(list, "excellentCourse");
	}

	/**
	 * 新增精品课程信息
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap) {
		
		return prefix + "/add";
	}

	/**
	 * 新增保存精品课程信息
	 */
	@RequiresPermissions("course:excellentCourse:add")
	@Log(title = "精品课程信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(ExcellentCourse excellentCourse) {
		excellentCourse.setCreateUser(ShiroUtils.getSysUser().getUserName());
		excellentCourse.setStatus(1);
		return toAjax(excellentCourseService.insertExcellentCourse(excellentCourse));
	}

	/**
	 * 修改精品课程信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		ExcellentCourse excellentCourse = excellentCourseService.selectExcellentCourseById(id);
		mmap.put("excellentCourse", excellentCourse);
		return prefix + "/edit";
	}

	/**
	 * 修改保存精品课程信息
	 */
	@RequiresPermissions("course:excellentCourse:edit")
	@Log(title = "精品课程信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(ExcellentCourse excellentCourse) {
		return toAjax(excellentCourseService.updateExcellentCourse(excellentCourse));
	}

	/**
	 * 删除精品课程信息
	 */
	@RequiresPermissions("course:excellentCourse:remove")
	@Log(title = "精品课程信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(excellentCourseService.deleteExcellentCourseByIds(ids));
	}
	
	
	/**
	 * 上传精品课程图片信息
	 */	
	@Log(title = "上传图片", businessType = BusinessType.UPDATE)
	@PostMapping("/uploadImg")
	@ResponseBody
	public AjaxResult uploadImg(@RequestParam("coursePicture") MultipartFile file) {
		try {
			if (!file.isEmpty()) {
				String imgPath = FileUploadUtils.upload(Global.getUploadPath(), file);				
				return success(imgPath);
		   }else{
			   return error("上传图片失败！");
		   }
		}catch (Exception e) {
			log.error("上传头像失败！", e);
			return error("上传图片失败！");
		}	
		
	}
	
	
	/**
	 * 上传精品课程视频信息
	 */	
	@Log(title = "上传视频", businessType = BusinessType.UPDATE)
	@PostMapping("/uploadVideo")
	@ResponseBody
	public AjaxResult uploadVideo(@RequestParam("courseVideo") MultipartFile file) {
		try {
			if (!file.isEmpty()) {
				String imgPath = FileUploadUtils.upload(Global.getUploadPath(), file);				
				return success(imgPath);
		   }else{
			   return error("上传视频失败！");
		   }
		}catch (Exception e) {
			log.error("上传视频失败！", e);
			return error("上传视频失败！");
		}	
		
	}
	
	
	
	
}
