package com.ruoyi.web.controller.course;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.Course;
import com.ruoyi.course.service.IClassInfoService;
import com.ruoyi.course.service.ICourseService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.web.controller.system.SysProfileController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/course")
public class CourseController extends BaseController {
	private static final Logger log = LoggerFactory.getLogger(CourseController.class);
	private String prefix = "course/course";

	@Autowired
	private ICourseService courseService;
	@Autowired
	private IClassInfoService classInfoService;
	
	@RequiresPermissions("course:course:view")
	@GetMapping()
	public String course(ModelMap mmap) {
		List<Map<String,Object>> courseTypes=courseService.querAllCourseTypes();
		mmap.put("courseTypes", courseTypes);
		return prefix + "/course";
	}

	/**
	 * 查询课程信息列表
	 */
	@RequiresPermissions("course:course:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Course course) {
		startPage();
		List<Course> list = courseService.selectCourseList(course);
		return getDataTable(list);
	}

	/**
	 * 导出课程信息列表
	 */
	@RequiresPermissions("course:course:export")
	@Log(title = "课程信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(Course course) {
		List<Course> list = courseService.selectCourseList(course);
		ExcelUtil<Course> util = new ExcelUtil<Course>(Course.class);
		return util.exportExcel(list, "course");
	}

	/**
	 * 新增课程信息
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap) {
		List<Map<String,Object>> courseTypes=courseService.querAllCourseTypes();
		String jobName="讲师";
		List<Map<String, Object>> teachers=classInfoService.queryAllTeacherInfo(jobName);
		
		mmap.put("courseTypes", courseTypes);		
		mmap.put("teachers", teachers);
		
		return prefix + "/add";
	}

	/**
	 * 新增保存课程信息
	 */
	@RequiresPermissions("course:course:add")
	@Log(title = "课程信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Course course) {
		course.setStatus(1);
		course.setCreateUser(ShiroUtils.getSysUser().getUserName());
		return toAjax(courseService.insertCourse(course));
	}

	/**
	 * 修改课程信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		Course course = courseService.selectCourseById(id);
		mmap.put("course", course);
		List<Map<String,Object>> courseTypes=courseService.querAllCourseTypes();
		String jobName="讲师";
		List<Map<String, Object>> teachers=classInfoService.queryAllTeacherInfo(jobName);
		
		mmap.put("courseTypes", courseTypes);		
		mmap.put("teachers", teachers);		
		return prefix + "/edit";
	}

	/**
	 * 修改保存课程信息
	 */
	@RequiresPermissions("course:course:edit")
	@Log(title = "课程信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Course course) {
		
		course.setUpdateUser(ShiroUtils.getSysUser().getUserName());
		return toAjax(courseService.updateCourse(course));
	}

	/**
	 * 删除课程信息
	 */
	@RequiresPermissions("course:course:remove")
	@Log(title = "课程信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(courseService.deleteCourseByIds(ids));
	}
	
	
	/**
	 * 课程信息停课
	 */
	@RequiresPermissions("course:course:edit")
	@Log(title = "课程信息", businessType = BusinessType.UPDATE)
	@PostMapping("/changeStatus")
	@ResponseBody
	public AjaxResult changeStatus(Course course) {
		return toAjax(courseService.changeCourseStatus(course));
	}
	
	
	@Log(title = "上传课程图片信息", businessType = BusinessType.UPDATE)
	@PostMapping("/uploadImg")
	@ResponseBody
	public AjaxResult uploadCourseImg(@RequestParam("coursePicture") MultipartFile file){			
		try {
			if (!file.isEmpty()) {
				String imgPath = FileUploadUtils.upload(Global.getUploadPath(), file);				
				return success(imgPath);
		   }else{
			   return error("上传图片失败！");
		   }
		}catch (Exception e) {
			log.error("上传头像失败！", e);
			return error("上传图片失败！");
		}
		
	}
	
	
}
