package com.ruoyi.web.controller.course;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.Student;
import com.ruoyi.course.service.IClassInfoService;
import com.ruoyi.course.service.IStudentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学员信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/student")
public class StudentController extends BaseController {
	private static final Logger log = LoggerFactory.getLogger(StudentController.class);
	private String prefix = "course/student";

	@Autowired
	private IStudentService studentService;
	@Autowired
	private IClassInfoService classService;

	@RequiresPermissions("course:student:view")
	@GetMapping()
	public String student() {
		return prefix + "/student";
	}

	/**
	 * 查询学员信息列表
	 */
	@RequiresPermissions("course:student:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Student student) {
		startPage();
		List<Student> list = studentService.selectStudentList(student);
		return getDataTable(list);
	}

	/**
	 * 导出学员信息列表
	 */
	@RequiresPermissions("course:student:export")
	@Log(title = "学员信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(Student student) {
		List<Student> list = studentService.selectStudentList(student);
		ExcelUtil<Student> util = new ExcelUtil<Student>(Student.class);
		return util.exportExcel(list, "student");
	}

	/**
	 * 新增学员信息
	 */
	@GetMapping("/add")
	public String add( ModelMap mmap) {
		List<Map<String, Object>> classes=classService.queryAllUsedClass();
		mmap.put("classes", classes);
		return prefix + "/add";
	}

	/**
	 * 新增保存学员信息
	 */
	@RequiresPermissions("course:student:add")
	@Log(title = "学员信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Student student) {
		student.setMoney(0L);
		student.setScore(0L);
		student.setRegisterTime(new Date());
		student.setStatus(1);
		
		return toAjax(studentService.insertStudent(student));
	}

	/**
	 * 修改学员信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		Student student = studentService.selectStudentById(id);
		List<Map<String, Object>> classes=classService.queryAllUsedClass();
		mmap.put("classes", classes);
		mmap.put("student", student);
		return prefix + "/edit";
	}

	/**
	 * 修改保存学员信息
	 */
	@RequiresPermissions("course:student:edit")
	@Log(title = "学员信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Student student) {
		return toAjax(studentService.updateStudent(student));
	}

	/**
	 * 删除学员信息
	 */
	@RequiresPermissions("course:student:remove")
	@Log(title = "学员信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(studentService.deleteStudentByIds(ids));
	}
	
	
	/**
	 * 修改学生的状态
	 */
	@RequiresPermissions("course:student:edit")
	@Log(title = "修改学生状态", businessType = BusinessType.UPDATE)
	@PostMapping("/changeStatus")
	@ResponseBody
	public AjaxResult changeStatus(Student student) {
		return toAjax(studentService.changeCourseStatus(student));
	}
	
	/**
	 * 修改学生的状态
	 */
	@RequiresPermissions("course:student:edit")
	@Log(title = "重置密码", businessType = BusinessType.UPDATE)
	@PostMapping("/resetPwd")
	@ResponseBody
	public AjaxResult resetPwd(Student student) {
		return toAjax(studentService.resetPwd(student));
	}
	
	
	@Log(title = "上传头像信息信息", businessType = BusinessType.UPDATE)
	@PostMapping("/uploadImg")
	@ResponseBody
	public AjaxResult uploadCourseImg(@RequestParam("coursePicture") MultipartFile file){			
		try {
			if (!file.isEmpty()) {
				String imgPath = FileUploadUtils.upload(Global.getAvatarPath(), file);				
				return success(imgPath);
		   }else{
			   return error("上传头像失败！");
		   }
		}catch (Exception e) {
			log.error("上传头像失败！", e);
			return error("上传头像失败！");
		}
		
	}
	
	
}
