package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.TeacherStyle;
import com.ruoyi.course.service.ITeacherStyleService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 师生风采信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/style")
public class TeacherStyleController extends BaseController {
	private String prefix = "course/style";

	@Autowired
	private ITeacherStyleService teacherStyleService;

	@RequiresPermissions("course:style:view")
	@GetMapping()
	public String style() {
		return prefix + "/style";
	} 

	/**
	 * 查询师生风采信息列表
	 */
	@RequiresPermissions("course:style:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(TeacherStyle teacherStyle) {
		startPage();
		List<TeacherStyle> list = teacherStyleService.selectTeacherStyleList(teacherStyle);
		return getDataTable(list);
	}

	/**
	 * 导出师生风采信息列表
	 */
	@RequiresPermissions("course:style:export")
	@Log(title = "师生风采信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(TeacherStyle teacherStyle) {
		List<TeacherStyle> list = teacherStyleService.selectTeacherStyleList(teacherStyle);
		ExcelUtil<TeacherStyle> util = new ExcelUtil<TeacherStyle>(TeacherStyle.class);
		return util.exportExcel(list, "style");
	}

	/**
	 * 新增师生风采信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存师生风采信息
	 */
	@RequiresPermissions("course:style:add")
	@Log(title = "师生风采信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(TeacherStyle teacherStyle) {
		teacherStyle.setCreateUser(ShiroUtils.getSysUser().getUserName());
		teacherStyle.setStatus(1);
		return toAjax(teacherStyleService.insertTeacherStyle(teacherStyle));
	}

	/**
	 * 修改师生风采信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		TeacherStyle teacherStyle = teacherStyleService.selectTeacherStyleById(id);
		mmap.put("teacherStyle", teacherStyle);
		return prefix + "/edit";
	}

	/**
	 * 修改保存师生风采信息
	 */
	@RequiresPermissions("course:style:edit")
	@Log(title = "师生风采信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(TeacherStyle teacherStyle) {
		teacherStyle.setUpdateUser(ShiroUtils.getSysUser().getUserName());
		return toAjax(teacherStyleService.updateTeacherStyle(teacherStyle));
	}

	/**
	 * 删除师生风采信息
	 */
	@RequiresPermissions("course:style:remove")
	@Log(title = "师生风采信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(teacherStyleService.deleteTeacherStyleByIds(ids));
	}
	
	
	/**
	 * 上传精品课程图片信息
	 */	
	@Log(title = "上传图片", businessType = BusinessType.UPDATE)
	@PostMapping("/uploadImg")
	@ResponseBody
	public AjaxResult uploadImg(@RequestParam("coursePicture") MultipartFile file) {
		try {
			if (!file.isEmpty()) {
				String imgPath = FileUploadUtils.upload(Global.getUploadPath(), file);				
				return success(imgPath);
		   }else{
			   return error("上传图片失败！");
		   }
		}catch (Exception e) {
			logger.error("上传头像失败！", e);
			return error("上传图片失败！");
		}	
		
	}
	
	
	/**
	 * 上传精品课程视频信息
	 */	
	@Log(title = "上传视频", businessType = BusinessType.UPDATE)
	@PostMapping("/uploadVideo")
	@ResponseBody
	public AjaxResult uploadVideo(@RequestParam("courseVideo") MultipartFile file) {
		try {
			if (!file.isEmpty()) {
				String imgPath = FileUploadUtils.upload(Global.getUploadPath(), file);				
				return success(imgPath);
		   }else{
			   return error("上传视频失败！");
		   }
		}catch (Exception e) {
			logger.error("上传视频失败！", e);
			return error("上传视频失败！");
		}		
	}
	
	
}
