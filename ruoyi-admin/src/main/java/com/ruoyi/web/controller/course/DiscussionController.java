package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.Discussion;
import com.ruoyi.course.service.IDiscussionService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 讨论信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/discussion")
public class DiscussionController extends BaseController {
	private String prefix = "course/discussion";

	@Autowired
	private IDiscussionService discussionService;

	@RequiresPermissions("course:discussion:view")
	@GetMapping()
	public String discussion() {
		return prefix + "/discussion";
	}

	/**
	 * 查询讨论信息列表
	 */
	@RequiresPermissions("course:discussion:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Discussion discussion) {
		startPage();
		List<Discussion> list = discussionService.selectDiscussionList(discussion);
		return getDataTable(list);
	}

	/**
	 * 导出讨论信息列表
	 */
	@RequiresPermissions("course:discussion:export")
	@Log(title = "讨论信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(Discussion discussion) {
		List<Discussion> list = discussionService.selectDiscussionList(discussion);
		ExcelUtil<Discussion> util = new ExcelUtil<Discussion>(Discussion.class);
		return util.exportExcel(list, "discussion");
	}

	/**
	 * 新增讨论信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存讨论信息
	 */
	@RequiresPermissions("course:discussion:add")
	@Log(title = "讨论信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Discussion discussion) {
		return toAjax(discussionService.insertDiscussion(discussion));
	}

	/**
	 * 修改讨论信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		Discussion discussion = discussionService.selectDiscussionById(id);
		mmap.put("discussion", discussion);
		return prefix + "/edit";
	}

	/**
	 * 修改保存讨论信息
	 */
	@RequiresPermissions("course:discussion:edit")
	@Log(title = "讨论信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Discussion discussion) {
		return toAjax(discussionService.updateDiscussion(discussion));
	}

	/**
	 * 删除讨论信息
	 */
	@RequiresPermissions("course:discussion:remove")
	@Log(title = "讨论信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(discussionService.deleteDiscussionByIds(ids));
	}
}
