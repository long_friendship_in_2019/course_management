package com.ruoyi.web.controller.front;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.course.domain.Course;
import com.ruoyi.course.service.ICourseService;

@Controller
@RequestMapping("/front/course")
public class FrontCourseController  extends BaseController{
	
	@Autowired
	private ICourseService courseService;
	
	private String prefix = "front";

	/**
	 * 前台主页
	 * 
	 * @return
	 */
	@GetMapping("")
	public String info(ModelMap mmap,Course course) {			
		List<Course> list=courseService.selectCourseListFront(course);
		mmap.put("list",list);		
		return prefix + "/course";
	}
	
	
	@GetMapping("/detail")
	public String detail(Long id,ModelMap mmap,HttpSession session) {	
		Object obj = session.getAttribute("student");
		if (obj == null) {
			return "redirect:/front/login";
		}  		
		
		Course course=courseService.selectCourseById(id);
		mmap.put("course",course);
		return prefix + "/coursedetail";
	}
	
	
	

}
