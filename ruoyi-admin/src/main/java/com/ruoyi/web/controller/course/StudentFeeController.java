package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.StudentFee;
import com.ruoyi.course.service.IStudentFeeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学员缴费信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/fee")
public class StudentFeeController extends BaseController {
	private String prefix = "course/fee";

	@Autowired
	private IStudentFeeService studentFeeService;

	@RequiresPermissions("course:fee:view")
	@GetMapping()
	public String fee() {
		return prefix + "/fee";
	}

	/**
	 * 查询学员缴费信息列表
	 */
	@RequiresPermissions("course:fee:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(StudentFee studentFee) {
		startPage();
		List<StudentFee> list = studentFeeService.selectStudentFeeList(studentFee);
		return getDataTable(list);
	}

	/**
	 * 导出学员缴费信息列表
	 */
	@RequiresPermissions("course:fee:export")
	@Log(title = "学员缴费信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(StudentFee studentFee) {
		List<StudentFee> list = studentFeeService.selectStudentFeeList(studentFee);
		ExcelUtil<StudentFee> util = new ExcelUtil<StudentFee>(StudentFee.class);
		return util.exportExcel(list, "fee");
	}

	/**
	 * 新增学员缴费信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存学员缴费信息
	 */
	@RequiresPermissions("course:fee:add")
	@Log(title = "学员缴费信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(StudentFee studentFee) {
		return toAjax(studentFeeService.insertStudentFee(studentFee));
	}

	/**
	 * 修改学员缴费信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		StudentFee studentFee = studentFeeService.selectStudentFeeById(id);
		mmap.put("studentFee", studentFee);
		return prefix + "/edit";
	}

	/**
	 * 修改保存学员缴费信息
	 */
	@RequiresPermissions("course:fee:edit")
	@Log(title = "学员缴费信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(StudentFee studentFee) {
		return toAjax(studentFeeService.updateStudentFee(studentFee));
	}

	/**
	 * 删除学员缴费信息
	 */
	@RequiresPermissions("course:fee:remove")
	@Log(title = "学员缴费信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(studentFeeService.deleteStudentFeeByIds(ids));
	}
}
