package com.ruoyi.web.controller.course;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.PersonCourseInfo;
import com.ruoyi.course.service.IPersonCourseInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 个人课程信息Controller
 * 
 * @author keqiang
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/course/personCourse")
public class PersonCourseInfoController extends BaseController {
	private String prefix = "course/personCourse";

	@Autowired
	private IPersonCourseInfoService personCourseInfoService;

	@RequiresPermissions("course:personCourse:view")
	@GetMapping()
	public String personCourse() {
		return prefix + "/personCourse";
	}

	/**
	 * 查询个人课程信息列表
	 */
	@RequiresPermissions("course:personCourse:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(PersonCourseInfo personCourseInfo) {
		startPage();
		List<PersonCourseInfo> list = personCourseInfoService.selectPersonCourseInfoList(personCourseInfo);
		return getDataTable(list);
	}

	/**
	 * 导出个人课程信息列表
	 */
	@RequiresPermissions("course:personCourse:export")
	@Log(title = "个人课程信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(PersonCourseInfo personCourseInfo) {
		List<PersonCourseInfo> list = personCourseInfoService.selectPersonCourseInfoList(personCourseInfo);
		ExcelUtil<PersonCourseInfo> util = new ExcelUtil<PersonCourseInfo>(PersonCourseInfo.class);
		return util.exportExcel(list, "personCourse");
	}

	/**
	 * 新增个人课程信息
	 */
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存个人课程信息
	 */
	@RequiresPermissions("course:personCourse:add")
	@Log(title = "个人课程信息", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(PersonCourseInfo personCourseInfo) {
		return toAjax(personCourseInfoService.insertPersonCourseInfo(personCourseInfo));
	}

	/**
	 * 修改个人课程信息
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap) {
		PersonCourseInfo personCourseInfo = personCourseInfoService.selectPersonCourseInfoById(id);
		mmap.put("personCourseInfo", personCourseInfo);
		return prefix + "/edit";
	}

	/**
	 * 修改保存个人课程信息
	 */
	@RequiresPermissions("course:personCourse:edit")
	@Log(title = "个人课程信息", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(PersonCourseInfo personCourseInfo) {
		return toAjax(personCourseInfoService.updatePersonCourseInfo(personCourseInfo));
	}

	/**
	 * 删除个人课程信息
	 */
	@RequiresPermissions("course:personCourse:remove")
	@Log(title = "个人课程信息", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {
		return toAjax(personCourseInfoService.deletePersonCourseInfoByIds(ids));
	}
}
