/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : 192.168.65.1:3306
 Source Schema         : banjiteachercourse

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 14/05/2020 21:52:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'wct_class_course_info', '班级课程信息表', 'ClassCourseInfo', 'crud', 'com.ruoyi.course', 'course', 'info', '班级课程信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 17:46:51', '');
INSERT INTO `gen_table` VALUES (2, 'wct_class_info', '班级信息表', 'ClassInfo', 'crud', 'com.ruoyi.course', 'course', 'classInfo', '班级信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 18:01:16', '');
INSERT INTO `gen_table` VALUES (3, 'wct_course', '课程信息表', 'Course', 'crud', 'com.ruoyi.course', 'course', 'course', '课程信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 18:30:14', '');
INSERT INTO `gen_table` VALUES (4, 'wct_course_apply', '课程报名号信息表', 'CourseApply', 'crud', 'com.ruoyi.course', 'course', 'apply', '课程报名号信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 21:37:49', '');
INSERT INTO `gen_table` VALUES (5, 'wct_course_appointment', '课程预约信息表', 'CourseAppointment', 'crud', 'com.ruoyi.course', 'course', 'appointment', '课程预约信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 21:43:48', '');
INSERT INTO `gen_table` VALUES (7, 'wct_course_type', '课程类别信息表', 'CourseType', 'crud', 'com.ruoyi.course', 'course', 'courseType', '课程类别信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 18:43:37', '');
INSERT INTO `gen_table` VALUES (8, 'wct_discussion', '讨论信息表', 'Discussion', 'crud', 'com.ruoyi.course', 'course', 'discussion', '讨论信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 20:43:52', '');
INSERT INTO `gen_table` VALUES (9, 'wct_excellent_course', '精品课程信息表', 'ExcellentCourse', 'crud', 'com.ruoyi.course', 'course', 'excellentCourse', '精品课程信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 20:34:07', '');
INSERT INTO `gen_table` VALUES (10, 'wct_message', '学员留言信息表', 'Message', 'crud', 'com.ruoyi.course', 'course', 'message', '学员留言信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 20:54:55', '');
INSERT INTO `gen_table` VALUES (11, 'wct_person_course_info', '个人课程信息表', 'PersonCourseInfo', 'crud', 'com.ruoyi.course', 'course', 'personCourse', '个人课程信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 21:51:00', '');
INSERT INTO `gen_table` VALUES (12, 'wct_student', '学员信息表', 'Student', 'crud', 'com.ruoyi.course', 'course', 'student', '学员信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 21:13:13', '');
INSERT INTO `gen_table` VALUES (13, 'wct_student_fee', '学员缴费信息表', 'StudentFee', 'crud', 'com.ruoyi.course', 'course', 'fee', '学员缴费信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 21:20:35', '');
INSERT INTO `gen_table` VALUES (14, 'wct_teacher_style', '师生风采信息表', 'TeacherStyle', 'crud', 'com.ruoyi.course', 'course', 'style', '师生风采信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-19 17:40:47', '', '2020-04-19 21:30:32', '');
INSERT INTO `gen_table` VALUES (15, 'wct_course_evaluation', '课程评价信息表', 'CourseEvaluation', 'crud', 'com.ruoyi.course', 'course', 'evaluation', '课程评价信息', 'keqiang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2020-04-21 20:30:19', '', '2020-04-21 20:31:25', '');

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 144 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 17:46:51');
INSERT INTO `gen_table_column` VALUES (2, '1', 'class_id', '班级编号', 'int(11)', 'Long', 'classId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 17:46:51');
INSERT INTO `gen_table_column` VALUES (3, '1', 'class_name', '班级名称', 'varchar(50)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 17:46:51');
INSERT INTO `gen_table_column` VALUES (4, '1', 'course_id', '课程编号', 'int(11)', 'Long', 'courseId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 17:46:51');
INSERT INTO `gen_table_column` VALUES (5, '1', 'course_name', '课程名称', 'varchar(50)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 17:46:51');
INSERT INTO `gen_table_column` VALUES (6, '1', 'create_time', '创建时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 17:46:51');
INSERT INTO `gen_table_column` VALUES (7, '2', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:01:16');
INSERT INTO `gen_table_column` VALUES (8, '2', 'name', '班级名称', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:01:16');
INSERT INTO `gen_table_column` VALUES (9, '2', 'introduce', '班级描述', 'varchar(2000)', 'String', 'introduce', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:01:16');
INSERT INTO `gen_table_column` VALUES (10, '2', 'teacher_id', '辅导员编号', 'int(11)', 'Long', 'teacherId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:01:16');
INSERT INTO `gen_table_column` VALUES (11, '2', 'teacher_name', '班级辅导员老师', 'varchar(50)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:01:17');
INSERT INTO `gen_table_column` VALUES (12, '2', 'create_time', '班级开设时间', 'date', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:01:17');
INSERT INTO `gen_table_column` VALUES (13, '2', 'status', '班级状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:01:17');
INSERT INTO `gen_table_column` VALUES (14, '3', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (15, '3', 'name', '课程名称', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (16, '3', 'introduce', '课程介绍', 'text', 'String', 'introduce', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (17, '3', 'picture', '课程图片', 'varchar(2000)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (18, '3', 'type_id', '课程分类编号', 'int(11)', 'Long', 'typeId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (19, '3', 'type_name', '课程分类名称', 'varchar(50)', 'String', 'typeName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (20, '3', 'price', '课程价格', 'double', 'Long', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (21, '3', 'status', '课程状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (22, '3', 'publish_time', '发布时间', 'timestamp', 'Date', 'publishTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 9, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (23, '3', 'teacher_id', '课程主讲老师编号', 'int(11)', 'Long', 'teacherId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 10, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (24, '3', 'teacher_name', '课程主讲老师', 'varchar(50)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 11, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (25, '3', 'create_user', '创建人', 'varchar(50)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 12, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (26, '3', 'create_time', '创建时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (27, '3', 'update_user', '修改人', 'varchar(50)', 'String', 'updateUser', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 14, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (28, '3', 'update_time', '修改时间', 'timestamp', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:30:14');
INSERT INTO `gen_table_column` VALUES (29, '4', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (30, '4', 'student_id', '学员编号', 'int(11)', 'Long', 'studentId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (31, '4', 'student_name', '学员姓名', 'varchar(50)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (32, '4', 'phone', '学员电话', 'varchar(50)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (33, '4', 'class_id', '班级编号', 'int(11)', 'Long', 'classId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (34, '4', 'class_name', '班级名称', 'varchar(50)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (35, '4', 'course_id', '课程编号', 'int(11)', 'Long', 'courseId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (36, '4', 'course_name', '课程名称', 'varchar(50)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (37, '4', 'price', '报名的价格', 'double', 'Long', 'price', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'input', '', 9, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (38, '4', 'create_time', '报名时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', '', 10, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (39, '4', 'course_time', '预计上课时间', 'date', 'Date', 'courseTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 11, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (40, '4', 'status', '支付状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'wct_pay_staus', 12, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:37:49');
INSERT INTO `gen_table_column` VALUES (41, '5', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:48');
INSERT INTO `gen_table_column` VALUES (42, '5', 'course_id', '课程编号', 'int(11)', 'Long', 'courseId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:48');
INSERT INTO `gen_table_column` VALUES (43, '5', 'course_name', '课程名称', 'varchar(50)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:48');
INSERT INTO `gen_table_column` VALUES (44, '5', 'student_id', '学员编号', 'int(11)', 'Long', 'studentId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:48');
INSERT INTO `gen_table_column` VALUES (45, '5', 'student_name', '学员名称', 'varchar(50)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (46, '5', 'status', '状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'wct_course_evaluation_status', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (47, '5', 'course_time', '预约上课时间', 'date', 'Date', 'courseTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (48, '5', 'actual_course_time', '实际上课时间', 'date', 'Date', 'actualCourseTime', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'datetime', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (49, '5', 'course_status', '上课状态', 'tinyint(4)', 'Integer', 'courseStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 9, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (50, '5', 'teacher_name', '上课老师', 'varchar(50)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 10, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (51, '5', 'address', '上课地点', 'varchar(500)', 'String', 'address', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 11, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (52, '5', 'create_time', '创建时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, '1', 'EQ', 'datetime', '', 12, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:43:49');
INSERT INTO `gen_table_column` VALUES (61, '7', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:43:37');
INSERT INTO `gen_table_column` VALUES (62, '7', 'type_name', '课程类型名称', 'varchar(50)', 'String', 'typeName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:43:37');
INSERT INTO `gen_table_column` VALUES (63, '7', 'create_user', '创建人', 'varchar(50)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:43:37');
INSERT INTO `gen_table_column` VALUES (64, '7', 'create_time', '创建时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:43:37');
INSERT INTO `gen_table_column` VALUES (65, '7', 'update_user', '修改人', 'varchar(50)', 'String', 'updateUser', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:43:37');
INSERT INTO `gen_table_column` VALUES (66, '7', 'update_time', '修改时间', 'timestamp', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 18:43:37');
INSERT INTO `gen_table_column` VALUES (67, '8', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:43:52');
INSERT INTO `gen_table_column` VALUES (68, '8', 'title', '讨论的标题', 'varchar(500)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:43:52');
INSERT INTO `gen_table_column` VALUES (69, '8', 'content', '讨论的内容', 'text', 'String', 'content', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:43:52');
INSERT INTO `gen_table_column` VALUES (70, '8', 'user', '发布人', 'varchar(50)', 'String', 'user', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:43:52');
INSERT INTO `gen_table_column` VALUES (71, '8', 'create_time', '发布时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, '1', 'BETWEEN', 'datetime', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:43:52');
INSERT INTO `gen_table_column` VALUES (72, '9', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (73, '9', 'course_name', '精品课程名称', 'varchar(50)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (74, '9', 'picture', '课程图片', 'varchar(2000)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (75, '9', 'video', '课程视频', 'varchar(2000)', 'String', 'video', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'textarea', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (76, '9', 'introduce', '课程简介', 'text', 'String', 'introduce', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (77, '9', 'status', '课程状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'wct_coursr_status', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (78, '9', 'price', '课程价格', 'double', 'Long', 'price', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'input', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (79, '9', 'teacher_name', '课程老师', 'varchar(50)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (80, '9', 'create_user', '创建人', 'varchar(50)', 'String', 'createUser', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (81, '9', 'create_time', '创建时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'datetime', '', 10, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:34:07');
INSERT INTO `gen_table_column` VALUES (82, '10', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (83, '10', 'content', '留言信内容', 'text', 'String', 'content', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (84, '10', 'student_id', '学员编号', 'int(11)', 'Long', 'studentId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (85, '10', 'student_name', '学员姓名', 'varchar(50)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (86, '10', 'message_object', '留言对象', 'varchar(50)', 'String', 'messageObject', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (87, '10', 'message_time', '留言时间', 'timestamp', 'Date', 'messageTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (88, '10', 'back_content', '留言回复信息', 'text', 'String', 'backContent', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (89, '10', 'teacher_name', '回复留言老师', 'varchar(50)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (90, '10', 'back_time', '回复时间', 'timestamp', 'Date', 'backTime', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'datetime', '', 9, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (91, '10', 'status', '留言状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'wct_message_status', 10, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 20:54:55');
INSERT INTO `gen_table_column` VALUES (92, '11', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:51:00');
INSERT INTO `gen_table_column` VALUES (93, '11', 'student_code', '学生编号', 'int(11)', 'Long', 'studentCode', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:51:00');
INSERT INTO `gen_table_column` VALUES (94, '11', 'student_name', '学生名称', 'varchar(50)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:51:00');
INSERT INTO `gen_table_column` VALUES (95, '11', 'course_id', '课程编号', 'int(11)', 'Long', 'courseId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:51:00');
INSERT INTO `gen_table_column` VALUES (96, '11', 'course_name', '课程名称', 'varchar(50)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:51:00');
INSERT INTO `gen_table_column` VALUES (97, '11', 'status', '课程状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'radio', 'wct_coursr_status', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:51:00');
INSERT INTO `gen_table_column` VALUES (98, '11', 'apply_time', '报名时间', 'timestamp', 'Date', 'applyTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:51:00');
INSERT INTO `gen_table_column` VALUES (99, '12', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (100, '12', 'name', '姓名', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (101, '12', 'account', '账号', 'varchar(50)', 'String', 'account', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (102, '12', 'password', '密码', 'varchar(50)', 'String', 'password', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (103, '12', 'email', '邮件', 'varchar(500)', 'String', 'email', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (104, '12', 'phone', '电话', 'varchar(50)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (105, '12', 'qq', 'QQ号', 'varchar(50)', 'String', 'qq', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (106, '12', 'wechat', '微信号', 'varchar(50)', 'String', 'wechat', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (107, '12', 'sex', '性别', 'tinyint(4)', 'Integer', 'sex', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_user_sex', 9, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (108, '12', 'img', '头像', 'varchar(2000)', 'String', 'img', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (109, '12', 'birthday', '生日', 'varchar(50)', 'String', 'birthday', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', '', 11, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (110, '12', 'idcard', '身份证号', 'varchar(50)', 'String', 'idcard', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (111, '12', 'address', '地址', 'varchar(500)', 'String', 'address', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'textarea', '', 13, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (112, '12', 'score', '积分', 'int(11)', 'Long', 'score', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'input', '', 14, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (113, '12', 'money', '账户余额', 'double', 'Long', 'money', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'input', '', 15, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (114, '12', 'class_id', '班级编号', 'int(11)', 'Long', 'classId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 16, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (115, '12', 'class_name', '班级名称', 'varchar(50)', 'String', 'className', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 17, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (116, '12', 'status', '状态(1正常,2冻结,3.失效,0删除)', 'varchar(500)', 'String', 'status', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'radio', 'wct_student_status', 18, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (117, '12', 'register_time', '注册时间', 'timestamp', 'Date', 'registerTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', '', 19, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:13:13');
INSERT INTO `gen_table_column` VALUES (118, '13', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (119, '13', 'method', '支付方式', 'varchar(50)', 'String', 'method', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (120, '13', 'money', '支付金额', 'double', 'Long', 'money', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'input', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (121, '13', 'pay_time', '支付时间', 'timestamp', 'Date', 'payTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (122, '13', 'course_id', '课程编号', 'int(11)', 'Long', 'courseId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (123, '13', 'course_name', '课程名称', 'varchar(50)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (124, '13', 'student_id', '学员编号', 'int(11)', 'Long', 'studentId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (125, '13', 'student_name', '学员的名称', 'varchar(50)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:20:35');
INSERT INTO `gen_table_column` VALUES (126, '14', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (127, '14', 'title', '标题', 'varchar(500)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', '', 2, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (128, '14', 'picture', '风采的图片', 'varchar(2000)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 3, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (129, '14', 'video', '风采的视频', 'varchar(2000)', 'String', 'video', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 4, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (130, '14', 'introduce', '说明', 'text', 'String', 'introduce', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (131, '14', 'status', '状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'wct_teacher_style', 6, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (132, '14', 'create_user', '创建人', 'varchar(50)', 'String', 'createUser', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', '', 7, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (133, '14', 'create_time', '创建时间', 'timestamp', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', '', 8, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (134, '14', 'update_user', '修改人', 'varchar(50)', 'String', 'updateUser', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (135, '14', 'update_time', '修改时间', 'timestamp', 'Date', 'updateTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2020-04-19 17:40:47', NULL, '2020-04-19 21:30:32');
INSERT INTO `gen_table_column` VALUES (136, '15', 'id', '编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');
INSERT INTO `gen_table_column` VALUES (137, '15', 'course_code', '课程编号', 'int(11)', 'Long', 'courseCode', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');
INSERT INTO `gen_table_column` VALUES (138, '15', 'course_name', '课程名称', 'varchar(50)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');
INSERT INTO `gen_table_column` VALUES (139, '15', 'teacher_name', '授课教师', 'varchar(50)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');
INSERT INTO `gen_table_column` VALUES (140, '15', 'score', '评价分数', 'int(11)', 'Long', 'score', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');
INSERT INTO `gen_table_column` VALUES (141, '15', 'student_id', '学员编号', 'int(11)', 'Long', 'studentId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 6, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');
INSERT INTO `gen_table_column` VALUES (142, '15', 'student_name', '学员姓名', 'varchar(50)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 7, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');
INSERT INTO `gen_table_column` VALUES (143, '15', 'evaluation_time', '评价时间', 'timestamp', 'Date', 'evaluationTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', 8, 'admin', '2020-04-21 20:30:19', NULL, '2020-04-21 20:31:25');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'huyang1589463815430', 1589464333697, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(13) NULL DEFAULT NULL,
  `prev_fire_time` bigint(13) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(2) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1589463820000, -1, 5, 'PAUSED', 'CRON', 1589463815000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1589463825000, -1, 5, 'PAUSED', 'CRON', 1589463816000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1589463820000, -1, 5, 'PAUSED', 'CRON', 1589463816000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '是否开启注册用户功能');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 201 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '科强技术有限公司', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 09:33:38');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '北京总公司', 1, '王科强', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 09:31:18');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '郑州分公司', 2, '王天诺', '15888888888', 'tianruo@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 09:33:38');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '教学部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 09:31:18');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '后勤管理部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 09:33:38');
INSERT INTO `sys_dept` VALUES (200, 102, '0,100,102', '教学部门', 3, '王天诺', '15962541478', 'tiannuo@163.com', '0', '0', 'admin', '2020-04-19 09:31:55', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (30, 1, '正常', '1', 'wct_class_staus', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 18:21:49', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (31, 2, '毕业', '2', 'wct_class_staus', '', '', 'Y', '0', 'admin', '2020-04-19 18:22:10', 'admin', '2020-04-19 18:22:24', '');
INSERT INTO `sys_dict_data` VALUES (32, 3, '停班', '3', 'wct_class_staus', '', '', 'Y', '0', 'admin', '2020-04-19 18:22:47', 'courseadmin', '2020-04-20 08:10:46', '');
INSERT INTO `sys_dict_data` VALUES (33, 1, '正常', '1', 'wct_coursr_status', '', '', 'Y', '0', 'admin', '2020-04-19 18:52:49', 'admin', '2020-04-19 19:03:04', '');
INSERT INTO `sys_dict_data` VALUES (34, 2, '停课', '2', 'wct_coursr_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 18:53:07', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (35, 1, '正常', '1', 'wct_student_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 18:55:22', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (36, 2, '冻结', '2', 'wct_student_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 18:55:36', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (37, 3, '失效', '2', 'wct_student_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 18:55:50', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (38, 4, '删除', '0', 'wct_student_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 18:56:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (39, 1, '正常', '1', 'wct_teacher_style', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:00:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (40, 2, '失效', '2', 'wct_teacher_style', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:01:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (41, 3, '失效', '3', 'wct_coursr_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:02:52', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (42, 1, '待支付', '1', 'wct_pay_staus', '', '', 'Y', '0', 'admin', '2020-04-19 19:07:40', 'admin', '2020-04-19 19:08:08', '');
INSERT INTO `sys_dict_data` VALUES (43, 2, '支付成功', '2', 'wct_pay_staus', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:08:00', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (44, 3, '退款', '3', 'wct_pay_staus', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:08:31', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (45, 1, '预约成功', '1', 'wct_course_evaluation_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:19:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (46, 2, '取消预约', '2', 'wct_course_evaluation_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:19:52', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (47, 3, '删除预约', '3', 'wct_course_evaluation_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 19:20:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (48, 1, '未读', '1', 'wct_message_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 20:52:31', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (49, 2, '已读', '2', 'wct_message_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 20:52:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (50, 3, '已回复', '3', 'wct_message_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 20:52:54', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (51, 4, '删除', '4', 'wct_message_status', NULL, NULL, 'Y', '0', 'admin', '2020-04-19 20:53:05', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (52, 1, '支付宝', 'ZFB', 'pay_type', NULL, NULL, 'Y', '0', 'courseadmin', '2020-04-21 14:47:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (53, 2, '微信', 'WX', 'pay_type', NULL, NULL, 'Y', '0', 'courseadmin', '2020-04-21 14:48:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (54, 3, '现金', 'XJ', 'pay_type', NULL, NULL, 'Y', '0', 'courseadmin', '2020-04-21 14:48:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (55, 4, '信用卡', 'XYK', 'pay_type', NULL, NULL, 'Y', '0', 'courseadmin', '2020-04-21 14:48:34', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (56, 5, '储蓄卡', 'CXK', 'pay_type', NULL, NULL, 'Y', '0', 'courseadmin', '2020-04-21 14:49:05', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (57, 6, '其他', 'QT', 'pay_type', NULL, NULL, 'Y', '0', 'courseadmin', '2020-04-21 14:49:16', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '班级状态字典', 'wct_class_staus', '0', 'admin', '2020-04-19 18:21:11', '', NULL, '班级状态的数据字典');
INSERT INTO `sys_dict_type` VALUES (12, '课程状态字典', 'wct_coursr_status', '0', 'admin', '2020-04-19 18:52:29', 'admin', '2020-04-19 18:55:01', '课程状态字典');
INSERT INTO `sys_dict_type` VALUES (13, '学员状态字典', 'wct_student_status', '0', 'admin', '2020-04-19 18:54:50', '', NULL, '学员状态字典');
INSERT INTO `sys_dict_type` VALUES (14, '师生风采状态字典', 'wct_teacher_style', '0', 'admin', '2020-04-19 19:00:22', '', NULL, '师生风采状态字典');
INSERT INTO `sys_dict_type` VALUES (15, '学生课程支付状态', 'wct_pay_staus', '0', 'admin', '2020-04-19 19:07:26', '', NULL, '学生课程支付状态');
INSERT INTO `sys_dict_type` VALUES (16, '学生课程预约状态', 'wct_course_evaluation_status', '0', 'admin', '2020-04-19 19:11:09', '', NULL, '学生课程预约状态');
INSERT INTO `sys_dict_type` VALUES (17, '留言信息状态', 'wct_message_status', '0', 'admin', '2020-04-19 20:51:01', 'admin', '2020-04-19 22:08:59', '留言信息状态');
INSERT INTO `sys_dict_type` VALUES (18, '支付方式数据字典', 'pay_type', '0', 'courseadmin', '2020-04-21 14:47:33', '', NULL, '支付方式数据字典');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 177 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 06:23:16');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-19 06:40:03');
INSERT INTO `sys_logininfor` VALUES (102, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 06:55:26');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 08:41:31');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 08:41:51');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 09:29:12');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 14:43:25');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 15:33:03');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 17:40:09');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-19 17:43:56');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 17:44:00');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 20:26:00');
INSERT INTO `sys_logininfor` VALUES (112, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2020-04-19 22:29:56');
INSERT INTO `sys_logininfor` VALUES (113, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2020-04-19 22:30:01');
INSERT INTO `sys_logininfor` VALUES (114, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误1次', '2020-04-19 22:30:10');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 22:30:25');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-19 22:31:42');
INSERT INTO `sys_logininfor` VALUES (117, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 22:32:02');
INSERT INTO `sys_logininfor` VALUES (118, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-19 22:33:19');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 22:33:26');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-19 22:33:58');
INSERT INTO `sys_logininfor` VALUES (121, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-19 22:34:09');
INSERT INTO `sys_logininfor` VALUES (122, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-20 00:45:59');
INSERT INTO `sys_logininfor` VALUES (123, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-20 08:10:00');
INSERT INTO `sys_logininfor` VALUES (124, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-20 08:27:47');
INSERT INTO `sys_logininfor` VALUES (125, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-20 10:16:54');
INSERT INTO `sys_logininfor` VALUES (126, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-20 10:34:20');
INSERT INTO `sys_logininfor` VALUES (127, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-21 20:28:12');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-21 20:28:22');
INSERT INTO `sys_logininfor` VALUES (129, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-21 20:35:25');
INSERT INTO `sys_logininfor` VALUES (130, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 08:16:42');
INSERT INTO `sys_logininfor` VALUES (131, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 08:59:22');
INSERT INTO `sys_logininfor` VALUES (132, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-22 09:04:04');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 19:58:58');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-22 20:09:38');
INSERT INTO `sys_logininfor` VALUES (135, 'lilonglong', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 20:09:44');
INSERT INTO `sys_logininfor` VALUES (136, 'lilonglong', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-22 20:12:38');
INSERT INTO `sys_logininfor` VALUES (137, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2020-04-22 20:12:50');
INSERT INTO `sys_logininfor` VALUES (138, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 20:12:54');
INSERT INTO `sys_logininfor` VALUES (139, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-22 20:13:05');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2020-04-22 20:13:15');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 20:13:18');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-22 20:17:06');
INSERT INTO `sys_logininfor` VALUES (143, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2020-04-22 20:17:15');
INSERT INTO `sys_logininfor` VALUES (144, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 20:17:18');
INSERT INTO `sys_logininfor` VALUES (145, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-22 20:22:24');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 20:22:31');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-04-22 20:22:55');
INSERT INTO `sys_logininfor` VALUES (148, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 20:23:08');
INSERT INTO `sys_logininfor` VALUES (149, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 21:03:05');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-22 21:52:57');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-23 05:24:29');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-23 20:21:54');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-24 08:10:15');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-04-24 08:16:42');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-13 08:36:43');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 00:07:26');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-05-14 20:51:53');
INSERT INTO `sys_logininfor` VALUES (158, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 20:52:05');
INSERT INTO `sys_logininfor` VALUES (159, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-05-14 21:12:57');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:13:03');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-05-14 21:13:27');
INSERT INTO `sys_logininfor` VALUES (162, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:13:31');
INSERT INTO `sys_logininfor` VALUES (163, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-05-14 21:13:59');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:14:04');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-05-14 21:15:20');
INSERT INTO `sys_logininfor` VALUES (166, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-05-14 21:15:27');
INSERT INTO `sys_logininfor` VALUES (167, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:15:31');
INSERT INTO `sys_logininfor` VALUES (168, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-05-14 21:23:45');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:23:52');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-05-14 21:25:27');
INSERT INTO `sys_logininfor` VALUES (171, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-05-14 21:25:37');
INSERT INTO `sys_logininfor` VALUES (172, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:25:41');
INSERT INTO `sys_logininfor` VALUES (173, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:29:14');
INSERT INTO `sys_logininfor` VALUES (174, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:32:05');
INSERT INTO `sys_logininfor` VALUES (175, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-05-14 21:44:14');
INSERT INTO `sys_logininfor` VALUES (176, 'courseadmin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-05-14 21:44:17');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1154 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1062, 1, '/system/user', 'menuItem', 'C', '0', 'system:user:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-22 20:05:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', 'system:role:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', 'system:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', 'system:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', 'system:post:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1062, 8, '/system/notice', 'menuItem', 'C', '0', 'system:notice:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2020-05-13 10:45:38', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 3, '/monitor/server', '', 'C', '0', 'monitor:server:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '表单构建', 3, 1, '/tool/build', '', 'C', '0', 'tool:build:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (114, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', 'tool:gen:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES (115, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', 'tool:swagger:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', 'system:user:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', 'system:user:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', 'system:role:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', 'system:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', 'system:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', 'system:post:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', 'system:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', 'monitor:logininfor:unlock', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', 'monitor:job:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 114, 1, '#', '', 'F', '0', 'tool:gen:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 114, 2, '#', '', 'F', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 114, 3, '#', '', 'F', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 114, 4, '#', '', 'F', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 114, 5, '#', '', 'F', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1062, '培训班招生系统管理', 0, 4, '#', 'menuItem', 'M', '0', '', 'fa fa-bank', 'admin', '2020-04-19 17:42:55', 'admin', '2020-04-22 20:04:37', '');
INSERT INTO `sys_menu` VALUES (1063, '班级课程信息', 1062, 1, '/course/info', 'menuItem', 'C', '1', 'course:info:view', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-04-19 21:59:00', '班级课程信息菜单');
INSERT INTO `sys_menu` VALUES (1064, '班级课程信息查询', 1063, 1, '#', '', 'F', '0', 'course:info:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1065, '班级课程信息新增', 1063, 2, '#', '', 'F', '0', 'course:info:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1066, '班级课程信息修改', 1063, 3, '#', '', 'F', '0', 'course:info:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1067, '班级课程信息删除', 1063, 4, '#', '', 'F', '0', 'course:info:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1068, '班级课程信息导出', 1063, 5, '#', '', 'F', '0', 'course:info:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1069, '班级信息', 1062, 2, '/course/classInfo', '', 'C', '0', 'course:classInfo:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '班级信息菜单');
INSERT INTO `sys_menu` VALUES (1070, '班级信息查询', 1069, 1, '#', '', 'F', '0', 'course:classInfo:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1071, '班级信息新增', 1069, 2, '#', '', 'F', '0', 'course:classInfo:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1072, '班级信息修改', 1069, 3, '#', '', 'F', '0', 'course:classInfo:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1073, '班级信息删除', 1069, 4, '#', '', 'F', '0', 'course:classInfo:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1074, '班级信息导出', 1069, 5, '#', '', 'F', '0', 'course:classInfo:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1075, '课程信息', 1062, 3, '/course/course', '', 'C', '0', 'course:course:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '课程信息菜单');
INSERT INTO `sys_menu` VALUES (1076, '课程信息查询', 1075, 1, '#', '', 'F', '0', 'course:course:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1077, '课程信息新增', 1075, 2, '#', '', 'F', '0', 'course:course:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1078, '课程信息修改', 1075, 3, '#', '', 'F', '0', 'course:course:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1079, '课程信息删除', 1075, 4, '#', '', 'F', '0', 'course:course:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1080, '课程信息导出', 1075, 5, '#', '', 'F', '0', 'course:course:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1087, '课程类别信息', 1062, 4, '/course/courseType', '', 'C', '0', 'course:courseType:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '课程类别信息菜单');
INSERT INTO `sys_menu` VALUES (1088, '课程类别信息查询', 1087, 1, '#', '', 'F', '0', 'course:courseType:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1089, '课程类别信息新增', 1087, 2, '#', '', 'F', '0', 'course:courseType:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1090, '课程类别信息修改', 1087, 3, '#', '', 'F', '0', 'course:courseType:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1091, '课程类别信息删除', 1087, 4, '#', '', 'F', '0', 'course:courseType:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1092, '课程类别信息导出', 1087, 5, '#', '', 'F', '0', 'course:courseType:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1093, '精品课程信息', 1062, 5, '/course/excellentCourse', '', 'C', '0', 'course:excellentCourse:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '精品课程信息菜单');
INSERT INTO `sys_menu` VALUES (1094, '精品课程信息查询', 1093, 1, '#', '', 'F', '0', 'course:excellentCourse:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1095, '精品课程信息新增', 1093, 2, '#', '', 'F', '0', 'course:excellentCourse:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1096, '精品课程信息修改', 1093, 3, '#', '', 'F', '0', 'course:excellentCourse:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1097, '精品课程信息删除', 1093, 4, '#', '', 'F', '0', 'course:excellentCourse:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1098, '精品课程信息导出', 1093, 5, '#', '', 'F', '0', 'course:excellentCourse:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1099, '讨论信息', 1062, 6, '/course/discussion', '', 'C', '0', 'course:discussion:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '讨论信息菜单');
INSERT INTO `sys_menu` VALUES (1100, '讨论信息查询', 1099, 1, '#', '', 'F', '0', 'course:discussion:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1101, '讨论信息新增', 1099, 2, '#', '', 'F', '0', 'course:discussion:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1102, '讨论信息修改', 1099, 3, '#', '', 'F', '0', 'course:discussion:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1103, '讨论信息删除', 1099, 4, '#', '', 'F', '0', 'course:discussion:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1104, '讨论信息导出', 1099, 5, '#', '', 'F', '0', 'course:discussion:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1105, '学员留言信息', 1062, 7, '/course/message', '', 'C', '0', 'course:message:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '学员留言信息菜单');
INSERT INTO `sys_menu` VALUES (1106, '学员留言信息查询', 1105, 1, '#', '', 'F', '0', 'course:message:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1107, '学员留言信息新增', 1105, 2, '#', '', 'F', '0', 'course:message:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1108, '学员留言信息修改', 1105, 3, '#', '', 'F', '0', 'course:message:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1109, '学员留言信息删除', 1105, 4, '#', '', 'F', '0', 'course:message:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1110, '学员留言信息导出', 1105, 5, '#', '', 'F', '0', 'course:message:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1111, '课程评价信息', 1062, 8, '/course/evaluation', '', 'C', '0', 'course:evaluation:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '课程评价信息菜单');
INSERT INTO `sys_menu` VALUES (1112, '课程评价信息查询', 1111, 1, '#', '', 'F', '0', 'course:evaluation:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1113, '课程评价信息新增', 1111, 2, '#', '', 'F', '0', 'course:evaluation:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1114, '课程评价信息修改', 1111, 3, '#', '', 'F', '0', 'course:evaluation:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1115, '课程评价信息删除', 1111, 4, '#', '', 'F', '0', 'course:evaluation:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1116, '课程评价信息导出', 1111, 5, '#', '', 'F', '0', 'course:evaluation:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1117, '学员信息', 1062, 9, '/course/student', '', 'C', '0', 'course:student:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '学员信息菜单');
INSERT INTO `sys_menu` VALUES (1118, '学员信息查询', 1117, 1, '#', '', 'F', '0', 'course:student:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1119, '学员信息新增', 1117, 2, '#', '', 'F', '0', 'course:student:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1120, '学员信息修改', 1117, 3, '#', '', 'F', '0', 'course:student:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1121, '学员信息删除', 1117, 4, '#', '', 'F', '0', 'course:student:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1122, '学员信息导出', 1117, 5, '#', '', 'F', '0', 'course:student:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1123, '学员缴费信息', 1062, 10, '/course/fee', '', 'C', '0', 'course:fee:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '学员缴费信息菜单');
INSERT INTO `sys_menu` VALUES (1124, '学员缴费信息查询', 1123, 1, '#', '', 'F', '0', 'course:fee:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1125, '学员缴费信息新增', 1123, 2, '#', '', 'F', '0', 'course:fee:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1126, '学员缴费信息修改', 1123, 3, '#', '', 'F', '0', 'course:fee:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1127, '学员缴费信息删除', 1123, 4, '#', '', 'F', '0', 'course:fee:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1128, '学员缴费信息导出', 1123, 5, '#', '', 'F', '0', 'course:fee:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1129, '师生风采信息', 1062, 11, '/course/style', '', 'C', '0', 'course:style:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '师生风采信息菜单');
INSERT INTO `sys_menu` VALUES (1130, '师生风采信息查询', 1129, 1, '#', '', 'F', '0', 'course:style:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1131, '师生风采信息新增', 1129, 2, '#', '', 'F', '0', 'course:style:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1132, '师生风采信息修改', 1129, 3, '#', '', 'F', '0', 'course:style:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1133, '师生风采信息删除', 1129, 4, '#', '', 'F', '0', 'course:style:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1134, '师生风采信息导出', 1129, 5, '#', '', 'F', '0', 'course:style:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1135, '课程报名号信息', 1062, 12, '/course/apply', '', 'C', '0', 'course:apply:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '课程报名号信息菜单');
INSERT INTO `sys_menu` VALUES (1136, '课程报名号信息查询', 1135, 1, '#', '', 'F', '0', 'course:apply:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1137, '课程报名号信息新增', 1135, 2, '#', '', 'F', '0', 'course:apply:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1138, '课程报名号信息修改', 1135, 3, '#', '', 'F', '0', 'course:apply:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1139, '课程报名号信息删除', 1135, 4, '#', '', 'F', '0', 'course:apply:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1140, '课程报名号信息导出', 1135, 5, '#', '', 'F', '0', 'course:apply:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1141, '课程预约信息', 1062, 13, '/course/appointment', '', 'C', '0', 'course:appointment:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '课程预约信息菜单');
INSERT INTO `sys_menu` VALUES (1142, '课程预约信息查询', 1141, 1, '#', '', 'F', '0', 'course:appointment:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1143, '课程预约信息新增', 1141, 2, '#', '', 'F', '0', 'course:appointment:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1144, '课程预约信息修改', 1141, 3, '#', '', 'F', '0', 'course:appointment:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1145, '课程预约信息删除', 1141, 4, '#', '', 'F', '0', 'course:appointment:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1146, '课程预约信息导出', 1141, 5, '#', '', 'F', '0', 'course:appointment:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1147, '学员信息展示', 0, 5, '#', 'menuItem', 'M', '0', NULL, 'fa fa-address-card', 'admin', '2020-04-19 21:53:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1148, '课程报名信息', 1062, 15, '/course/personCourse', 'menuItem', 'C', '0', 'course:personCourse:view', '#', 'admin', '2018-03-01 00:00:00', 'admin', '2020-05-14 21:24:50', '个人课程信息菜单');
INSERT INTO `sys_menu` VALUES (1149, '个人课程信息查询', 1148, 1, '#', '', 'F', '0', 'course:personCourse:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1150, '个人课程信息新增', 1148, 2, '#', '', 'F', '0', 'course:personCourse:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1151, '个人课程信息修改', 1148, 3, '#', '', 'F', '0', 'course:personCourse:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1152, '个人课程信息删除', 1148, 4, '#', '', 'F', '0', 'course:personCourse:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1153, '个人课程信息导出', 1148, 5, '#', '', 'F', '0', 'course:personCourse:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (10, '课程涨价通知', '1', '<h1 style=\"text-align: center; \">关于精品课程涨价的通知</h1><p style=\"text-align: left;\">&nbsp; &nbsp; 各位学员:</p><p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;经教务研究决定,所有的精品课程在新一年会有价格调整,请尽快报名，于一周后2020年4月25日涨价，请知悉~</p><p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 科强科技有限公司</p><p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2020年4月18日</p>', '0', 'admin', '2020-04-19 09:37:26', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (11, '公告测试1', '2', '<p>阿斯达阿达达我df安慰发发发萨法萨法萨法萨法</p><p>撒发阿萨法saf&nbsp;算法</p><p>发送发送发送</p><p>分撒按时asf</p>', '0', 'admin', '2020-05-14 00:08:01', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (12, '五一放假通知', '1', '<p><span style=\"font-size: 14px;\">各位同学:</span></p><p><span style=\"font-size: 14px;\">&nbsp; &nbsp; &nbsp; &nbsp;大家好~<span style=\"background-color: rgb(255, 0, 0);\">五一</span>学校放假5天，请知晓~多对多</span></p>', '0', 'courseadmin', '2020-05-14 21:16:49', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 369 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '通知公告', 3, 'com.ruoyi.web.controller.system.SysNoticeController.remove()', 'POST', 1, 'admin', '研发部门', '/system/notice/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 06:36:03');
INSERT INTO `sys_oper_log` VALUES (101, '通知公告', 3, 'com.ruoyi.web.controller.system.SysNoticeController.remove()', 'POST', 1, 'admin', '研发部门', '/system/notice/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 06:36:06');
INSERT INTO `sys_oper_log` VALUES (102, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,3,113,114,1057,1058,1059,1060,1061,115\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 06:38:04');
INSERT INTO `sys_oper_log` VALUES (103, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"张无忌\" ],\r\n  \"deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"13106547896\" ],\r\n  \"email\" : [ \"courseadmin@163.com\" ],\r\n  \"loginName\" : [ \"courseadmin\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"100\" ],\r\n  \"remark\" : [ \"管理员用户信息\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"100\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 06:39:49');
INSERT INTO `sys_oper_log` VALUES (104, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', 'POST', 1, 'courseadmin', '研发部门', '/system/user/profile/updateAvatar', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 07:23:16');
INSERT INTO `sys_oper_log` VALUES (105, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.update()', 'POST', 1, 'courseadmin', '研发部门', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"\" ],\r\n  \"userName\" : [ \"张无忌\" ],\r\n  \"phonenumber\" : [ \"13106547896\" ],\r\n  \"email\" : [ \"courseadmin@163.com\" ],\r\n  \"sex\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 07:23:32');
INSERT INTO `sys_oper_log` VALUES (106, '用户管理', 5, 'com.ruoyi.web.controller.system.SysUserController.export()', 'POST', 1, 'admin', '研发部门', '/system/user/export', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"\" ],\r\n  \"parentId\" : [ \"\" ],\r\n  \"loginName\" : [ \"\" ],\r\n  \"phonenumber\" : [ \"\" ],\r\n  \"status\" : [ \"\" ],\r\n  \"params[beginTime]\" : [ \"\" ],\r\n  \"params[endTime]\" : [ \"\" ],\r\n  \"orderByColumn\" : [ \"createTime\" ],\r\n  \"isAsc\" : [ \"desc\" ]\r\n}', '{\r\n  \"msg\" : \"a2b2a25c-f2a7-42fb-a0fa-ac7e0005e088_用户数据.xlsx\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 08:44:21');
INSERT INTO `sys_oper_log` VALUES (107, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 08:58:40');
INSERT INTO `sys_oper_log` VALUES (108, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 08:58:54');
INSERT INTO `sys_oper_log` VALUES (109, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,3,113,114,1057,1058,1059,1060,1061,115\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 08:58:59');
INSERT INTO `sys_oper_log` VALUES (110, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"辅导员\" ],\r\n  \"roleKey\" : [ \"fudaoyuan\" ],\r\n  \"roleSort\" : [ \"5\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:29:51');
INSERT INTO `sys_oper_log` VALUES (111, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"101\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"科强技术有限公司\" ],\r\n  \"deptName\" : [ \"北京总公司\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"leader\" : [ \"王科强\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:30:29');
INSERT INTO `sys_oper_log` VALUES (112, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"102\" ],\r\n  \"parentId\" : [ \"100\" ],\r\n  \"parentName\" : [ \"科强技术有限公司\" ],\r\n  \"deptName\" : [ \"郑州分公司\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"王天诺\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"tianruo@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:30:55');
INSERT INTO `sys_oper_log` VALUES (113, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"107\" ],\r\n  \"parentId\" : [ \"101\" ],\r\n  \"parentName\" : [ \"北京总公司\" ],\r\n  \"deptName\" : [ \"教学部门\" ],\r\n  \"orderNum\" : [ \"5\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:31:18');
INSERT INTO `sys_oper_log` VALUES (114, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"102\" ],\r\n  \"deptName\" : [ \"教学部门\" ],\r\n  \"orderNum\" : [ \"3\" ],\r\n  \"leader\" : [ \"王天诺\" ],\r\n  \"phone\" : [ \"15962541478\" ],\r\n  \"email\" : [ \"tiannuo@163.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:31:55');
INSERT INTO `sys_oper_log` VALUES (115, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/system/dept/remove/105', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"部门存在用户,不允许删除\",\r\n  \"code\" : 301\r\n}', 0, NULL, '2020-04-19 09:32:24');
INSERT INTO `sys_oper_log` VALUES (116, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '研发部门', '/system/dept/remove/105', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"部门存在用户,不允许删除\",\r\n  \"code\" : 301\r\n}', 0, NULL, '2020-04-19 09:32:57');
INSERT INTO `sys_oper_log` VALUES (117, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"2\" ],\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"若依\" ],\r\n  \"dept.deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"15666666666\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"loginName\" : [ \"ry\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"2\" ],\r\n  \"remark\" : [ \"测试员\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:33:12');
INSERT INTO `sys_oper_log` VALUES (118, '部门管理', 2, 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"109\" ],\r\n  \"parentId\" : [ \"102\" ],\r\n  \"parentName\" : [ \"郑州分公司\" ],\r\n  \"deptName\" : [ \"后勤管理部门\" ],\r\n  \"orderNum\" : [ \"2\" ],\r\n  \"leader\" : [ \"若依\" ],\r\n  \"phone\" : [ \"15888888888\" ],\r\n  \"email\" : [ \"ry@qq.com\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:33:38');
INSERT INTO `sys_oper_log` VALUES (119, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/notice/add', '127.0.0.1', '内网IP', '{\r\n  \"noticeTitle\" : [ \"课程涨价通知\" ],\r\n  \"noticeType\" : [ \"1\" ],\r\n  \"noticeContent\" : [ \"<h1 style=\\\"text-align: center; \\\">关于精品课程涨价的通知</h1><p style=\\\"text-align: left;\\\">&nbsp; &nbsp; 各位学员:</p><p style=\\\"text-align: left;\\\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;经教务研究决定,所有的精品课程在新一年会有价格调整,请尽快报名，于一周后2020年4月25日涨价，请知悉~</p><p style=\\\"text-align: left;\\\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 科强科技有限公司</p><p style=\\\"text-align: left;\\\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2020年4月18日</p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 09:37:26');
INSERT INTO `sys_oper_log` VALUES (120, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\r\n  \"tables\" : [ \"wct_student,wct_class_course_info,wct_message,wct_excellent_course,wct_teacher_style,wct_discussion,wct_student_fee,wct_course_type,wct_course_evaluation,wct_person_course_info,wct_course_appointment,wct_course_apply,wct_course,wct_class_info\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 17:40:47');
INSERT INTO `sys_oper_log` VALUES (121, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_class_course_info', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 17:41:05');
INSERT INTO `sys_oper_log` VALUES (122, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"课程系统管理\" ],\r\n  \"url\" : [ \"\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-bank\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 17:42:55');
INSERT INTO `sys_oper_log` VALUES (123, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,3,113,114,1057,1058,1059,1060,1061,115,1062\" ]\r\n}', 'null', 1, '不允许操作超级管理员角色', '2020-04-19 17:43:22');
INSERT INTO `sys_oper_log` VALUES (124, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,1062\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 17:43:36');
INSERT INTO `sys_oper_log` VALUES (125, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 17:43:47');
INSERT INTO `sys_oper_log` VALUES (126, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"102\" ],\r\n  \"roleName\" : [ \"辅导员\" ],\r\n  \"roleKey\" : [ \"fudaoyuan\" ],\r\n  \"roleSort\" : [ \"5\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 17:43:53');
INSERT INTO `sys_oper_log` VALUES (127, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"1\" ],\r\n  \"tableName\" : [ \"wct_class_course_info\" ],\r\n  \"tableComment\" : [ \"班级课程信息表\" ],\r\n  \"className\" : [ \"ClassCourseInfo\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"1\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"2\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"班级编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"classId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"3\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"班级名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"className\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"4\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"课程编号\" ],\r\n  \"columns[3].javaType\" : [ \"Long\" ],\r\n  \"columns[3].javaField\" : [ \"courseId\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"5\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ ', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 17:46:51');
INSERT INTO `sys_oper_log` VALUES (128, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_class_course_info', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 17:47:05');
INSERT INTO `sys_oper_log` VALUES (129, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"menuIds\" : [ \"1062\" ]\r\n}', 'null', 1, '不允许操作超级管理员角色', '2020-04-19 17:48:51');
INSERT INTO `sys_oper_log` VALUES (130, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"1063\" ],\r\n  \"parentId\" : [ \"1062\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"班级课程信息\" ],\r\n  \"url\" : [ \"/course/info\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"course:info:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 17:59:31');
INSERT INTO `sys_oper_log` VALUES (131, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"2\" ],\r\n  \"tableName\" : [ \"wct_class_info\" ],\r\n  \"tableComment\" : [ \"班级信息表\" ],\r\n  \"className\" : [ \"ClassInfo\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"7\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"8\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"班级名称\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"name\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"9\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"班级描述\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"introduce\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"textarea\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"10\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"辅导员编号\" ],\r\n  \"columns[3].javaType\" : [ \"Long\" ],\r\n  \"columns[3].javaField\" : [ \"teacherId\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"11\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"班级辅导员老师\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"teach', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:01:17');
INSERT INTO `sys_oper_log` VALUES (132, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_class_info', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 18:01:21');
INSERT INTO `sys_oper_log` VALUES (133, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"张洋\" ],\r\n  \"deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"13152147852\" ],\r\n  \"email\" : [ \"zhangyang@163.com\" ],\r\n  \"loginName\" : [ \"zhangyang\" ],\r\n  \"password\" : [ \"zhangyang\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:09:22');
INSERT INTO `sys_oper_log` VALUES (134, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"班级状态字典\" ],\r\n  \"dictType\" : [ \"wct_class_staus\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"班级状态的数据字典\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:21:11');
INSERT INTO `sys_oper_log` VALUES (135, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"正常\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_class_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:21:49');
INSERT INTO `sys_oper_log` VALUES (136, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"结束\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_class_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:22:11');
INSERT INTO `sys_oper_log` VALUES (137, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/edit', '127.0.0.1', '内网IP', '{\r\n  \"dictCode\" : [ \"31\" ],\r\n  \"dictLabel\" : [ \"毕业\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_class_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:22:24');
INSERT INTO `sys_oper_log` VALUES (138, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"停止\" ],\r\n  \"dictValue\" : [ \"3\" ],\r\n  \"dictType\" : [ \"wct_class_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:22:47');
INSERT INTO `sys_oper_log` VALUES (139, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"3\" ],\r\n  \"tableName\" : [ \"wct_course\" ],\r\n  \"tableComment\" : [ \"课程信息表\" ],\r\n  \"className\" : [ \"WctCourse\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"14\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"15\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"name\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"16\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程介绍\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"introduce\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"17\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"课程图片\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"picture\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"textarea\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"18\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"课程分类编号\" ],\r\n  \"columns[4].javaType\" : [ \"Long\" ],\r\n  \"columns[4].javaField\" : [ \"typeId\" ],', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:26:37');
INSERT INTO `sys_oper_log` VALUES (140, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 18:26:41');
INSERT INTO `sys_oper_log` VALUES (141, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"3\" ],\r\n  \"tableName\" : [ \"wct_course\" ],\r\n  \"tableComment\" : [ \"课程信息表\" ],\r\n  \"className\" : [ \"Course\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"14\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"15\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"name\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"16\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程介绍\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"introduce\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"17\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"课程图片\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"picture\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"textarea\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"18\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"课程分类编号\" ],\r\n  \"columns[4].javaType\" : [ \"Long\" ],\r\n  \"columns[4].javaField\" : [ \"typeId\" ],\r\n ', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:30:14');
INSERT INTO `sys_oper_log` VALUES (142, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 18:30:20');
INSERT INTO `sys_oper_log` VALUES (143, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"7\" ],\r\n  \"tableName\" : [ \"wct_course_type\" ],\r\n  \"tableComment\" : [ \"课程类别信息表\" ],\r\n  \"className\" : [ \"CourseType\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"61\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"62\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程类型名称\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"typeName\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"63\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"创建人\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"createUser\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"64\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"创建时间\" ],\r\n  \"columns[3].javaType\" : [ \"Date\" ],\r\n  \"columns[3].javaField\" : [ \"createTime\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"datetime\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"65\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"修改人\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"updateUser\" ],\r\n  \"columns[4].isInsert\" : [ \"1\" ],\r\n  \"columns[4', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:34:48');
INSERT INTO `sys_oper_log` VALUES (144, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course_type', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 18:34:53');
INSERT INTO `sys_oper_log` VALUES (145, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"7\" ],\r\n  \"tableName\" : [ \"wct_course_type\" ],\r\n  \"tableComment\" : [ \"课程类别信息表\" ],\r\n  \"className\" : [ \"CourseType\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"61\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"62\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程类型名称\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"typeName\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"63\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"创建人\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"createUser\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"64\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"创建时间\" ],\r\n  \"columns[3].javaType\" : [ \"Date\" ],\r\n  \"columns[3].javaField\" : [ \"createTime\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"datetime\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"65\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"修改人\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"updateUser\" ],\r\n  \"columns[4].isInsert\" : [ \"1\" ],\r\n  \"columns[4', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:43:37');
INSERT INTO `sys_oper_log` VALUES (146, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course_type', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 18:43:43');
INSERT INTO `sys_oper_log` VALUES (147, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"9\" ],\r\n  \"tableName\" : [ \"wct_excellent_course\" ],\r\n  \"tableComment\" : [ \"精品课程信息表\" ],\r\n  \"className\" : [ \"ExcellentCourse\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"72\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"73\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"精品课程名称\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"courseName\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"74\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程图片\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"picture\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"textarea\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"75\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"课程视频\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"video\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"textarea\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"76\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"课程简介\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"introduce\" ],\r\n  \"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:51:52');
INSERT INTO `sys_oper_log` VALUES (148, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"wct_coursr_status\" ],\r\n  \"dictType\" : [ \"课程状态字典\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:52:29');
INSERT INTO `sys_oper_log` VALUES (149, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"正常\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"课程状态字典\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:52:49');
INSERT INTO `sys_oper_log` VALUES (150, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"停课\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"课程状态字典\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:53:07');
INSERT INTO `sys_oper_log` VALUES (151, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/edit', '127.0.0.1', '内网IP', '{\r\n  \"dictCode\" : [ \"33\" ],\r\n  \"dictLabel\" : [ \"开课\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"课程状态字典\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:53:15');
INSERT INTO `sys_oper_log` VALUES (152, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"学员状态字典\" ],\r\n  \"dictType\" : [ \"wct_student_status\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"学员状态字典\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:54:50');
INSERT INTO `sys_oper_log` VALUES (153, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/edit', '127.0.0.1', '内网IP', '{\r\n  \"dictId\" : [ \"12\" ],\r\n  \"dictName\" : [ \"课程状态字典\" ],\r\n  \"dictType\" : [ \"wct_coursr_status\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"课程状态字典\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:55:01');
INSERT INTO `sys_oper_log` VALUES (154, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"正常\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_student_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:55:22');
INSERT INTO `sys_oper_log` VALUES (155, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"冻结\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_student_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:55:36');
INSERT INTO `sys_oper_log` VALUES (156, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"失效\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_student_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:55:50');
INSERT INTO `sys_oper_log` VALUES (157, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"删除\" ],\r\n  \"dictValue\" : [ \"0\" ],\r\n  \"dictType\" : [ \"wct_student_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"4\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 18:56:04');
INSERT INTO `sys_oper_log` VALUES (158, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"师生风采状态字典\" ],\r\n  \"dictType\" : [ \"wct_teacher_style\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"师生风采状态字典\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:00:22');
INSERT INTO `sys_oper_log` VALUES (159, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"正常\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_teacher_style\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:00:38');
INSERT INTO `sys_oper_log` VALUES (160, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"失效\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_teacher_style\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:01:23');
INSERT INTO `sys_oper_log` VALUES (161, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"失效\" ],\r\n  \"dictValue\" : [ \"3\" ],\r\n  \"dictType\" : [ \"wct_coursr_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:02:53');
INSERT INTO `sys_oper_log` VALUES (162, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/edit', '127.0.0.1', '内网IP', '{\r\n  \"dictCode\" : [ \"33\" ],\r\n  \"dictLabel\" : [ \"正常\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_coursr_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:03:04');
INSERT INTO `sys_oper_log` VALUES (163, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"学生课程支付状态\" ],\r\n  \"dictType\" : [ \"wct_pay_staus\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"学生课程支付状态\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:07:26');
INSERT INTO `sys_oper_log` VALUES (164, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"支付成功\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_pay_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:07:40');
INSERT INTO `sys_oper_log` VALUES (165, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"支付成功\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_pay_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:08:00');
INSERT INTO `sys_oper_log` VALUES (166, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/edit', '127.0.0.1', '内网IP', '{\r\n  \"dictCode\" : [ \"42\" ],\r\n  \"dictLabel\" : [ \"待支付\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_pay_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:08:08');
INSERT INTO `sys_oper_log` VALUES (167, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"退款\" ],\r\n  \"dictValue\" : [ \"3\" ],\r\n  \"dictType\" : [ \"wct_pay_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:08:31');
INSERT INTO `sys_oper_log` VALUES (168, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"学生课程预约状态\" ],\r\n  \"dictType\" : [ \"wct_course_evaluation_status\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"学生课程预约状态\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:11:09');
INSERT INTO `sys_oper_log` VALUES (169, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"预约成功 \" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_course_evaluation_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:19:29');
INSERT INTO `sys_oper_log` VALUES (170, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"取消预约 \" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_course_evaluation_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:19:52');
INSERT INTO `sys_oper_log` VALUES (171, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"删除预约\" ],\r\n  \"dictValue\" : [ \"3\" ],\r\n  \"dictType\" : [ \"wct_course_evaluation_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 19:20:23');
INSERT INTO `sys_oper_log` VALUES (172, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"9\" ],\r\n  \"tableName\" : [ \"wct_excellent_course\" ],\r\n  \"tableComment\" : [ \"精品课程信息表\" ],\r\n  \"className\" : [ \"ExcellentCourse\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"72\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"73\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"精品课程名称\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"courseName\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"74\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程图片\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"picture\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"textarea\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"75\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"课程视频\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"video\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"textarea\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"76\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"课程简介\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"introduce\" ],\r\n  \"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:34:07');
INSERT INTO `sys_oper_log` VALUES (173, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_excellent_course', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 20:34:13');
INSERT INTO `sys_oper_log` VALUES (174, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"8\" ],\r\n  \"tableName\" : [ \"wct_discussion\" ],\r\n  \"tableComment\" : [ \"讨论信息表\" ],\r\n  \"className\" : [ \"Discussion\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"67\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"68\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"讨论的标题\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"title\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"textarea\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"69\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"讨论的内容\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"content\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"70\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"发布人\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"user\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"LIKE\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"71\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"发布时间\" ],\r\n  \"columns[4].javaType\" : [ \"Date\" ],\r\n  \"columns[4].javaField\" : [ \"createTi', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:43:52');
INSERT INTO `sys_oper_log` VALUES (175, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_discussion', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 20:43:58');
INSERT INTO `sys_oper_log` VALUES (176, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"10\" ],\r\n  \"tableName\" : [ \"wct_message\" ],\r\n  \"tableComment\" : [ \"学员留言信息表\" ],\r\n  \"className\" : [ \"Message\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"82\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"83\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"留言信内容\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"content\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"84\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"学员编号\" ],\r\n  \"columns[2].javaType\" : [ \"Long\" ],\r\n  \"columns[2].javaField\" : [ \"studentId\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"85\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"学员姓名\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"studentName\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"LIKE\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"86\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"留言对象\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"messa', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:50:20');
INSERT INTO `sys_oper_log` VALUES (177, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"留言信息状态\" ],\r\n  \"dictType\" : [ \"wct_message_status\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:51:01');
INSERT INTO `sys_oper_log` VALUES (178, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"未读\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"wct_message_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:52:31');
INSERT INTO `sys_oper_log` VALUES (179, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"已读\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"wct_message_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:52:44');
INSERT INTO `sys_oper_log` VALUES (180, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"已回复\" ],\r\n  \"dictValue\" : [ \"3\" ],\r\n  \"dictType\" : [ \"wct_message_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:52:54');
INSERT INTO `sys_oper_log` VALUES (181, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"删除\" ],\r\n  \"dictValue\" : [ \"4\" ],\r\n  \"dictType\" : [ \"wct_message_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"4\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:53:05');
INSERT INTO `sys_oper_log` VALUES (182, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"10\" ],\r\n  \"tableName\" : [ \"wct_message\" ],\r\n  \"tableComment\" : [ \"学员留言信息表\" ],\r\n  \"className\" : [ \"Message\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"82\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"83\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"留言信内容\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"content\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"84\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"学员编号\" ],\r\n  \"columns[2].javaType\" : [ \"Long\" ],\r\n  \"columns[2].javaField\" : [ \"studentId\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"85\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"学员姓名\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"studentName\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"LIKE\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"86\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"留言对象\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"messa', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 20:54:55');
INSERT INTO `sys_oper_log` VALUES (183, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_message', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 20:55:01');
INSERT INTO `sys_oper_log` VALUES (184, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"6\" ],\r\n  \"tableName\" : [ \"wct_course_evaluation\" ],\r\n  \"tableComment\" : [ \"课程评价信息表\" ],\r\n  \"className\" : [ \"WctCourseEvaluation\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"53\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"54\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"courseCode\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"55\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"courseNam2\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"56\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"授课教师\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"teacherName\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"LIKE\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"57\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"评价分数\" ],\r\n  \"columns[4].j', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:03:02');
INSERT INTO `sys_oper_log` VALUES (185, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"6\" ],\r\n  \"tableName\" : [ \"wct_course_evaluation\" ],\r\n  \"tableComment\" : [ \"课程评价信息表\" ],\r\n  \"className\" : [ \"CourseEvaluation\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"53\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"54\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"courseCode\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"55\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"courseNam2\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"56\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"授课教师\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"teacherName\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"LIKE\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"57\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"评价分数\" ],\r\n  \"columns[4].java', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:03:33');
INSERT INTO `sys_oper_log` VALUES (186, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"6\" ],\r\n  \"tableName\" : [ \"wct_course_evaluation\" ],\r\n  \"tableComment\" : [ \"课程评价信息表\" ],\r\n  \"className\" : [ \"CourseEvaluation\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"53\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"54\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"courseCode\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"55\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"courseNam2\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"56\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"授课教师\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"teacherName\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"LIKE\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"57\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"评价分数\" ],\r\n  \"columns[4].java', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:04:01');
INSERT INTO `sys_oper_log` VALUES (187, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course_evaluation', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 21:04:07');
INSERT INTO `sys_oper_log` VALUES (188, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"12\" ],\r\n  \"tableName\" : [ \"wct_student\" ],\r\n  \"tableComment\" : [ \"学员信息表\" ],\r\n  \"className\" : [ \"Student\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"99\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"100\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"姓名\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"name\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"101\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"账号\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"account\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"102\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"密码\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"password\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"103\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"邮件\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"email\" ],\r\n  \"columns[4].isInsert\" : [ \"1\" ],\r\n  \"co', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:13:13');
INSERT INTO `sys_oper_log` VALUES (189, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_student', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 21:13:19');
INSERT INTO `sys_oper_log` VALUES (190, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"13\" ],\r\n  \"tableName\" : [ \"wct_student_fee\" ],\r\n  \"tableComment\" : [ \"学员缴费信息表\" ],\r\n  \"className\" : [ \"StudentFee\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"118\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"119\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"支付方式\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"method\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"select\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"120\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"支付金额\" ],\r\n  \"columns[2].javaType\" : [ \"Long\" ],\r\n  \"columns[2].javaField\" : [ \"money\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"BETWEEN\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"121\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"支付时间\" ],\r\n  \"columns[3].javaType\" : [ \"Date\" ],\r\n  \"columns[3].javaField\" : [ \"payTime\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"BETWEEN\" ],\r\n  \"columns[3].htmlType\" : [ \"datetime\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"122\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"课程编号', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:20:35');
INSERT INTO `sys_oper_log` VALUES (191, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_student_fee', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 21:20:40');
INSERT INTO `sys_oper_log` VALUES (192, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"14\" ],\r\n  \"tableName\" : [ \"wct_teacher_style\" ],\r\n  \"tableComment\" : [ \"师生风采信息表\" ],\r\n  \"className\" : [ \"TeacherStyle\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"126\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"127\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"标题\" ],\r\n  \"columns[1].javaType\" : [ \"String\" ],\r\n  \"columns[1].javaField\" : [ \"title\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].isQuery\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"LIKE\" ],\r\n  \"columns[1].htmlType\" : [ \"textarea\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"128\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"风采的图片\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"picture\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"EQ\" ],\r\n  \"columns[2].htmlType\" : [ \"textarea\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"129\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"风采的视频\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"video\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"textarea\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"130\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"说明\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaFie', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:30:32');
INSERT INTO `sys_oper_log` VALUES (193, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_teacher_style', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 21:30:39');
INSERT INTO `sys_oper_log` VALUES (194, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"4\" ],\r\n  \"tableName\" : [ \"wct_course_apply\" ],\r\n  \"tableComment\" : [ \"课程报名号信息表\" ],\r\n  \"className\" : [ \"CourseApply\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"29\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"30\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"学员编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"studentId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"31\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"学员姓名\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"studentName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"32\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"学员电话\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"phone\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"33\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"班级编号\" ],\r\n  \"columns[4].javaType\" : [ \"Long\" ],\r\n  \"columns[4].javaField\" : [ \"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:37:50');
INSERT INTO `sys_oper_log` VALUES (195, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course_apply', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 21:38:18');
INSERT INTO `sys_oper_log` VALUES (196, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"5\" ],\r\n  \"tableName\" : [ \"wct_course_appointment\" ],\r\n  \"tableComment\" : [ \"课程预约信息表\" ],\r\n  \"className\" : [ \"CourseAppointment\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"41\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"42\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"courseId\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"43\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"courseName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"44\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"学员编号\" ],\r\n  \"columns[3].javaType\" : [ \"Long\" ],\r\n  \"columns[3].javaField\" : [ \"studentId\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"45\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"学员名称\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].java', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:43:49');
INSERT INTO `sys_oper_log` VALUES (197, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course_appointment', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 21:44:21');
INSERT INTO `sys_oper_log` VALUES (198, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"11\" ],\r\n  \"tableName\" : [ \"wct_person_course_info\" ],\r\n  \"tableComment\" : [ \"个人课程信息表\" ],\r\n  \"className\" : [ \"PersonCourseInfo\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"92\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"93\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"学生编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"studentCode\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"94\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"学生名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"studentName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"95\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"课程编号\" ],\r\n  \"columns[3].javaType\" : [ \"Long\" ],\r\n  \"columns[3].javaField\" : [ \"courseId\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"EQ\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"96\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[4].javaType\" : [ \"String\" ],\r\n  \"columns[4].javaField\" : [ \"courseName\" ],\r\n  \"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:51:00');
INSERT INTO `sys_oper_log` VALUES (199, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_person_course_info', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-19 21:51:08');
INSERT INTO `sys_oper_log` VALUES (200, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"学员信息展示\" ],\r\n  \"url\" : [ \"/person\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"5\" ],\r\n  \"icon\" : [ \"fa fa-address-card\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:53:45');
INSERT INTO `sys_oper_log` VALUES (201, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"1063\" ],\r\n  \"parentId\" : [ \"1062\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"班级课程信息\" ],\r\n  \"url\" : [ \"/course/info\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"course:info:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 21:59:00');
INSERT INTO `sys_oper_log` VALUES (202, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"李龙龙\" ],\r\n  \"deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"18514784587\" ],\r\n  \"email\" : [ \"lilonglong@163.com\" ],\r\n  \"loginName\" : [ \"lilonglong\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:00:39');
INSERT INTO `sys_oper_log` VALUES (203, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"张肖荣\" ],\r\n  \"deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"18745214578\" ],\r\n  \"email\" : [ \"zhangxiaor@163.com\" ],\r\n  \"loginName\" : [ \"zhangxiaorong\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"102\" ],\r\n  \"remark\" : [ \"辅助学员信息的 管理\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"102\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:02:26');
INSERT INTO `sys_oper_log` VALUES (204, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '127.0.0.1', '内网IP', '{\r\n  \"deptId\" : [ \"104\" ],\r\n  \"userName\" : [ \"杨倩\" ],\r\n  \"deptName\" : [ \"市场部门\" ],\r\n  \"phonenumber\" : [ \"15632145789\" ],\r\n  \"email\" : [ \"yangqian@163.com\" ],\r\n  \"loginName\" : [ \"yanqian\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"负责招生咨询\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:03:35');
INSERT INTO `sys_oper_log` VALUES (205, '岗位管理', 1, 'com.ruoyi.web.controller.system.SysPostController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/post/add', '127.0.0.1', '内网IP', '{\r\n  \"postName\" : [ \"讲师\" ],\r\n  \"postCode\" : [ \"js\" ],\r\n  \"postSort\" : [ \"5\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"进行课程的讲授\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:04:32');
INSERT INTO `sys_oper_log` VALUES (206, '岗位管理', 1, 'com.ruoyi.web.controller.system.SysPostController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/post/add', '127.0.0.1', '内网IP', '{\r\n  \"postName\" : [ \"助教\" ],\r\n  \"postCode\" : [ \"zj\" ],\r\n  \"postSort\" : [ \"6\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"辅助讲师进行答疑以及问题的解答\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:05:00');
INSERT INTO `sys_oper_log` VALUES (207, '岗位管理', 1, 'com.ruoyi.web.controller.system.SysPostController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/post/add', '127.0.0.1', '内网IP', '{\r\n  \"postName\" : [ \"辅导员\" ],\r\n  \"postCode\" : [ \"fdy\" ],\r\n  \"postSort\" : [ \"7\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"负责学生信息的管理\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:05:26');
INSERT INTO `sys_oper_log` VALUES (208, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"2\" ],\r\n  \"postName\" : [ \"校长\" ],\r\n  \"postCode\" : [ \"xz\" ],\r\n  \"postSort\" : [ \"2\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:06:01');
INSERT INTO `sys_oper_log` VALUES (209, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/post/edit', '127.0.0.1', '内网IP', '{\r\n  \"postId\" : [ \"4\" ],\r\n  \"postName\" : [ \"职员\" ],\r\n  \"postCode\" : [ \"user\" ],\r\n  \"postSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:06:24');
INSERT INTO `sys_oper_log` VALUES (210, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"104\" ],\r\n  \"deptId\" : [ \"104\" ],\r\n  \"userName\" : [ \"杨倩\" ],\r\n  \"dept.deptName\" : [ \"市场部门\" ],\r\n  \"phonenumber\" : [ \"15632145789\" ],\r\n  \"email\" : [ \"yangqian@163.com\" ],\r\n  \"loginName\" : [ \"yanqian\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"负责招生咨询\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:06:39');
INSERT INTO `sys_oper_log` VALUES (211, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"103\" ],\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"张肖荣\" ],\r\n  \"dept.deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"18745214578\" ],\r\n  \"email\" : [ \"zhangxiaor@163.com\" ],\r\n  \"loginName\" : [ \"zhangxiaorong\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"102\" ],\r\n  \"remark\" : [ \"辅助学员信息的 管理\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"102\" ],\r\n  \"postIds\" : [ \"7\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:06:53');
INSERT INTO `sys_oper_log` VALUES (212, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dict/edit', '127.0.0.1', '内网IP', '{\r\n  \"dictId\" : [ \"17\" ],\r\n  \"dictName\" : [ \"留言信息状态\" ],\r\n  \"dictType\" : [ \"wct_message_status\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"留言信息状态\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:08:59');
INSERT INTO `sys_oper_log` VALUES (213, '重置密码', 2, 'com.ruoyi.web.controller.system.SysUserController.resetPwd()', 'GET', 1, 'admin', '研发部门', '/system/user/resetPwd/100', '127.0.0.1', '内网IP', '{ }', '\"system/user/resetPwd\"', 0, NULL, '2020-04-19 22:31:27');
INSERT INTO `sys_oper_log` VALUES (214, '重置密码', 2, 'com.ruoyi.web.controller.system.SysUserController.resetPwdSave()', 'POST', 1, 'admin', '研发部门', '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"100\" ],\r\n  \"loginName\" : [ \"courseadmin\" ],\r\n  \"password\" : [ \"123456\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:31:34');
INSERT INTO `sys_oper_log` VALUES (215, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:33:01');
INSERT INTO `sys_oper_log` VALUES (216, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,1062,1063,1064,1065,1066,1067,1068,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:33:53');
INSERT INTO `sys_oper_log` VALUES (217, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1105,1106,1107,1108,1109,1110,1129,1130,1131,1132,1133,1134\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:34:51');
INSERT INTO `sys_oper_log` VALUES (218, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"102\" ],\r\n  \"roleName\" : [ \"辅导员\" ],\r\n  \"roleKey\" : [ \"fudaoyuan\" ],\r\n  \"roleSort\" : [ \"5\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:35:21');
INSERT INTO `sys_oper_log` VALUES (219, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"学生\" ],\r\n  \"roleKey\" : [ \"student\" ],\r\n  \"roleSort\" : [ \"8\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1147,1148,1149,1150,1151,1152,1153\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:35:53');
INSERT INTO `sys_oper_log` VALUES (220, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"招生办\" ],\r\n  \"roleKey\" : [ \"erroll\" ],\r\n  \"roleSort\" : [ \"6\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:36:51');
INSERT INTO `sys_oper_log` VALUES (221, '课程类别信息', 1, 'com.ruoyi.web.controller.course.CourseTypeController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/courseType/add', '127.0.0.1', '内网IP', '{\r\n  \"typeName\" : [ \"积木\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:41:15');
INSERT INTO `sys_oper_log` VALUES (222, '课程类别信息', 1, 'com.ruoyi.web.controller.course.CourseTypeController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/courseType/add', '127.0.0.1', '内网IP', '{\r\n  \"typeName\" : [ \"机器人\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:41:34');
INSERT INTO `sys_oper_log` VALUES (223, '课程类别信息', 1, 'com.ruoyi.web.controller.course.CourseTypeController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/courseType/add', '127.0.0.1', '内网IP', '{\r\n  \"typeName\" : [ \"绘画\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:41:49');
INSERT INTO `sys_oper_log` VALUES (224, '课程类别信息', 1, 'com.ruoyi.web.controller.course.CourseTypeController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/courseType/add', '127.0.0.1', '内网IP', '{\r\n  \"typeName\" : [ \"编程\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:41:59');
INSERT INTO `sys_oper_log` VALUES (225, '课程类别信息', 1, 'com.ruoyi.web.controller.course.CourseTypeController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/courseType/add', '127.0.0.1', '内网IP', '{\r\n  \"typeName\" : [ \"其他\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:42:03');
INSERT INTO `sys_oper_log` VALUES (226, '课程类别信息', 2, 'com.ruoyi.web.controller.course.CourseTypeController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/courseType/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"typeName\" : [ \"少儿动画教育\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-19 22:42:29');
INSERT INTO `sys_oper_log` VALUES (227, '班级信息', 1, 'com.ruoyi.web.controller.course.ClassInfoController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"儿童画班\" ],\r\n  \"introduce\" : [ \"通过积木教学，开发儿童的智力的班级\" ],\r\n  \"teacherName\" : [ \"张肖荣\" ],\r\n  \"teacherId\" : [ \"103\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 00:51:47');
INSERT INTO `sys_oper_log` VALUES (228, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"101\" ],\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"张洋\" ],\r\n  \"dept.deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"13152147852\" ],\r\n  \"email\" : [ \"zhangyang@163.com\" ],\r\n  \"loginName\" : [ \"zhangyang\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"4,7\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 00:52:13');
INSERT INTO `sys_oper_log` VALUES (229, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.remove()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 01:11:02');
INSERT INTO `sys_oper_log` VALUES (230, '班级信息', 1, 'com.ruoyi.web.controller.course.ClassInfoController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"儿童积木\" ],\r\n  \"introduce\" : [ \"开发儿童智力启发课程\" ],\r\n  \"teacherName\" : [ \"张肖荣\" ],\r\n  \"teacherId\" : [ \"103\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 01:11:48');
INSERT INTO `sys_oper_log` VALUES (231, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/data/edit', '127.0.0.1', '内网IP', '{\r\n  \"dictCode\" : [ \"32\" ],\r\n  \"dictLabel\" : [ \"停班\" ],\r\n  \"dictValue\" : [ \"3\" ],\r\n  \"dictType\" : [ \"wct_class_staus\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 08:10:46');
INSERT INTO `sys_oper_log` VALUES (232, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1069,1070,1071,1072,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1105,1106,1107,1108,1109,1110,1129,1130,1131,1132,1133,1134\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 08:18:20');
INSERT INTO `sys_oper_log` VALUES (233, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"102\" ],\r\n  \"roleName\" : [ \"辅导员\" ],\r\n  \"roleKey\" : [ \"fudaoyuan\" ],\r\n  \"roleSort\" : [ \"5\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1069,1070,1071,1072,1074,1075,1076,1077,1078,1079,1080,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 08:18:30');
INSERT INTO `sys_oper_log` VALUES (234, '讨论信息', 1, 'com.ruoyi.web.controller.course.DiscussionController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/discussion/add', '127.0.0.1', '内网IP', '{\r\n  \"title\" : [ \"画画课程非常的不错\" ],\r\n  \"content\" : [ \"今天老师讲的画画的课程非常好,感觉收货非常多\" ],\r\n  \"user\" : [ \"张洋\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 08:31:18');
INSERT INTO `sys_oper_log` VALUES (235, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1069,1070,1071,1072,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1104,1105,1106,1107,1108,1109,1110,1129,1130,1131,1132,1133,1134\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 08:31:54');
INSERT INTO `sys_oper_log` VALUES (236, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"102\" ],\r\n  \"roleName\" : [ \"辅导员\" ],\r\n  \"roleKey\" : [ \"fudaoyuan\" ],\r\n  \"roleSort\" : [ \"5\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1069,1070,1071,1072,1074,1075,1076,1077,1078,1079,1080,1093,1094,1095,1096,1097,1098,1099,1100,1101,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 08:32:04');
INSERT INTO `sys_oper_log` VALUES (237, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"102\" ],\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"李龙龙\" ],\r\n  \"dept.deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"18514784587\" ],\r\n  \"email\" : [ \"lilonglong@163.com\" ],\r\n  \"loginName\" : [ \"lilonglong\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"5\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 10:47:08');
INSERT INTO `sys_oper_log` VALUES (238, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"101\" ],\r\n  \"deptId\" : [ \"107\" ],\r\n  \"userName\" : [ \"张洋\" ],\r\n  \"dept.deptName\" : [ \"教学部门\" ],\r\n  \"phonenumber\" : [ \"13152147852\" ],\r\n  \"email\" : [ \"zhangyang@163.com\" ],\r\n  \"loginName\" : [ \"zhangyang\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"5,7\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 10:47:28');
INSERT INTO `sys_oper_log` VALUES (239, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/20/c14074ae306d78a4b0e68cc697eb9fcc.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:00:55');
INSERT INTO `sys_oper_log` VALUES (240, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/20/5bdbdfd513aeffce4ed7dd159005499d.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:03:27');
INSERT INTO `sys_oper_log` VALUES (241, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/20/de4f48ce2ad282a16cdeabca37e0e4eb.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:07:14');
INSERT INTO `sys_oper_log` VALUES (242, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"积木初级教学\" ],\r\n  \"introduce\" : [ \"培养孩子智力，提高儿童动手能力\" ],\r\n  \"picture\" : [ \"\" ],\r\n  \"typeName\" : [ \"积木\" ],\r\n  \"typeId\" : [ \"1\" ],\r\n  \"price\" : [ \"380\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"publishTime\" : [ \"2020-04-08\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:16:15');
INSERT INTO `sys_oper_log` VALUES (243, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/20/a380ad2bf7849bbb48356f03071cfff2.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:18:46');
INSERT INTO `sys_oper_log` VALUES (244, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/20/e873d15f1091975dfcf0f5744227336b.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:19:23');
INSERT INTO `sys_oper_log` VALUES (245, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"1\" ],\r\n  \"introduce\" : [ \"1\" ],\r\n  \"picture\" : [ \"\" ],\r\n  \"typeName\" : [ \"积木\" ],\r\n  \"typeId\" : [ \"1\" ],\r\n  \"price\" : [ \"198\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"publishTime\" : [ \"2020-04-08\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:23:40');
INSERT INTO `sys_oper_log` VALUES (246, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/20/ddc34ac8e4548d7003e75dba555de253.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:24:22');
INSERT INTO `sys_oper_log` VALUES (247, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"2\" ],\r\n  \"introduce\" : [ \"2\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/04/20/ddc34ac8e4548d7003e75dba555de253.png\" ],\r\n  \"typeName\" : [ \"其他\" ],\r\n  \"typeId\" : [ \"5\" ],\r\n  \"price\" : [ \"156\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"publishTime\" : [ \"2020-04-06\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:26:35');
INSERT INTO `sys_oper_log` VALUES (248, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"积木3\" ],\r\n  \"introduce\" : [ \"积木3\" ],\r\n  \"picture\" : [ \"\" ],\r\n  \"typeName\" : [ \"积木\" ],\r\n  \"typeId\" : [ \"1\" ],\r\n  \"price\" : [ \"159\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"publishTime\" : [ \"2020-04-09\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:27:13');
INSERT INTO `sys_oper_log` VALUES (249, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/20/d7b2cd9cad6e11f6c4fe6ca72eca3164.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:27:37');
INSERT INTO `sys_oper_log` VALUES (250, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"4\" ],\r\n  \"introduce\" : [ \"4\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/04/20/d7b2cd9cad6e11f6c4fe6ca72eca3164.png\" ],\r\n  \"typeName\" : [ \"积木\" ],\r\n  \"typeId\" : [ \"1\" ],\r\n  \"price\" : [ \"60\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"publishTime\" : [ \"2020-04-15\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:28:55');
INSERT INTO `sys_oper_log` VALUES (251, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"5\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:30:03');
INSERT INTO `sys_oper_log` VALUES (252, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"name\" : [ \"积木2教程\" ],\r\n  \"introduce\" : [ \"积木2教程是针对少年学习的课程\" ],\r\n  \"price\" : [ \"198\" ],\r\n  \"status\" : [ \"3\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:43:30');
INSERT INTO `sys_oper_log` VALUES (253, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"name\" : [ \"积木2教程\" ],\r\n  \"introduce\" : [ \"积木2教程是针对少年学习的课程\" ],\r\n  \"price\" : [ \"198\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"102\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:43:51');
INSERT INTO `sys_oper_log` VALUES (254, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"name\" : [ \"积木2教程\" ],\r\n  \"introduce\" : [ \"积木2教程是针对少年学习的课程\" ],\r\n  \"price\" : [ \"168\" ],\r\n  \"status\" : [ \"3\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"102\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:45:55');
INSERT INTO `sys_oper_log` VALUES (255, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"name\" : [ \"积木2教程\" ],\r\n  \"introduce\" : [ \"积木2教程是针对少年学习的课程\" ],\r\n  \"price\" : [ \"168\" ],\r\n  \"status\" : [ \"3\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"102\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:47:17');
INSERT INTO `sys_oper_log` VALUES (256, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"name\" : [ \"积木2教程\" ],\r\n  \"introduce\" : [ \"积木2教程是针对少年学习的课程\" ],\r\n  \"price\" : [ \"168\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"teacherName\" : [ \"李龙龙\" ],\r\n  \"teacherId\" : [ \"102\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:49:32');
INSERT INTO `sys_oper_log` VALUES (257, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"3\" ],\r\n  \"name\" : [ \"积木学习\" ],\r\n  \"introduce\" : [ \"积木的技术搭建等学习\" ],\r\n  \"price\" : [ \"156\" ],\r\n  \"status\" : [ \"2\" ],\r\n  \"teacherName\" : [ \"李龙龙\" ],\r\n  \"teacherId\" : [ \"102\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:56:50');
INSERT INTO `sys_oper_log` VALUES (258, '课程信息', 5, 'com.ruoyi.web.controller.course.CourseController.export()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/export', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"\" ],\r\n  \"typeName\" : [ \"\" ],\r\n  \"status\" : [ \"\" ],\r\n  \"params[beginPublishTime]\" : [ \"\" ],\r\n  \"params[endPublishTime]\" : [ \"\" ],\r\n  \"teacherName\" : [ \"\" ],\r\n  \"orderByColumn\" : [ \"\" ],\r\n  \"isAsc\" : [ \"asc\" ]\r\n}', '{\r\n  \"msg\" : \"2576d568-94fe-44d2-9ef7-5fd97dca9797_course.xlsx\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 11:57:26');
INSERT INTO `sys_oper_log` VALUES (259, '课程信息', 5, 'com.ruoyi.web.controller.course.CourseController.export()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/export', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"\" ],\r\n  \"typeName\" : [ \"\" ],\r\n  \"status\" : [ \"\" ],\r\n  \"params[beginPublishTime]\" : [ \"\" ],\r\n  \"params[endPublishTime]\" : [ \"\" ],\r\n  \"teacherName\" : [ \"\" ],\r\n  \"orderByColumn\" : [ \"\" ],\r\n  \"isAsc\" : [ \"asc\" ]\r\n}', '{\r\n  \"msg\" : \"c585fdd6-2bd1-4358-a29d-7443853790c3_course.xlsx\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 12:02:35');
INSERT INTO `sys_oper_log` VALUES (260, '班级信息', 1, 'com.ruoyi.web.controller.course.ClassInfoController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"少儿智力开发优秀班\" ],\r\n  \"introduce\" : [ \"提高儿童智力水平，赢在起跑线~\" ],\r\n  \"teacherName\" : [ \"张肖荣\" ],\r\n  \"teacherId\" : [ \"103\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 15:15:00');
INSERT INTO `sys_oper_log` VALUES (261, '班级信息', 1, 'com.ruoyi.web.controller.course.ClassInfoController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"秒秒班\" ],\r\n  \"introduce\" : [ \"3岁以下儿童的托管课程\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-20 15:21:15');
INSERT INTO `sys_oper_log` VALUES (262, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"3\" ],\r\n  \"status\" : [ \"3\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 13:47:24');
INSERT INTO `sys_oper_log` VALUES (263, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"3\" ],\r\n  \"status\" : [ \"3\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 13:47:34');
INSERT INTO `sys_oper_log` VALUES (264, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"4\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 13:48:56');
INSERT INTO `sys_oper_log` VALUES (265, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"4\" ],\r\n  \"status\" : [ \"3\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 13:49:03');
INSERT INTO `sys_oper_log` VALUES (266, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"5\" ],\r\n  \"status\" : [ \"3\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 13:49:07');
INSERT INTO `sys_oper_log` VALUES (267, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"4\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 13:49:11');
INSERT INTO `sys_oper_log` VALUES (268, '上传图片', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/21/0aa766ae27c6689587e7fe6b878e56f3.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:28:49');
INSERT INTO `sys_oper_log` VALUES (269, '上传图片', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"上传图片失败！\",\r\n  \"code\" : 500\r\n}', 0, NULL, '2020-04-21 14:34:59');
INSERT INTO `sys_oper_log` VALUES (270, '上传图片', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/21/8c85cd5df6f1d281681d01fb088abd59.jfif\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:41:29');
INSERT INTO `sys_oper_log` VALUES (271, '上传视频', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadVideo()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadVideo', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"上传视频失败！\",\r\n  \"code\" : 500\r\n}', 0, NULL, '2020-04-21 14:41:45');
INSERT INTO `sys_oper_log` VALUES (272, '上传视频', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadVideo()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadVideo', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/21/0184dffc882d29e44b12b51d1815600e.mkv\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:42:21');
INSERT INTO `sys_oper_log` VALUES (273, '精品课程信息', 1, 'com.ruoyi.web.controller.course.ExcellentCourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/add', '127.0.0.1', '内网IP', '{\r\n  \"courseName\" : [ \"积木教学\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/04/21/8c85cd5df6f1d281681d01fb088abd59.jfif\" ],\r\n  \"video\" : [ \"/profile/upload/2020/04/21/0184dffc882d29e44b12b51d1815600e.mkv\" ],\r\n  \"introduce\" : [ \"精品优秀的课程，值得拥有~\" ],\r\n  \"price\" : [ \"650\" ],\r\n  \"teacherName\" : [ \"张三丰\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:42:58');
INSERT INTO `sys_oper_log` VALUES (274, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"支付方式数据字典\" ],\r\n  \"dictType\" : [ \"pay_type\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"支付方式数据字典\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:47:33');
INSERT INTO `sys_oper_log` VALUES (275, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"支付宝\" ],\r\n  \"dictValue\" : [ \"ZFB\" ],\r\n  \"dictType\" : [ \"pay_type\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:47:51');
INSERT INTO `sys_oper_log` VALUES (276, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"微信\" ],\r\n  \"dictValue\" : [ \"WX\" ],\r\n  \"dictType\" : [ \"pay_type\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:48:04');
INSERT INTO `sys_oper_log` VALUES (277, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"现金\" ],\r\n  \"dictValue\" : [ \"XJ\" ],\r\n  \"dictType\" : [ \"pay_type\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:48:16');
INSERT INTO `sys_oper_log` VALUES (278, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"信用卡\" ],\r\n  \"dictValue\" : [ \"XYK\" ],\r\n  \"dictType\" : [ \"pay_type\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"4\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:48:34');
INSERT INTO `sys_oper_log` VALUES (279, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"储蓄卡\" ],\r\n  \"dictValue\" : [ \"CXK\" ],\r\n  \"dictType\" : [ \"pay_type\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"5\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:49:05');
INSERT INTO `sys_oper_log` VALUES (280, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"其他\" ],\r\n  \"dictValue\" : [ \"QT\" ],\r\n  \"dictType\" : [ \"pay_type\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"6\" ],\r\n  \"listClass\" : [ \"\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 14:49:16');
INSERT INTO `sys_oper_log` VALUES (281, '学员留言信息', 1, 'com.ruoyi.web.controller.course.MessageController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/add', '127.0.0.1', '内网IP', '{\r\n  \"content\" : [ \"今天是否上课\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"张明\" ],\r\n  \"messageObject\" : [ \"系统管理员\" ],\r\n  \"messageTime\" : [ \"2020-03-04\" ],\r\n  \"backContent\" : [ \"\" ],\r\n  \"teacherName\" : [ \"\" ],\r\n  \"backTime\" : [ \"\" ],\r\n  \"status\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:25:14');
INSERT INTO `sys_oper_log` VALUES (282, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:25:28');
INSERT INTO `sys_oper_log` VALUES (283, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"status\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:25:33');
INSERT INTO `sys_oper_log` VALUES (284, '学员留言信息', 1, 'com.ruoyi.web.controller.course.MessageController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/add', '127.0.0.1', '内网IP', '{\r\n  \"content\" : [ \"画画课程有点听不懂\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"张明\" ],\r\n  \"messageObject\" : [ \"肖伟老师\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:27:34');
INSERT INTO `sys_oper_log` VALUES (285, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:27:37');
INSERT INTO `sys_oper_log` VALUES (286, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"backContent\" : [ \"准备明天再去复习一遍~\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:31:48');
INSERT INTO `sys_oper_log` VALUES (287, '学员留言信息', 1, 'com.ruoyi.web.controller.course.MessageController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/add', '127.0.0.1', '内网IP', '{\r\n  \"content\" : [ \"测试留言\" ],\r\n  \"studentId\" : [ \"2\" ],\r\n  \"studentName\" : [ \"李广\" ],\r\n  \"messageObject\" : [ \"小五老师\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:33:47');
INSERT INTO `sys_oper_log` VALUES (288, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"3\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:35:18');
INSERT INTO `sys_oper_log` VALUES (289, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.answeringSaved()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/answeringSaved', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"3\" ],\r\n  \"backContent\" : [ \"好的，感谢留言，稍后我们会关注并给予处理~\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:35:43');
INSERT INTO `sys_oper_log` VALUES (290, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.answeringSaved()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/answeringSaved', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"backContent\" : [ \"后续会再讲解一次\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:36:07');
INSERT INTO `sys_oper_log` VALUES (291, '学员留言信息', 1, 'com.ruoyi.web.controller.course.MessageController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/add', '127.0.0.1', '内网IP', '{\r\n  \"content\" : [ \"今天学习状态不好\" ],\r\n  \"studentId\" : [ \"2\" ],\r\n  \"studentName\" : [ \"李想\" ],\r\n  \"messageObject\" : [ \"小五老师\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:36:57');
INSERT INTO `sys_oper_log` VALUES (292, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"4\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:37:06');
INSERT INTO `sys_oper_log` VALUES (293, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.answeringSaved()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/answeringSaved', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"4\" ],\r\n  \"backContent\" : [ \"好好加油努力~~\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 15:37:16');
INSERT INTO `sys_oper_log` VALUES (294, '课程评价信息', 1, 'com.ruoyi.web.controller.course.CourseEvaluationController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/evaluation/add', '127.0.0.1', '内网IP', '{\r\n  \"courseCode\" : [ \"1\" ],\r\n  \"courseNam2\" : [ \"积木教程\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"score\" : [ \"5\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"张明\" ],\r\n  \"evaluationTime\" : [ \"2020-04-01\" ]\r\n}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown error 1054\r\n### The error may involve com.ruoyi.course.mapper.CourseEvaluationMapper.insertCourseEvaluation-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into wct_course_evaluation          ( course_code,             course_nam2,             teacher_name,             score,             student_id,             student_name,             evaluation_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown error 1054\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown error 1054', '2020-04-21 20:27:13');
INSERT INTO `sys_oper_log` VALUES (295, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'POST', 1, 'admin', '研发部门', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"6\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 20:30:14');
INSERT INTO `sys_oper_log` VALUES (296, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\r\n  \"tables\" : [ \"wct_course_evaluation\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 20:30:19');
INSERT INTO `sys_oper_log` VALUES (297, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\r\n  \"tableId\" : [ \"15\" ],\r\n  \"tableName\" : [ \"wct_course_evaluation\" ],\r\n  \"tableComment\" : [ \"课程评价信息表\" ],\r\n  \"className\" : [ \"CourseEvaluation\" ],\r\n  \"functionAuthor\" : [ \"keqiang\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"columns[0].columnId\" : [ \"136\" ],\r\n  \"columns[0].sort\" : [ \"1\" ],\r\n  \"columns[0].columnComment\" : [ \"编号\" ],\r\n  \"columns[0].javaType\" : [ \"Long\" ],\r\n  \"columns[0].javaField\" : [ \"id\" ],\r\n  \"columns[0].isInsert\" : [ \"1\" ],\r\n  \"columns[0].queryType\" : [ \"EQ\" ],\r\n  \"columns[0].htmlType\" : [ \"input\" ],\r\n  \"columns[0].dictType\" : [ \"\" ],\r\n  \"columns[1].columnId\" : [ \"137\" ],\r\n  \"columns[1].sort\" : [ \"2\" ],\r\n  \"columns[1].columnComment\" : [ \"课程编号\" ],\r\n  \"columns[1].javaType\" : [ \"Long\" ],\r\n  \"columns[1].javaField\" : [ \"courseCode\" ],\r\n  \"columns[1].isInsert\" : [ \"1\" ],\r\n  \"columns[1].isEdit\" : [ \"1\" ],\r\n  \"columns[1].isList\" : [ \"1\" ],\r\n  \"columns[1].queryType\" : [ \"EQ\" ],\r\n  \"columns[1].htmlType\" : [ \"input\" ],\r\n  \"columns[1].dictType\" : [ \"\" ],\r\n  \"columns[2].columnId\" : [ \"138\" ],\r\n  \"columns[2].sort\" : [ \"3\" ],\r\n  \"columns[2].columnComment\" : [ \"课程名称\" ],\r\n  \"columns[2].javaType\" : [ \"String\" ],\r\n  \"columns[2].javaField\" : [ \"courseName\" ],\r\n  \"columns[2].isInsert\" : [ \"1\" ],\r\n  \"columns[2].isEdit\" : [ \"1\" ],\r\n  \"columns[2].isList\" : [ \"1\" ],\r\n  \"columns[2].isQuery\" : [ \"1\" ],\r\n  \"columns[2].queryType\" : [ \"LIKE\" ],\r\n  \"columns[2].htmlType\" : [ \"input\" ],\r\n  \"columns[2].dictType\" : [ \"\" ],\r\n  \"columns[3].columnId\" : [ \"139\" ],\r\n  \"columns[3].sort\" : [ \"4\" ],\r\n  \"columns[3].columnComment\" : [ \"授课教师\" ],\r\n  \"columns[3].javaType\" : [ \"String\" ],\r\n  \"columns[3].javaField\" : [ \"teacherName\" ],\r\n  \"columns[3].isInsert\" : [ \"1\" ],\r\n  \"columns[3].isEdit\" : [ \"1\" ],\r\n  \"columns[3].isList\" : [ \"1\" ],\r\n  \"columns[3].isQuery\" : [ \"1\" ],\r\n  \"columns[3].queryType\" : [ \"LIKE\" ],\r\n  \"columns[3].htmlType\" : [ \"input\" ],\r\n  \"columns[3].dictType\" : [ \"\" ],\r\n  \"columns[4].columnId\" : [ \"140\" ],\r\n  \"columns[4].sort\" : [ \"5\" ],\r\n  \"columns[4].columnComment\" : [ \"评价分数\" ],\r\n  \"columns[4', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 20:31:25');
INSERT INTO `sys_oper_log` VALUES (298, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/wct_course_evaluation', '127.0.0.1', '内网IP', '{ }', 'null', 0, NULL, '2020-04-21 20:31:29');
INSERT INTO `sys_oper_log` VALUES (299, '课程评价信息', 1, 'com.ruoyi.web.controller.course.CourseEvaluationController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/evaluation/add', '127.0.0.1', '内网IP', '{\r\n  \"courseCode\" : [ \"1\" ],\r\n  \"courseName\" : [ \"积木教学\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"score\" : [ \"5\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"张明\" ],\r\n  \"evaluationTime\" : [ \"2020-04-01\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 20:35:55');
INSERT INTO `sys_oper_log` VALUES (300, '课程评价信息', 5, 'com.ruoyi.web.controller.course.CourseEvaluationController.export()', 'POST', 1, 'courseadmin', '研发部门', '/course/evaluation/export', '127.0.0.1', '内网IP', '{\r\n  \"courseName\" : [ \"\" ],\r\n  \"teacherName\" : [ \"\" ],\r\n  \"studentName\" : [ \"\" ],\r\n  \"params[beginEvaluationTime]\" : [ \"\" ],\r\n  \"params[endEvaluationTime]\" : [ \"\" ],\r\n  \"orderByColumn\" : [ \"\" ],\r\n  \"isAsc\" : [ \"asc\" ]\r\n}', '{\r\n  \"msg\" : \"42d66692-65d9-4ad9-a7d6-f3a1b34fbc7b_evaluation.xlsx\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-21 20:36:05');
INSERT INTO `sys_oper_log` VALUES (301, '学员信息', 1, 'com.ruoyi.web.controller.course.StudentController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/student/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"李明\" ],\r\n  \"account\" : [ \"liming\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"email\" : [ \"sadasd@163.com\" ],\r\n  \"phone\" : [ \"16541254784\" ],\r\n  \"qq\" : [ \"124456\" ],\r\n  \"wechat\" : [ \"123456\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"img\" : [ \"\" ],\r\n  \"birthday\" : [ \"1995-10-15\" ],\r\n  \"idcard\" : [ \"1234567687984542523\" ],\r\n  \"address\" : [ \"北京市西城区\" ],\r\n  \"className\" : [ \"画画一班\" ],\r\n  \"classId\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 09:00:49');
INSERT INTO `sys_oper_log` VALUES (302, '上传头像信息信息', 2, 'com.ruoyi.web.controller.course.StudentController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/student/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/avatar/2020/04/22/e3875894e8d8354a4f584605f666733a.jfif\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 09:03:25');
INSERT INTO `sys_oper_log` VALUES (303, '学员信息', 1, 'com.ruoyi.web.controller.course.StudentController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/student/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"张明\" ],\r\n  \"account\" : [ \"zhangming\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"email\" : [ \"zhangming@163.com\" ],\r\n  \"phone\" : [ \"15478541241\" ],\r\n  \"qq\" : [ \"12457874141\" ],\r\n  \"wechat\" : [ \"12457874141\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"img\" : [ \"/profile/avatar/2020/04/22/e3875894e8d8354a4f584605f666733a.jfif\" ],\r\n  \"birthday\" : [ \"1990-02-08\" ],\r\n  \"idcard\" : [ \"133333333333\" ],\r\n  \"address\" : [ \"sdfasfasfasf\" ],\r\n  \"className\" : [ \"画画一班\" ],\r\n  \"classId\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 09:03:45');
INSERT INTO `sys_oper_log` VALUES (304, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"1062\" ],\r\n  \"parentId\" : [ \"0\" ],\r\n  \"menuType\" : [ \"M\" ],\r\n  \"menuName\" : [ \"培训班招生系统管理\" ],\r\n  \"url\" : [ \"#\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"\" ],\r\n  \"orderNum\" : [ \"4\" ],\r\n  \"icon\" : [ \"fa fa-bank\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:04:37');
INSERT INTO `sys_oper_log` VALUES (305, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"100\" ],\r\n  \"parentId\" : [ \"1062\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"用户管理\" ],\r\n  \"url\" : [ \"/system/user\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"system:user:view\" ],\r\n  \"orderNum\" : [ \"1\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:05:00');
INSERT INTO `sys_oper_log` VALUES (306, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"招生系统管理员\" ],\r\n  \"roleKey\" : [ \"zhaosheng\" ],\r\n  \"roleSort\" : [ \"8\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1063,1064,1065,1066,1067,1068,100,1000,1001,1002,1003,1004,1005,1006,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:06:47');
INSERT INTO `sys_oper_log` VALUES (307, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"101\" ],\r\n  \"roleName\" : [ \"代课教师\" ],\r\n  \"roleKey\" : [ \"teacher\" ],\r\n  \"roleSort\" : [ \"4\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1069,1070,1071,1072,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1104,1105,1106,1107,1108,1109,1110,1129,1130,1131,1132,1133,1134,1135,1136,1140,1141,1142,1146\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:09:11');
INSERT INTO `sys_oper_log` VALUES (308, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1063,1064,1065,1066,1067,1068,100,1000,1001,1002,1003,1004,1005,1006,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:13:31');
INSERT INTO `sys_oper_log` VALUES (309, '角色管理', 1, 'com.ruoyi.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/role/add', '127.0.0.1', '内网IP', '{\r\n  \"roleName\" : [ \"运行管理员\" ],\r\n  \"roleKey\" : [ \"yunxingadmin\" ],\r\n  \"roleSort\" : [ \"9\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,100,1000,1001,1002,1003,1005,1006,1069,1070,1071,1072,1073,1075,1076,1077,1078,1079,1087,1088,1089,1090,1091,1093,1094,1095,1096,1097,1099,1100,1102,1103,1105,1106,1108,1109,1111,1112,1113,1114,1115,1117,1118,1119,1120,1121,1123,1124,1126,1127,1129,1130,1131,1132,1133,1135,1136,1138,1141,1142,1144\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:16:40');
INSERT INTO `sys_oper_log` VALUES (310, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"100\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"张无忌\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"13106547896\" ],\r\n  \"email\" : [ \"courseadmin@163.com\" ],\r\n  \"loginName\" : [ \"courseadmin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"106\" ],\r\n  \"remark\" : [ \"管理员用户信息\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"106\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:17:00');
INSERT INTO `sys_oper_log` VALUES (311, '班级信息', 1, 'com.ruoyi.web.controller.course.ClassInfoController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"舞蹈一班\" ],\r\n  \"introduce\" : [ \"舞蹈教育\" ],\r\n  \"teacherName\" : [ \"张肖荣\" ],\r\n  \"teacherId\" : [ \"103\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:46:14');
INSERT INTO `sys_oper_log` VALUES (312, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"3\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:56:36');
INSERT INTO `sys_oper_log` VALUES (313, '班级信息', 3, 'com.ruoyi.web.controller.course.ClassInfoController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/classInfo/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"4\" ],\r\n  \"status\" : [ \"3\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 20:56:43');
INSERT INTO `sys_oper_log` VALUES (314, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/22/2692060788ca725b169c4458b8a1096a.jfif\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 21:04:59');
INSERT INTO `sys_oper_log` VALUES (315, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"课程1\" ],\r\n  \"introduce\" : [ \"课程1测试...\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/04/22/2692060788ca725b169c4458b8a1096a.jfif\" ],\r\n  \"typeName\" : [ \"少儿动画教育\" ],\r\n  \"typeId\" : [ \"2\" ],\r\n  \"price\" : [ \"180\" ],\r\n  \"publishTime\" : [ \"2020-04-08\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 21:05:26');
INSERT INTO `sys_oper_log` VALUES (316, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.answeringSaved()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/answeringSaved', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"backContent\" : [ \"eeeeee\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-22 21:12:14');
INSERT INTO `sys_oper_log` VALUES (317, '学员缴费信息', 1, 'com.ruoyi.web.controller.course.StudentFeeController.addSave()', 'POST', 1, 'admin', '研发部门', '/course/fee/add', '127.0.0.1', '内网IP', '{\r\n  \"status\" : [ \"ZFB\" ],\r\n  \"money\" : [ \"158\" ],\r\n  \"payTime\" : [ \"2020-03-30\" ],\r\n  \"courseId\" : [ \"1\" ],\r\n  \"courseName\" : [ \"积木教程\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"李明\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-23 05:47:22');
INSERT INTO `sys_oper_log` VALUES (318, '学员缴费信息', 1, 'com.ruoyi.web.controller.course.StudentFeeController.addSave()', 'POST', 1, 'admin', '研发部门', '/course/fee/add', '127.0.0.1', '内网IP', '{\r\n  \"method\" : [ \"WX\" ],\r\n  \"money\" : [ \"190\" ],\r\n  \"payTime\" : [ \"2020-04-07\" ],\r\n  \"courseId\" : [ \"2\" ],\r\n  \"courseName\" : [ \"积木2\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"李明\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-23 05:50:06');
INSERT INTO `sys_oper_log` VALUES (319, '课程报名号信息', 1, 'com.ruoyi.web.controller.course.CourseApplyController.addSave()', 'POST', 1, 'admin', '研发部门', '/course/apply/add', '127.0.0.1', '内网IP', '{\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"1\" ],\r\n  \"phone\" : [ \"13514587421\" ],\r\n  \"classId\" : [ \"1\" ],\r\n  \"className\" : [ \"画画一班\" ],\r\n  \"courseId\" : [ \"1\" ],\r\n  \"courseName\" : [ \"积木教程\" ],\r\n  \"price\" : [ \"180\" ],\r\n  \"courseTime\" : [ \"2020-04-23\" ],\r\n  \"status\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-23 20:23:31');
INSERT INTO `sys_oper_log` VALUES (320, '课程报名号信息', 2, 'com.ruoyi.web.controller.course.CourseApplyController.editSave()', 'POST', 1, 'admin', '研发部门', '/course/apply/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"李明\" ],\r\n  \"phone\" : [ \"13514587421\" ],\r\n  \"classId\" : [ \"1\" ],\r\n  \"className\" : [ \"画画一班\" ],\r\n  \"courseId\" : [ \"1\" ],\r\n  \"courseName\" : [ \"积木教程\" ],\r\n  \"price\" : [ \"180\" ],\r\n  \"courseTime\" : [ \"2020-04-23\" ],\r\n  \"status\" : [ \"1\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-23 20:23:41');
INSERT INTO `sys_oper_log` VALUES (321, '课程预约信息', 1, 'com.ruoyi.web.controller.course.CourseAppointmentController.addSave()', 'POST', 1, 'admin', '研发部门', '/course/appointment/add', '127.0.0.1', '内网IP', '{\r\n  \"courseId\" : [ \"1\" ],\r\n  \"courseName\" : [ \"积木教程\" ],\r\n  \"studentId\" : [ \"1\" ],\r\n  \"studentName\" : [ \"李明\" ],\r\n  \"status\" : [ \"1\" ],\r\n  \"courseTime\" : [ \"2020-04-29\" ],\r\n  \"actualCourseTime\" : [ \"2020-04-30\" ],\r\n  \"courseStatus\" : [ \"\" ],\r\n  \"teacherName\" : [ \"张明\" ],\r\n  \"address\" : [ \"北京市朝阳区金融广场\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-23 20:24:53');
INSERT INTO `sys_oper_log` VALUES (322, '上传图片', 2, 'com.ruoyi.web.controller.course.TeacherStyleController.uploadImg()', 'POST', 1, 'admin', '研发部门', '/course/style/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/24/e0de4def6198986e3e419e1ffec48b10.png\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-24 08:17:15');
INSERT INTO `sys_oper_log` VALUES (323, '上传视频', 2, 'com.ruoyi.web.controller.course.TeacherStyleController.uploadVideo()', 'POST', 1, 'admin', '研发部门', '/course/style/uploadVideo', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/04/24/7185b61b0ea3e3ad29a6d4635d432fea.mkv\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-24 08:17:47');
INSERT INTO `sys_oper_log` VALUES (324, '师生风采信息', 1, 'com.ruoyi.web.controller.course.TeacherStyleController.addSave()', 'POST', 1, 'admin', '研发部门', '/course/style/add', '127.0.0.1', '内网IP', '{\r\n  \"title\" : [ \"优秀 学员风采\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/04/24/e0de4def6198986e3e419e1ffec48b10.png\" ],\r\n  \"video\" : [ \"/profile/upload/2020/04/24/7185b61b0ea3e3ad29a6d4635d432fea.mkv\" ],\r\n  \"introduce\" : [ \"优秀学员的风采展示\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-04-24 08:17:57');
INSERT INTO `sys_oper_log` VALUES (325, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"107\" ],\r\n  \"parentId\" : [ \"1062\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"通知公告\" ],\r\n  \"url\" : [ \"/system/notice\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"system:notice:view\" ],\r\n  \"orderNum\" : [ \"8\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-13 10:45:38');
INSERT INTO `sys_oper_log` VALUES (326, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/notice/add', '127.0.0.1', '内网IP', '{\r\n  \"noticeTitle\" : [ \"全市开展大排查\" ],\r\n  \"noticeType\" : [ \"2\" ],\r\n  \"noticeContent\" : [ \"<p style=\\\"margin-bottom: 18px; padding: 0px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;\\\\\\\\5FAE软雅黑&quot;, SimSun, &quot;\\\\\\\\5B8B体&quot;, Arial; font-size: 18px; letter-spacing: 1px;\\\">　　在暂停增长一天后，吉林舒兰疫情再度扩散。<br></p><p style=\\\"margin-bottom: 18px; padding: 0px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;\\\\\\\\5FAE软雅黑&quot;, SimSun, &quot;\\\\\\\\5B8B体&quot;, Arial; font-size: 18px; letter-spacing: 1px;\\\">　　昨日（12日）31个省区市及兵团新增6例本土病例，均在吉林，都是此前病例的密切接触者。目前，舒兰疫情传染链共有22名确诊患者，21例在吉林，1例在辽宁沈阳。</p><p style=\\\"margin-bottom: 18px; padding: 0px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;\\\\\\\\5FAE软雅黑&quot;, SimSun, &quot;\\\\\\\\5B8B体&quot;, Arial; font-size: 18px; letter-spacing: 1px;\\\">　　此外，吉林昨日又报告1例疑似病例、1例无症状感染者，现有367名密切接触者在隔离中。</p><p style=\\\"margin-bottom: 18px; padding: 0px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;\\\\\\\\5FAE软雅黑&quot;, SimSun, &quot;\\\\\\\\5B8B体&quot;, Arial; font-size: 18px; letter-spacing: 1px;\\\">　　截至5月12日15时，舒兰市已成为高风险地区，同样有确诊病例的吉林市丰满区成为中风险地区。</p><div class=\\\"img_wrapper\\\" style=\\\"margin: 0px; padding: 0px; text-align: center; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;\\\\\\\\5FAE软雅黑&quot;, SimSun, &quot;\\\\\\\\5B8B体&quot;, Arial; font-size: 18px; letter-spacing: 1px;\\\"><img src=\\\"https://n.sinaimg.cn/news/crawl/165/w550h415/20200513/e416-itmiwrz5698863.jpg\\\" alt=\\\"\\\" style=\\\"margin: 0px auto; padding: 0px; border-style: none; vertical-align: top; display: block; max-width: 640px;\\\"><span class=\\\"img_descr\\\" style=\\\"margin: 5px auto; padding: 6px 0px; line-height: 20px; font-size: 16px; display: inline-block; zoom: 1; text-align: left; font-weight: 700;\\\"></span></div><p style=\\\"margin-bottom: 18px; p', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: #22001\r\n### The error may involve com.ruoyi.system.mapper.SysNoticeMapper.insertNotice-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into sys_notice (     notice_title,       notice_type,       notice_content,       status,             create_by,      create_time    )values(     ?,       ?,       ?,       ?,             ?,      sysdate()   )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: #22001\n; Data truncation: #22001; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: #22001', '2020-05-13 10:50:40');
INSERT INTO `sys_oper_log` VALUES (327, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/notice/add', '127.0.0.1', '内网IP', '{\r\n  \"noticeTitle\" : [ \"公告测试1\" ],\r\n  \"noticeType\" : [ \"2\" ],\r\n  \"noticeContent\" : [ \"<p>阿斯达阿达达我df安慰发发发萨法萨法萨法萨法</p><p>撒发阿萨法saf&nbsp;算法</p><p>发送发送发送</p><p>分撒按时asf</p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 00:08:01');
INSERT INTO `sys_oper_log` VALUES (328, '学员留言信息', 2, 'com.ruoyi.web.controller.course.MessageController.answeringSaved()', 'POST', 1, 'courseadmin', '研发部门', '/course/message/answeringSaved', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"8\" ],\r\n  \"backContent\" : [ \"继续加油~\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 20:52:33');
INSERT INTO `sys_oper_log` VALUES (329, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"104\" ],\r\n  \"deptId\" : [ \"104\" ],\r\n  \"userName\" : [ \"杨倩\" ],\r\n  \"dept.deptName\" : [ \"市场部门\" ],\r\n  \"phonenumber\" : [ \"15632145789\" ],\r\n  \"email\" : [ \"yangqian@163.com\" ],\r\n  \"loginName\" : [ \"yanqian\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"负责招生咨询\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 20:55:49');
INSERT INTO `sys_oper_log` VALUES (330, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"机器人教学\" ],\r\n  \"introduce\" : [ \"机器人教学的信息，值得小孩子学习\" ],\r\n  \"picture\" : [ \"\" ],\r\n  \"typeName\" : [ \"少儿动画教育\" ],\r\n  \"typeId\" : [ \"2\" ],\r\n  \"price\" : [ \"150\" ],\r\n  \"publishTime\" : [ \"2020-05-28\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 20:57:08');
INSERT INTO `sys_oper_log` VALUES (331, '课程信息', 3, 'com.ruoyi.web.controller.course.CourseController.remove()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"7\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 20:59:07');
INSERT INTO `sys_oper_log` VALUES (332, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"机器人\" ],\r\n  \"introduce\" : [ \"机器人ddddd\" ],\r\n  \"picture\" : [ \"\" ],\r\n  \"typeName\" : [ \"少儿动画教育\" ],\r\n  \"typeId\" : [ \"2\" ],\r\n  \"price\" : [ \"180\" ],\r\n  \"publishTime\" : [ \"2020-05-28\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 20:59:34');
INSERT INTO `sys_oper_log` VALUES (333, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/674ce47b6b5cd43f3a7b9dc847b3212b.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:01:05');
INSERT INTO `sys_oper_log` VALUES (334, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"机器人\" ],\r\n  \"introduce\" : [ \"点点滴滴\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/674ce47b6b5cd43f3a7b9dc847b3212b.jpg\" ],\r\n  \"typeName\" : [ \"少儿动画教育\" ],\r\n  \"typeId\" : [ \"2\" ],\r\n  \"price\" : [ \"180\" ],\r\n  \"publishTime\" : [ \"2020-04-08\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:01:27');
INSERT INTO `sys_oper_log` VALUES (335, '课程信息', 3, 'com.ruoyi.web.controller.course.CourseController.remove()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"8\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:01:32');
INSERT INTO `sys_oper_log` VALUES (336, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"少儿舞蹈\" ],\r\n  \"introduce\" : [ \"少儿舞蹈的教学\" ],\r\n  \"picture\" : [ \"\" ],\r\n  \"typeName\" : [ \"少儿动画教育\" ],\r\n  \"typeId\" : [ \"2\" ],\r\n  \"price\" : [ \"500\" ],\r\n  \"publishTime\" : [ \"2020-05-29\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:02:31');
INSERT INTO `sys_oper_log` VALUES (337, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"10\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:02:46');
INSERT INTO `sys_oper_log` VALUES (338, '上传课程图片信息', 2, 'com.ruoyi.web.controller.course.CourseController.uploadCourseImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/fbbcaae69cbb22bada17ef3b782dcc37.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:03:12');
INSERT INTO `sys_oper_log` VALUES (339, '课程信息', 1, 'com.ruoyi.web.controller.course.CourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/add', '127.0.0.1', '内网IP', '{\r\n  \"name\" : [ \"少儿舞蹈\" ],\r\n  \"introduce\" : [ \"活跃少儿的性格\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/fbbcaae69cbb22bada17ef3b782dcc37.jpg\" ],\r\n  \"typeName\" : [ \"少儿动画教育\" ],\r\n  \"typeId\" : [ \"2\" ],\r\n  \"price\" : [ \"500\" ],\r\n  \"publishTime\" : [ \"2020-05-28\" ],\r\n  \"teacherName\" : [ \"张洋\" ],\r\n  \"teacherId\" : [ \"101\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:03:33');
INSERT INTO `sys_oper_log` VALUES (340, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:03:52');
INSERT INTO `sys_oper_log` VALUES (341, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"2\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:03:54');
INSERT INTO `sys_oper_log` VALUES (342, '课程信息', 2, 'com.ruoyi.web.controller.course.CourseController.changeStatus()', 'POST', 1, 'courseadmin', '研发部门', '/course/course/changeStatus', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"3\" ],\r\n  \"status\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:03:57');
INSERT INTO `sys_oper_log` VALUES (343, '上传图片', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/1dfaad433b66c0a5760e85d27022bf2d.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:06:15');
INSERT INTO `sys_oper_log` VALUES (344, '精品课程信息', 1, 'com.ruoyi.web.controller.course.ExcellentCourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/add', '127.0.0.1', '内网IP', '{\r\n  \"courseName\" : [ \"机器人精品课程\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/1dfaad433b66c0a5760e85d27022bf2d.jpg\" ],\r\n  \"video\" : [ \"\" ],\r\n  \"introduce\" : [ \"精品课程的结束...\" ],\r\n  \"price\" : [ \"500\" ],\r\n  \"teacherName\" : [ \"李方\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:07:03');
INSERT INTO `sys_oper_log` VALUES (345, '上传图片', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/1062d5fe4afd9610e27914ac843fb168.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:07:44');
INSERT INTO `sys_oper_log` VALUES (346, '精品课程信息', 1, 'com.ruoyi.web.controller.course.ExcellentCourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/add', '127.0.0.1', '内网IP', '{\r\n  \"courseName\" : [ \"绘画课程\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/1062d5fe4afd9610e27914ac843fb168.jpg\" ],\r\n  \"video\" : [ \"\" ],\r\n  \"introduce\" : [ \"精品绘画课程\" ],\r\n  \"price\" : [ \"800\" ],\r\n  \"teacherName\" : [ \"李广\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:08:03');
INSERT INTO `sys_oper_log` VALUES (347, '上传图片', 2, 'com.ruoyi.web.controller.course.ExcellentCourseController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/96253a7477b432d81a17bf8caa569a6d.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:08:26');
INSERT INTO `sys_oper_log` VALUES (348, '精品课程信息', 1, 'com.ruoyi.web.controller.course.ExcellentCourseController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/excellentCourse/add', '127.0.0.1', '内网IP', '{\r\n  \"courseName\" : [ \"精品积木课程\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/96253a7477b432d81a17bf8caa569a6d.jpg\" ],\r\n  \"video\" : [ \"\" ],\r\n  \"introduce\" : [ \"精品积木课程\" ],\r\n  \"price\" : [ \"800\" ],\r\n  \"teacherName\" : [ \"李方\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:08:43');
INSERT INTO `sys_oper_log` VALUES (349, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"104\" ],\r\n  \"deptId\" : [ \"104\" ],\r\n  \"userName\" : [ \"杨倩\" ],\r\n  \"dept.deptName\" : [ \"市场部门\" ],\r\n  \"phonenumber\" : [ \"15632145789\" ],\r\n  \"email\" : [ \"yangqian@163.com\" ],\r\n  \"loginName\" : [ \"yanqian\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"role\" : [ \"101\" ],\r\n  \"remark\" : [ \"教师介绍Angel老师 英语专业学士,英语专业八级。从事少儿英语教育数年,有丰富的教学经验。性格开朗活泼,语感和表达能力较强。富有亲和力、耐心及责任感,课堂气氛活跃,...\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"101\" ],\r\n  \"postIds\" : [ \"4\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:09:51');
INSERT INTO `sys_oper_log` VALUES (350, '上传图片', 2, 'com.ruoyi.web.controller.course.TeacherStyleController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/style/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/9ee5a174bee72cc0129cf4872433fd12.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:12:02');
INSERT INTO `sys_oper_log` VALUES (351, '师生风采信息', 1, 'com.ruoyi.web.controller.course.TeacherStyleController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/style/add', '127.0.0.1', '内网IP', '{\r\n  \"title\" : [ \"东海大道\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/9ee5a174bee72cc0129cf4872433fd12.jpg\" ],\r\n  \"video\" : [ \"\" ],\r\n  \"introduce\" : [ \"教师介绍Angel老师 英语专业学士,英语专业八级。从事少儿英语教育数年,有丰富的教学经验。性格开朗活泼,语感和表达能力较强。富有亲和力、耐心及责任感,课堂气氛活跃,...\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:12:15');
INSERT INTO `sys_oper_log` VALUES (352, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1063,1064,1065,1066,1067,1068,100,1000,1001,1002,1003,1004,1005,1006,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,107,1035,1036,1037,1038,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:13:24');
INSERT INTO `sys_oper_log` VALUES (353, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1063,1064,1065,1066,1067,1068,100,1000,1001,1002,1003,1004,1005,1006,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,107,1035,1036,1037,1038,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:14:17');
INSERT INTO `sys_oper_log` VALUES (354, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"100\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"张无忌\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"13106547896\" ],\r\n  \"email\" : [ \"courseadmin@163.com\" ],\r\n  \"loginName\" : [ \"courseadmin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"106\" ],\r\n  \"remark\" : [ \"管理员用户信息\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"106\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:14:39');
INSERT INTO `sys_oper_log` VALUES (355, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"106\" ],\r\n  \"roleName\" : [ \"运行管理员\" ],\r\n  \"roleKey\" : [ \"yunxingadmin\" ],\r\n  \"roleSort\" : [ \"9\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,100,1000,1001,1002,1003,1005,1006,1069,1070,1071,1072,1073,1075,1076,1077,1078,1079,1087,1088,1089,1090,1091,1093,1094,1095,1096,1097,1099,1100,1102,1103,1105,1106,1108,1109,1111,1112,1113,1114,1115,107,1035,1036,1037,1038,1117,1118,1119,1120,1121,1123,1124,1126,1127,1129,1130,1131,1132,1133,1135,1136,1138,1141,1142,1144\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:14:59');
INSERT INTO `sys_oper_log` VALUES (356, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\r\n  \"userId\" : [ \"100\" ],\r\n  \"deptId\" : [ \"103\" ],\r\n  \"userName\" : [ \"张无忌\" ],\r\n  \"dept.deptName\" : [ \"研发部门\" ],\r\n  \"phonenumber\" : [ \"13106547896\" ],\r\n  \"email\" : [ \"courseadmin@163.com\" ],\r\n  \"loginName\" : [ \"courseadmin\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"role\" : [ \"106\", \"100\" ],\r\n  \"remark\" : [ \"管理员用户信息\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"106,100\" ],\r\n  \"postIds\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:15:16');
INSERT INTO `sys_oper_log` VALUES (357, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/system/notice/add', '127.0.0.1', '内网IP', '{\r\n  \"noticeTitle\" : [ \"五一放假通知\" ],\r\n  \"noticeType\" : [ \"1\" ],\r\n  \"noticeContent\" : [ \"<p><span style=\\\"font-size: 14px;\\\">各位同学:</span></p><p><span style=\\\"font-size: 14px;\\\">&nbsp; &nbsp; &nbsp; &nbsp;大家好~<span style=\\\"background-color: rgb(255, 0, 0);\\\">五一</span>学校放假5天，请知晓~多对多</span></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:16:49');
INSERT INTO `sys_oper_log` VALUES (358, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '研发部门', '/system/menu/remove/1135', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"存在子菜单,不允许删除\",\r\n  \"code\" : 301\r\n}', 0, NULL, '2020-05-14 21:24:09');
INSERT INTO `sys_oper_log` VALUES (359, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\r\n  \"menuId\" : [ \"1148\" ],\r\n  \"parentId\" : [ \"1062\" ],\r\n  \"menuType\" : [ \"C\" ],\r\n  \"menuName\" : [ \"课程报名信息\" ],\r\n  \"url\" : [ \"/course/personCourse\" ],\r\n  \"target\" : [ \"menuItem\" ],\r\n  \"perms\" : [ \"course:personCourse:view\" ],\r\n  \"orderNum\" : [ \"15\" ],\r\n  \"icon\" : [ \"#\" ],\r\n  \"visible\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:24:50');
INSERT INTO `sys_oper_log` VALUES (360, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"100\" ],\r\n  \"roleName\" : [ \"后台管理员\" ],\r\n  \"roleKey\" : [ \"courseadmin\" ],\r\n  \"roleSort\" : [ \"3\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1063,1064,1065,1066,1067,1068,100,1000,1001,1002,1003,1004,1005,1006,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,107,1035,1036,1037,1038,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1148,1149,1150,1151,1152,1153\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:25:08');
INSERT INTO `sys_oper_log` VALUES (361, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"106\" ],\r\n  \"roleName\" : [ \"运行管理员\" ],\r\n  \"roleKey\" : [ \"yunxingadmin\" ],\r\n  \"roleSort\" : [ \"9\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,100,1000,1001,1002,1003,1005,1006,1069,1070,1071,1072,1073,1075,1076,1077,1078,1079,1087,1088,1089,1090,1091,1093,1094,1095,1096,1097,1099,1100,1102,1103,1105,1106,1108,1109,1111,1112,1113,1114,1115,107,1035,1036,1037,1038,1117,1118,1119,1120,1121,1123,1124,1126,1127,1129,1130,1131,1132,1133,1135,1136,1138,1141,1142,1144\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:25:14');
INSERT INTO `sys_oper_log` VALUES (362, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\r\n  \"roleId\" : [ \"105\" ],\r\n  \"roleName\" : [ \"招生系统管理员\" ],\r\n  \"roleKey\" : [ \"zhaosheng\" ],\r\n  \"roleSort\" : [ \"8\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ],\r\n  \"menuIds\" : [ \"1062,1063,1064,1065,1066,1067,1068,100,1000,1001,1002,1003,1004,1005,1006,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:25:19');
INSERT INTO `sys_oper_log` VALUES (363, '个人课程信息', 3, 'com.ruoyi.web.controller.course.PersonCourseInfoController.remove()', 'POST', 1, 'courseadmin', '研发部门', '/course/personCourse/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"3\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:33:07');
INSERT INTO `sys_oper_log` VALUES (364, '个人课程信息', 3, 'com.ruoyi.web.controller.course.PersonCourseInfoController.remove()', 'POST', 1, 'courseadmin', '研发部门', '/course/personCourse/remove', '127.0.0.1', '内网IP', '{\r\n  \"ids\" : [ \"2\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:33:10');
INSERT INTO `sys_oper_log` VALUES (365, '上传图片', 2, 'com.ruoyi.web.controller.course.TeacherStyleController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/style/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/c0d42ca462dfcb5586a17fac20e77b94.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:44:31');
INSERT INTO `sys_oper_log` VALUES (366, '师生风采信息', 1, 'com.ruoyi.web.controller.course.TeacherStyleController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/style/add', '127.0.0.1', '内网IP', '{\r\n  \"title\" : [ \"一起参加演出活动\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/c0d42ca462dfcb5586a17fac20e77b94.jpg\" ],\r\n  \"video\" : [ \"\" ],\r\n  \"introduce\" : [ \"大使大赛的发生发达萨法萨法萨法萨法是否萨法萨法发萨法萨法撒发阿发阿萨法分撒分撒阿萨法萨法萨法舒服\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:44:48');
INSERT INTO `sys_oper_log` VALUES (367, '上传图片', 2, 'com.ruoyi.web.controller.course.TeacherStyleController.uploadImg()', 'POST', 1, 'courseadmin', '研发部门', '/course/style/uploadImg', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"/profile/upload/2020/05/14/8248e7b584852b0087a852df08c8bbe4.jpg\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:48:21');
INSERT INTO `sys_oper_log` VALUES (368, '师生风采信息', 1, 'com.ruoyi.web.controller.course.TeacherStyleController.addSave()', 'POST', 1, 'courseadmin', '研发部门', '/course/style/add', '127.0.0.1', '内网IP', '{\r\n  \"title\" : [ \"郊外旅游图片\" ],\r\n  \"picture\" : [ \"/profile/upload/2020/05/14/8248e7b584852b0087a852df08c8bbe4.jpg\" ],\r\n  \"video\" : [ \"\" ],\r\n  \"introduce\" : [ \"去啊沙发舒服\\r\\n为个sgsagassa\\r\\nsfasfasfsafw\\r\\nfawfafasfasfas\\r\\n\\r\\nfasfafasffs\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-05-14 21:48:43');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (2, 'xz', '校长', 2, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 22:06:01', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (4, 'user', '职员', 4, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 22:06:24', '');
INSERT INTO `sys_post` VALUES (5, 'js', '讲师', 5, '0', 'admin', '2020-04-19 22:04:31', '', NULL, '进行课程的讲授');
INSERT INTO `sys_post` VALUES (6, 'zj', '助教', 6, '0', 'admin', '2020-04-19 22:05:00', '', NULL, '辅助讲师进行答疑以及问题的解答');
INSERT INTO `sys_post` VALUES (7, 'fdy', '辅导员', 7, '0', 'admin', '2020-04-19 22:05:26', '', NULL, '负责学生信息的管理');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, '1', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '普通角色');
INSERT INTO `sys_role` VALUES (100, '后台管理员', 'courseadmin', 3, '1', '0', '0', 'admin', '2020-04-19 06:38:04', 'admin', '2020-05-14 21:25:08', '');
INSERT INTO `sys_role` VALUES (101, '代课教师', 'teacher', 4, '1', '0', '0', 'admin', '2020-04-19 08:58:40', 'admin', '2020-04-22 20:09:11', '');
INSERT INTO `sys_role` VALUES (102, '辅导员', 'fudaoyuan', 5, '1', '0', '0', 'admin', '2020-04-19 09:29:51', 'courseadmin', '2020-04-20 08:32:04', '');
INSERT INTO `sys_role` VALUES (103, '学生', 'student', 8, '1', '0', '0', 'courseadmin', '2020-04-19 22:35:53', '', NULL, NULL);
INSERT INTO `sys_role` VALUES (104, '招生办', 'erroll', 6, '1', '0', '0', 'courseadmin', '2020-04-19 22:36:51', '', NULL, NULL);
INSERT INTO `sys_role` VALUES (105, '招生系统管理员', 'zhaosheng', 8, '1', '0', '0', 'admin', '2020-04-22 20:06:47', 'admin', '2020-05-14 21:25:19', '');
INSERT INTO `sys_role` VALUES (106, '运行管理员', 'yunxingadmin', 9, '1', '0', '0', 'admin', '2020-04-22 20:16:39', 'admin', '2020-05-14 21:25:14', '');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (2, 1061);
INSERT INTO `sys_role_menu` VALUES (100, 100);
INSERT INTO `sys_role_menu` VALUES (100, 107);
INSERT INTO `sys_role_menu` VALUES (100, 1000);
INSERT INTO `sys_role_menu` VALUES (100, 1001);
INSERT INTO `sys_role_menu` VALUES (100, 1002);
INSERT INTO `sys_role_menu` VALUES (100, 1003);
INSERT INTO `sys_role_menu` VALUES (100, 1004);
INSERT INTO `sys_role_menu` VALUES (100, 1005);
INSERT INTO `sys_role_menu` VALUES (100, 1006);
INSERT INTO `sys_role_menu` VALUES (100, 1035);
INSERT INTO `sys_role_menu` VALUES (100, 1036);
INSERT INTO `sys_role_menu` VALUES (100, 1037);
INSERT INTO `sys_role_menu` VALUES (100, 1038);
INSERT INTO `sys_role_menu` VALUES (100, 1062);
INSERT INTO `sys_role_menu` VALUES (100, 1063);
INSERT INTO `sys_role_menu` VALUES (100, 1064);
INSERT INTO `sys_role_menu` VALUES (100, 1065);
INSERT INTO `sys_role_menu` VALUES (100, 1066);
INSERT INTO `sys_role_menu` VALUES (100, 1067);
INSERT INTO `sys_role_menu` VALUES (100, 1068);
INSERT INTO `sys_role_menu` VALUES (100, 1069);
INSERT INTO `sys_role_menu` VALUES (100, 1070);
INSERT INTO `sys_role_menu` VALUES (100, 1071);
INSERT INTO `sys_role_menu` VALUES (100, 1072);
INSERT INTO `sys_role_menu` VALUES (100, 1073);
INSERT INTO `sys_role_menu` VALUES (100, 1074);
INSERT INTO `sys_role_menu` VALUES (100, 1075);
INSERT INTO `sys_role_menu` VALUES (100, 1076);
INSERT INTO `sys_role_menu` VALUES (100, 1077);
INSERT INTO `sys_role_menu` VALUES (100, 1078);
INSERT INTO `sys_role_menu` VALUES (100, 1079);
INSERT INTO `sys_role_menu` VALUES (100, 1080);
INSERT INTO `sys_role_menu` VALUES (100, 1087);
INSERT INTO `sys_role_menu` VALUES (100, 1088);
INSERT INTO `sys_role_menu` VALUES (100, 1089);
INSERT INTO `sys_role_menu` VALUES (100, 1090);
INSERT INTO `sys_role_menu` VALUES (100, 1091);
INSERT INTO `sys_role_menu` VALUES (100, 1092);
INSERT INTO `sys_role_menu` VALUES (100, 1093);
INSERT INTO `sys_role_menu` VALUES (100, 1094);
INSERT INTO `sys_role_menu` VALUES (100, 1095);
INSERT INTO `sys_role_menu` VALUES (100, 1096);
INSERT INTO `sys_role_menu` VALUES (100, 1097);
INSERT INTO `sys_role_menu` VALUES (100, 1098);
INSERT INTO `sys_role_menu` VALUES (100, 1099);
INSERT INTO `sys_role_menu` VALUES (100, 1100);
INSERT INTO `sys_role_menu` VALUES (100, 1101);
INSERT INTO `sys_role_menu` VALUES (100, 1102);
INSERT INTO `sys_role_menu` VALUES (100, 1103);
INSERT INTO `sys_role_menu` VALUES (100, 1104);
INSERT INTO `sys_role_menu` VALUES (100, 1105);
INSERT INTO `sys_role_menu` VALUES (100, 1106);
INSERT INTO `sys_role_menu` VALUES (100, 1107);
INSERT INTO `sys_role_menu` VALUES (100, 1108);
INSERT INTO `sys_role_menu` VALUES (100, 1109);
INSERT INTO `sys_role_menu` VALUES (100, 1110);
INSERT INTO `sys_role_menu` VALUES (100, 1111);
INSERT INTO `sys_role_menu` VALUES (100, 1112);
INSERT INTO `sys_role_menu` VALUES (100, 1113);
INSERT INTO `sys_role_menu` VALUES (100, 1114);
INSERT INTO `sys_role_menu` VALUES (100, 1115);
INSERT INTO `sys_role_menu` VALUES (100, 1116);
INSERT INTO `sys_role_menu` VALUES (100, 1117);
INSERT INTO `sys_role_menu` VALUES (100, 1118);
INSERT INTO `sys_role_menu` VALUES (100, 1119);
INSERT INTO `sys_role_menu` VALUES (100, 1120);
INSERT INTO `sys_role_menu` VALUES (100, 1121);
INSERT INTO `sys_role_menu` VALUES (100, 1122);
INSERT INTO `sys_role_menu` VALUES (100, 1123);
INSERT INTO `sys_role_menu` VALUES (100, 1124);
INSERT INTO `sys_role_menu` VALUES (100, 1125);
INSERT INTO `sys_role_menu` VALUES (100, 1126);
INSERT INTO `sys_role_menu` VALUES (100, 1127);
INSERT INTO `sys_role_menu` VALUES (100, 1128);
INSERT INTO `sys_role_menu` VALUES (100, 1129);
INSERT INTO `sys_role_menu` VALUES (100, 1130);
INSERT INTO `sys_role_menu` VALUES (100, 1131);
INSERT INTO `sys_role_menu` VALUES (100, 1132);
INSERT INTO `sys_role_menu` VALUES (100, 1133);
INSERT INTO `sys_role_menu` VALUES (100, 1134);
INSERT INTO `sys_role_menu` VALUES (100, 1135);
INSERT INTO `sys_role_menu` VALUES (100, 1136);
INSERT INTO `sys_role_menu` VALUES (100, 1137);
INSERT INTO `sys_role_menu` VALUES (100, 1138);
INSERT INTO `sys_role_menu` VALUES (100, 1139);
INSERT INTO `sys_role_menu` VALUES (100, 1140);
INSERT INTO `sys_role_menu` VALUES (100, 1141);
INSERT INTO `sys_role_menu` VALUES (100, 1142);
INSERT INTO `sys_role_menu` VALUES (100, 1143);
INSERT INTO `sys_role_menu` VALUES (100, 1144);
INSERT INTO `sys_role_menu` VALUES (100, 1145);
INSERT INTO `sys_role_menu` VALUES (100, 1146);
INSERT INTO `sys_role_menu` VALUES (100, 1148);
INSERT INTO `sys_role_menu` VALUES (100, 1149);
INSERT INTO `sys_role_menu` VALUES (100, 1150);
INSERT INTO `sys_role_menu` VALUES (100, 1151);
INSERT INTO `sys_role_menu` VALUES (100, 1152);
INSERT INTO `sys_role_menu` VALUES (100, 1153);
INSERT INTO `sys_role_menu` VALUES (101, 1062);
INSERT INTO `sys_role_menu` VALUES (101, 1069);
INSERT INTO `sys_role_menu` VALUES (101, 1070);
INSERT INTO `sys_role_menu` VALUES (101, 1071);
INSERT INTO `sys_role_menu` VALUES (101, 1072);
INSERT INTO `sys_role_menu` VALUES (101, 1074);
INSERT INTO `sys_role_menu` VALUES (101, 1075);
INSERT INTO `sys_role_menu` VALUES (101, 1076);
INSERT INTO `sys_role_menu` VALUES (101, 1077);
INSERT INTO `sys_role_menu` VALUES (101, 1078);
INSERT INTO `sys_role_menu` VALUES (101, 1079);
INSERT INTO `sys_role_menu` VALUES (101, 1080);
INSERT INTO `sys_role_menu` VALUES (101, 1087);
INSERT INTO `sys_role_menu` VALUES (101, 1088);
INSERT INTO `sys_role_menu` VALUES (101, 1089);
INSERT INTO `sys_role_menu` VALUES (101, 1090);
INSERT INTO `sys_role_menu` VALUES (101, 1091);
INSERT INTO `sys_role_menu` VALUES (101, 1092);
INSERT INTO `sys_role_menu` VALUES (101, 1093);
INSERT INTO `sys_role_menu` VALUES (101, 1094);
INSERT INTO `sys_role_menu` VALUES (101, 1095);
INSERT INTO `sys_role_menu` VALUES (101, 1096);
INSERT INTO `sys_role_menu` VALUES (101, 1097);
INSERT INTO `sys_role_menu` VALUES (101, 1098);
INSERT INTO `sys_role_menu` VALUES (101, 1099);
INSERT INTO `sys_role_menu` VALUES (101, 1100);
INSERT INTO `sys_role_menu` VALUES (101, 1104);
INSERT INTO `sys_role_menu` VALUES (101, 1105);
INSERT INTO `sys_role_menu` VALUES (101, 1106);
INSERT INTO `sys_role_menu` VALUES (101, 1107);
INSERT INTO `sys_role_menu` VALUES (101, 1108);
INSERT INTO `sys_role_menu` VALUES (101, 1109);
INSERT INTO `sys_role_menu` VALUES (101, 1110);
INSERT INTO `sys_role_menu` VALUES (101, 1129);
INSERT INTO `sys_role_menu` VALUES (101, 1130);
INSERT INTO `sys_role_menu` VALUES (101, 1131);
INSERT INTO `sys_role_menu` VALUES (101, 1132);
INSERT INTO `sys_role_menu` VALUES (101, 1133);
INSERT INTO `sys_role_menu` VALUES (101, 1134);
INSERT INTO `sys_role_menu` VALUES (101, 1135);
INSERT INTO `sys_role_menu` VALUES (101, 1136);
INSERT INTO `sys_role_menu` VALUES (101, 1140);
INSERT INTO `sys_role_menu` VALUES (101, 1141);
INSERT INTO `sys_role_menu` VALUES (101, 1142);
INSERT INTO `sys_role_menu` VALUES (101, 1146);
INSERT INTO `sys_role_menu` VALUES (102, 1062);
INSERT INTO `sys_role_menu` VALUES (102, 1069);
INSERT INTO `sys_role_menu` VALUES (102, 1070);
INSERT INTO `sys_role_menu` VALUES (102, 1071);
INSERT INTO `sys_role_menu` VALUES (102, 1072);
INSERT INTO `sys_role_menu` VALUES (102, 1074);
INSERT INTO `sys_role_menu` VALUES (102, 1075);
INSERT INTO `sys_role_menu` VALUES (102, 1076);
INSERT INTO `sys_role_menu` VALUES (102, 1077);
INSERT INTO `sys_role_menu` VALUES (102, 1078);
INSERT INTO `sys_role_menu` VALUES (102, 1079);
INSERT INTO `sys_role_menu` VALUES (102, 1080);
INSERT INTO `sys_role_menu` VALUES (102, 1093);
INSERT INTO `sys_role_menu` VALUES (102, 1094);
INSERT INTO `sys_role_menu` VALUES (102, 1095);
INSERT INTO `sys_role_menu` VALUES (102, 1096);
INSERT INTO `sys_role_menu` VALUES (102, 1097);
INSERT INTO `sys_role_menu` VALUES (102, 1098);
INSERT INTO `sys_role_menu` VALUES (102, 1099);
INSERT INTO `sys_role_menu` VALUES (102, 1100);
INSERT INTO `sys_role_menu` VALUES (102, 1101);
INSERT INTO `sys_role_menu` VALUES (102, 1104);
INSERT INTO `sys_role_menu` VALUES (102, 1105);
INSERT INTO `sys_role_menu` VALUES (102, 1106);
INSERT INTO `sys_role_menu` VALUES (102, 1107);
INSERT INTO `sys_role_menu` VALUES (102, 1108);
INSERT INTO `sys_role_menu` VALUES (102, 1109);
INSERT INTO `sys_role_menu` VALUES (102, 1110);
INSERT INTO `sys_role_menu` VALUES (102, 1111);
INSERT INTO `sys_role_menu` VALUES (102, 1112);
INSERT INTO `sys_role_menu` VALUES (102, 1113);
INSERT INTO `sys_role_menu` VALUES (102, 1114);
INSERT INTO `sys_role_menu` VALUES (102, 1115);
INSERT INTO `sys_role_menu` VALUES (102, 1116);
INSERT INTO `sys_role_menu` VALUES (102, 1117);
INSERT INTO `sys_role_menu` VALUES (102, 1118);
INSERT INTO `sys_role_menu` VALUES (102, 1119);
INSERT INTO `sys_role_menu` VALUES (102, 1120);
INSERT INTO `sys_role_menu` VALUES (102, 1121);
INSERT INTO `sys_role_menu` VALUES (102, 1122);
INSERT INTO `sys_role_menu` VALUES (102, 1123);
INSERT INTO `sys_role_menu` VALUES (102, 1124);
INSERT INTO `sys_role_menu` VALUES (102, 1125);
INSERT INTO `sys_role_menu` VALUES (102, 1126);
INSERT INTO `sys_role_menu` VALUES (102, 1127);
INSERT INTO `sys_role_menu` VALUES (102, 1128);
INSERT INTO `sys_role_menu` VALUES (102, 1129);
INSERT INTO `sys_role_menu` VALUES (102, 1130);
INSERT INTO `sys_role_menu` VALUES (102, 1131);
INSERT INTO `sys_role_menu` VALUES (102, 1132);
INSERT INTO `sys_role_menu` VALUES (102, 1133);
INSERT INTO `sys_role_menu` VALUES (102, 1134);
INSERT INTO `sys_role_menu` VALUES (102, 1135);
INSERT INTO `sys_role_menu` VALUES (102, 1136);
INSERT INTO `sys_role_menu` VALUES (102, 1137);
INSERT INTO `sys_role_menu` VALUES (102, 1138);
INSERT INTO `sys_role_menu` VALUES (102, 1139);
INSERT INTO `sys_role_menu` VALUES (102, 1140);
INSERT INTO `sys_role_menu` VALUES (102, 1141);
INSERT INTO `sys_role_menu` VALUES (102, 1142);
INSERT INTO `sys_role_menu` VALUES (102, 1143);
INSERT INTO `sys_role_menu` VALUES (102, 1144);
INSERT INTO `sys_role_menu` VALUES (102, 1145);
INSERT INTO `sys_role_menu` VALUES (102, 1146);
INSERT INTO `sys_role_menu` VALUES (103, 1147);
INSERT INTO `sys_role_menu` VALUES (103, 1148);
INSERT INTO `sys_role_menu` VALUES (103, 1149);
INSERT INTO `sys_role_menu` VALUES (103, 1150);
INSERT INTO `sys_role_menu` VALUES (103, 1151);
INSERT INTO `sys_role_menu` VALUES (103, 1152);
INSERT INTO `sys_role_menu` VALUES (103, 1153);
INSERT INTO `sys_role_menu` VALUES (104, 1062);
INSERT INTO `sys_role_menu` VALUES (104, 1075);
INSERT INTO `sys_role_menu` VALUES (104, 1076);
INSERT INTO `sys_role_menu` VALUES (104, 1077);
INSERT INTO `sys_role_menu` VALUES (104, 1078);
INSERT INTO `sys_role_menu` VALUES (104, 1079);
INSERT INTO `sys_role_menu` VALUES (104, 1080);
INSERT INTO `sys_role_menu` VALUES (104, 1087);
INSERT INTO `sys_role_menu` VALUES (104, 1088);
INSERT INTO `sys_role_menu` VALUES (104, 1089);
INSERT INTO `sys_role_menu` VALUES (104, 1090);
INSERT INTO `sys_role_menu` VALUES (104, 1091);
INSERT INTO `sys_role_menu` VALUES (104, 1092);
INSERT INTO `sys_role_menu` VALUES (104, 1123);
INSERT INTO `sys_role_menu` VALUES (104, 1124);
INSERT INTO `sys_role_menu` VALUES (104, 1125);
INSERT INTO `sys_role_menu` VALUES (104, 1126);
INSERT INTO `sys_role_menu` VALUES (104, 1127);
INSERT INTO `sys_role_menu` VALUES (104, 1128);
INSERT INTO `sys_role_menu` VALUES (104, 1129);
INSERT INTO `sys_role_menu` VALUES (104, 1130);
INSERT INTO `sys_role_menu` VALUES (104, 1131);
INSERT INTO `sys_role_menu` VALUES (104, 1132);
INSERT INTO `sys_role_menu` VALUES (104, 1133);
INSERT INTO `sys_role_menu` VALUES (104, 1134);
INSERT INTO `sys_role_menu` VALUES (104, 1135);
INSERT INTO `sys_role_menu` VALUES (104, 1136);
INSERT INTO `sys_role_menu` VALUES (104, 1137);
INSERT INTO `sys_role_menu` VALUES (104, 1138);
INSERT INTO `sys_role_menu` VALUES (104, 1139);
INSERT INTO `sys_role_menu` VALUES (104, 1140);
INSERT INTO `sys_role_menu` VALUES (104, 1141);
INSERT INTO `sys_role_menu` VALUES (104, 1142);
INSERT INTO `sys_role_menu` VALUES (104, 1143);
INSERT INTO `sys_role_menu` VALUES (104, 1144);
INSERT INTO `sys_role_menu` VALUES (104, 1145);
INSERT INTO `sys_role_menu` VALUES (104, 1146);
INSERT INTO `sys_role_menu` VALUES (105, 100);
INSERT INTO `sys_role_menu` VALUES (105, 1000);
INSERT INTO `sys_role_menu` VALUES (105, 1001);
INSERT INTO `sys_role_menu` VALUES (105, 1002);
INSERT INTO `sys_role_menu` VALUES (105, 1003);
INSERT INTO `sys_role_menu` VALUES (105, 1004);
INSERT INTO `sys_role_menu` VALUES (105, 1005);
INSERT INTO `sys_role_menu` VALUES (105, 1006);
INSERT INTO `sys_role_menu` VALUES (105, 1062);
INSERT INTO `sys_role_menu` VALUES (105, 1063);
INSERT INTO `sys_role_menu` VALUES (105, 1064);
INSERT INTO `sys_role_menu` VALUES (105, 1065);
INSERT INTO `sys_role_menu` VALUES (105, 1066);
INSERT INTO `sys_role_menu` VALUES (105, 1067);
INSERT INTO `sys_role_menu` VALUES (105, 1068);
INSERT INTO `sys_role_menu` VALUES (105, 1069);
INSERT INTO `sys_role_menu` VALUES (105, 1070);
INSERT INTO `sys_role_menu` VALUES (105, 1071);
INSERT INTO `sys_role_menu` VALUES (105, 1072);
INSERT INTO `sys_role_menu` VALUES (105, 1073);
INSERT INTO `sys_role_menu` VALUES (105, 1074);
INSERT INTO `sys_role_menu` VALUES (105, 1075);
INSERT INTO `sys_role_menu` VALUES (105, 1076);
INSERT INTO `sys_role_menu` VALUES (105, 1077);
INSERT INTO `sys_role_menu` VALUES (105, 1078);
INSERT INTO `sys_role_menu` VALUES (105, 1079);
INSERT INTO `sys_role_menu` VALUES (105, 1080);
INSERT INTO `sys_role_menu` VALUES (105, 1087);
INSERT INTO `sys_role_menu` VALUES (105, 1088);
INSERT INTO `sys_role_menu` VALUES (105, 1089);
INSERT INTO `sys_role_menu` VALUES (105, 1090);
INSERT INTO `sys_role_menu` VALUES (105, 1091);
INSERT INTO `sys_role_menu` VALUES (105, 1092);
INSERT INTO `sys_role_menu` VALUES (105, 1093);
INSERT INTO `sys_role_menu` VALUES (105, 1094);
INSERT INTO `sys_role_menu` VALUES (105, 1095);
INSERT INTO `sys_role_menu` VALUES (105, 1096);
INSERT INTO `sys_role_menu` VALUES (105, 1097);
INSERT INTO `sys_role_menu` VALUES (105, 1098);
INSERT INTO `sys_role_menu` VALUES (105, 1099);
INSERT INTO `sys_role_menu` VALUES (105, 1100);
INSERT INTO `sys_role_menu` VALUES (105, 1101);
INSERT INTO `sys_role_menu` VALUES (105, 1102);
INSERT INTO `sys_role_menu` VALUES (105, 1103);
INSERT INTO `sys_role_menu` VALUES (105, 1104);
INSERT INTO `sys_role_menu` VALUES (105, 1105);
INSERT INTO `sys_role_menu` VALUES (105, 1106);
INSERT INTO `sys_role_menu` VALUES (105, 1107);
INSERT INTO `sys_role_menu` VALUES (105, 1108);
INSERT INTO `sys_role_menu` VALUES (105, 1109);
INSERT INTO `sys_role_menu` VALUES (105, 1110);
INSERT INTO `sys_role_menu` VALUES (105, 1111);
INSERT INTO `sys_role_menu` VALUES (105, 1112);
INSERT INTO `sys_role_menu` VALUES (105, 1113);
INSERT INTO `sys_role_menu` VALUES (105, 1114);
INSERT INTO `sys_role_menu` VALUES (105, 1115);
INSERT INTO `sys_role_menu` VALUES (105, 1116);
INSERT INTO `sys_role_menu` VALUES (105, 1117);
INSERT INTO `sys_role_menu` VALUES (105, 1118);
INSERT INTO `sys_role_menu` VALUES (105, 1119);
INSERT INTO `sys_role_menu` VALUES (105, 1120);
INSERT INTO `sys_role_menu` VALUES (105, 1121);
INSERT INTO `sys_role_menu` VALUES (105, 1122);
INSERT INTO `sys_role_menu` VALUES (105, 1123);
INSERT INTO `sys_role_menu` VALUES (105, 1124);
INSERT INTO `sys_role_menu` VALUES (105, 1125);
INSERT INTO `sys_role_menu` VALUES (105, 1126);
INSERT INTO `sys_role_menu` VALUES (105, 1127);
INSERT INTO `sys_role_menu` VALUES (105, 1128);
INSERT INTO `sys_role_menu` VALUES (105, 1129);
INSERT INTO `sys_role_menu` VALUES (105, 1130);
INSERT INTO `sys_role_menu` VALUES (105, 1131);
INSERT INTO `sys_role_menu` VALUES (105, 1132);
INSERT INTO `sys_role_menu` VALUES (105, 1133);
INSERT INTO `sys_role_menu` VALUES (105, 1134);
INSERT INTO `sys_role_menu` VALUES (105, 1135);
INSERT INTO `sys_role_menu` VALUES (105, 1136);
INSERT INTO `sys_role_menu` VALUES (105, 1137);
INSERT INTO `sys_role_menu` VALUES (105, 1138);
INSERT INTO `sys_role_menu` VALUES (105, 1139);
INSERT INTO `sys_role_menu` VALUES (105, 1140);
INSERT INTO `sys_role_menu` VALUES (105, 1141);
INSERT INTO `sys_role_menu` VALUES (105, 1142);
INSERT INTO `sys_role_menu` VALUES (105, 1143);
INSERT INTO `sys_role_menu` VALUES (105, 1144);
INSERT INTO `sys_role_menu` VALUES (105, 1145);
INSERT INTO `sys_role_menu` VALUES (105, 1146);
INSERT INTO `sys_role_menu` VALUES (106, 100);
INSERT INTO `sys_role_menu` VALUES (106, 107);
INSERT INTO `sys_role_menu` VALUES (106, 1000);
INSERT INTO `sys_role_menu` VALUES (106, 1001);
INSERT INTO `sys_role_menu` VALUES (106, 1002);
INSERT INTO `sys_role_menu` VALUES (106, 1003);
INSERT INTO `sys_role_menu` VALUES (106, 1005);
INSERT INTO `sys_role_menu` VALUES (106, 1006);
INSERT INTO `sys_role_menu` VALUES (106, 1035);
INSERT INTO `sys_role_menu` VALUES (106, 1036);
INSERT INTO `sys_role_menu` VALUES (106, 1037);
INSERT INTO `sys_role_menu` VALUES (106, 1038);
INSERT INTO `sys_role_menu` VALUES (106, 1062);
INSERT INTO `sys_role_menu` VALUES (106, 1069);
INSERT INTO `sys_role_menu` VALUES (106, 1070);
INSERT INTO `sys_role_menu` VALUES (106, 1071);
INSERT INTO `sys_role_menu` VALUES (106, 1072);
INSERT INTO `sys_role_menu` VALUES (106, 1073);
INSERT INTO `sys_role_menu` VALUES (106, 1075);
INSERT INTO `sys_role_menu` VALUES (106, 1076);
INSERT INTO `sys_role_menu` VALUES (106, 1077);
INSERT INTO `sys_role_menu` VALUES (106, 1078);
INSERT INTO `sys_role_menu` VALUES (106, 1079);
INSERT INTO `sys_role_menu` VALUES (106, 1087);
INSERT INTO `sys_role_menu` VALUES (106, 1088);
INSERT INTO `sys_role_menu` VALUES (106, 1089);
INSERT INTO `sys_role_menu` VALUES (106, 1090);
INSERT INTO `sys_role_menu` VALUES (106, 1091);
INSERT INTO `sys_role_menu` VALUES (106, 1093);
INSERT INTO `sys_role_menu` VALUES (106, 1094);
INSERT INTO `sys_role_menu` VALUES (106, 1095);
INSERT INTO `sys_role_menu` VALUES (106, 1096);
INSERT INTO `sys_role_menu` VALUES (106, 1097);
INSERT INTO `sys_role_menu` VALUES (106, 1099);
INSERT INTO `sys_role_menu` VALUES (106, 1100);
INSERT INTO `sys_role_menu` VALUES (106, 1102);
INSERT INTO `sys_role_menu` VALUES (106, 1103);
INSERT INTO `sys_role_menu` VALUES (106, 1105);
INSERT INTO `sys_role_menu` VALUES (106, 1106);
INSERT INTO `sys_role_menu` VALUES (106, 1108);
INSERT INTO `sys_role_menu` VALUES (106, 1109);
INSERT INTO `sys_role_menu` VALUES (106, 1111);
INSERT INTO `sys_role_menu` VALUES (106, 1112);
INSERT INTO `sys_role_menu` VALUES (106, 1113);
INSERT INTO `sys_role_menu` VALUES (106, 1114);
INSERT INTO `sys_role_menu` VALUES (106, 1115);
INSERT INTO `sys_role_menu` VALUES (106, 1117);
INSERT INTO `sys_role_menu` VALUES (106, 1118);
INSERT INTO `sys_role_menu` VALUES (106, 1119);
INSERT INTO `sys_role_menu` VALUES (106, 1120);
INSERT INTO `sys_role_menu` VALUES (106, 1121);
INSERT INTO `sys_role_menu` VALUES (106, 1123);
INSERT INTO `sys_role_menu` VALUES (106, 1124);
INSERT INTO `sys_role_menu` VALUES (106, 1126);
INSERT INTO `sys_role_menu` VALUES (106, 1127);
INSERT INTO `sys_role_menu` VALUES (106, 1129);
INSERT INTO `sys_role_menu` VALUES (106, 1130);
INSERT INTO `sys_role_menu` VALUES (106, 1131);
INSERT INTO `sys_role_menu` VALUES (106, 1132);
INSERT INTO `sys_role_menu` VALUES (106, 1133);
INSERT INTO `sys_role_menu` VALUES (106, 1135);
INSERT INTO `sys_role_menu` VALUES (106, 1136);
INSERT INTO `sys_role_menu` VALUES (106, 1138);
INSERT INTO `sys_role_menu` VALUES (106, 1141);
INSERT INTO `sys_role_menu` VALUES (106, 1142);
INSERT INTO `sys_role_menu` VALUES (106, 1144);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '李想', '00', 'ry@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2020-05-14 21:23:53', 'admin', '2018-03-16 11:33:00', 'ry', '2020-05-14 21:23:52', '管理员');
INSERT INTO `sys_user` VALUES (2, 107, 'ry', '李现', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'admin', '2020-04-19 09:33:12', '测试员');
INSERT INTO `sys_user` VALUES (100, 103, 'courseadmin', '张无忌', '00', 'courseadmin@163.com', '13106547896', '0', '/profile/avatar/2020/04/19/42b25c5c7a94a2e766a0aafe28c9ca2e.png', 'e7ecce67517938c32f6b5bdcfb8e2819', 'd34ad9', '0', '0', '127.0.0.1', '2020-05-14 21:44:18', 'admin', '2020-04-19 06:39:49', 'admin', '2020-05-14 21:44:17', '管理员用户信息');
INSERT INTO `sys_user` VALUES (101, 107, 'zhangyang', '张洋', '00', 'zhangyang@163.com', '13152147852', '0', '', 'e9c297a70a09c87eff6b0772e5a0e37c', '781db7', '0', '0', '', NULL, 'admin', '2020-04-19 18:09:22', 'courseadmin', '2020-04-20 10:47:28', '');
INSERT INTO `sys_user` VALUES (102, 107, 'lilonglong', '李龙龙', '00', 'lilonglong@163.com', '18514784587', '0', '', '25e28cbdb405ec294e039e1b2ce7a082', '8c5032', '0', '0', '127.0.0.1', '2020-04-22 20:09:45', 'admin', '2020-04-19 22:00:35', 'courseadmin', '2020-04-22 20:09:44', '');
INSERT INTO `sys_user` VALUES (103, 107, 'zhangxiaorong', '张肖荣', '00', 'zhangxiaor@163.com', '18745214578', '1', '', '9135e616da4c2d31d8664b7cec257674', '2da948', '0', '0', '', NULL, 'admin', '2020-04-19 22:02:26', 'admin', '2020-04-19 22:06:53', '辅助学员信息的 管理');
INSERT INTO `sys_user` VALUES (104, 104, 'yanqian', '杨倩', '00', 'yangqian@163.com', '15632145789', '1', '', 'e5b9ab2e8b3e19a4dc1fb9a9d43783ba', '84d0f3', '0', '0', '', NULL, 'admin', '2020-04-19 22:03:35', 'courseadmin', '2020-05-14 21:09:51', '教师介绍Angel老师 英语专业学士,英语专业八级。从事少儿英语教育数年,有丰富的教学经验。性格开朗活泼,语感和表达能力较强。富有亲和力、耐心及责任感,课堂气氛活跃,...');

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('59bf3fd1-6097-4d65-acb0-02dcd265f478', 'courseadmin', '研发部门', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', 'on_line', '2020-05-14 21:25:28', '2020-05-14 21:49:14', 1800000);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);
INSERT INTO `sys_user_post` VALUES (100, 2);
INSERT INTO `sys_user_post` VALUES (101, 5);
INSERT INTO `sys_user_post` VALUES (101, 7);
INSERT INTO `sys_user_post` VALUES (102, 5);
INSERT INTO `sys_user_post` VALUES (103, 7);
INSERT INTO `sys_user_post` VALUES (104, 4);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (100, 100);
INSERT INTO `sys_user_role` VALUES (100, 106);
INSERT INTO `sys_user_role` VALUES (101, 101);
INSERT INTO `sys_user_role` VALUES (102, 101);
INSERT INTO `sys_user_role` VALUES (103, 102);
INSERT INTO `sys_user_role` VALUES (104, 101);

-- ----------------------------
-- Table structure for wct_class_course_info
-- ----------------------------
DROP TABLE IF EXISTS `wct_class_course_info`;
CREATE TABLE `wct_class_course_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `class_id` int(11) NULL DEFAULT NULL COMMENT '班级编号',
  `class_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级名称',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程编号',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级课程信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_class_course_info
-- ----------------------------
INSERT INTO `wct_class_course_info` VALUES (1, 1, '画画一班', 1, '积木初级教学', NULL);
INSERT INTO `wct_class_course_info` VALUES (2, 1, '画画一班', 4, '积木3', NULL);
INSERT INTO `wct_class_course_info` VALUES (3, 3, '儿童积木', 1, '积木初级教学', NULL);
INSERT INTO `wct_class_course_info` VALUES (4, 3, '儿童积木', 2, '积木2教程', NULL);
INSERT INTO `wct_class_course_info` VALUES (5, 3, '儿童积木', 4, '积木3', NULL);

-- ----------------------------
-- Table structure for wct_class_info
-- ----------------------------
DROP TABLE IF EXISTS `wct_class_info`;
CREATE TABLE `wct_class_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编辑名称',
  `introduce` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级描述',
  `teacher_id` int(11) NULL DEFAULT NULL COMMENT '辅导员编号',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级辅导员老师',
  `create_time` date NULL DEFAULT NULL COMMENT '班级开设时间',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '班级状态(1正常，2毕业,3停止）',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `class_name_ix`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_class_info
-- ----------------------------
INSERT INTO `wct_class_info` VALUES (1, '画画一班', '完成素描等基础知识', 101, '张洋', '2020-04-01', 1);
INSERT INTO `wct_class_info` VALUES (3, '儿童积木', '开发儿童智力启发课程', 103, '张肖荣', '2020-04-20', 2);
INSERT INTO `wct_class_info` VALUES (4, '少儿智力开发优秀班', '提高儿童智力水平，赢在起跑线~', 103, '张肖荣', '2020-04-20', 3);
INSERT INTO `wct_class_info` VALUES (5, '秒秒班', '3岁以下儿童的托管课程', 101, '张洋', '2020-04-20', 3);
INSERT INTO `wct_class_info` VALUES (6, '舞蹈一班', '舞蹈教育', 103, '张肖荣', '2020-04-22', 1);

-- ----------------------------
-- Table structure for wct_course
-- ----------------------------
DROP TABLE IF EXISTS `wct_course`;
CREATE TABLE `wct_course`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '课程介绍',
  `picture` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程图片',
  `type_id` int(11) NULL DEFAULT NULL COMMENT '课程分类编号',
  `type_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程分类名称',
  `price` double NULL DEFAULT NULL COMMENT '课程价格',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '课程状态(1正常 2听课 3删除）',
  `publish_time` date NULL DEFAULT NULL COMMENT '发布时间',
  `teacher_id` int(11) NULL DEFAULT NULL COMMENT '课程主讲老师编号',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程主讲老师',
  `create_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_course
-- ----------------------------
INSERT INTO `wct_course` VALUES (1, '积木初级教学', '培养孩子智力，提高儿童动手能力', '/profile/upload/2020/04/20/ddc34ac8e4548d7003e75dba555de253.png', 1, '积木', 380, 2, '2020-04-08', 101, '张洋', '张无忌', '2020-04-20 11:16:16', NULL, NULL);
INSERT INTO `wct_course` VALUES (2, '积木2教程', '积木2教程是针对少年学习的课程', '/profile/upload/2020/04/20/ddc34ac8e4548d7003e75dba555de253.png', 1, '积木', 168, 2, '2020-04-08', 102, '李龙龙', '张无忌', '2020-04-20 11:23:40', '张无忌', '2020-04-20 11:49:33');
INSERT INTO `wct_course` VALUES (3, '积木学习', '积木的技术搭建等学习', '/profile/upload/2020/04/20/ddc34ac8e4548d7003e75dba555de253.png', 5, '其他', 156, 2, '2020-04-06', 102, '李龙龙', '张无忌', '2020-04-20 11:26:35', '张无忌', '2020-04-20 11:56:51');
INSERT INTO `wct_course` VALUES (4, '积木3', '积木3', '/profile/upload/2020/04/20/ddc34ac8e4548d7003e75dba555de253.png', 1, '积木', 159, 1, '2020-04-09', 101, '张洋', '张无忌', '2020-04-20 11:27:14', NULL, NULL);
INSERT INTO `wct_course` VALUES (5, '4', '4', '/profile/upload/2020/04/20/d7b2cd9cad6e11f6c4fe6ca72eca3164.png', 1, '积木', 60, 2, '2020-04-15', 101, '张洋', '张无忌', '2020-04-20 11:28:56', NULL, NULL);
INSERT INTO `wct_course` VALUES (6, '课程1', '课程1测试...', '/profile/upload/2020/04/22/2692060788ca725b169c4458b8a1096a.jfif', 2, '少儿动画教育', 180, 1, '2020-04-08', 101, '张洋', '张无忌', '2020-04-22 21:05:26', NULL, NULL);
INSERT INTO `wct_course` VALUES (9, '机器人', '点点滴滴', '/profile/upload/2020/05/14/674ce47b6b5cd43f3a7b9dc847b3212b.jpg', 2, '少儿动画教育', 180, 1, '2020-04-08', 101, '张洋', '张无忌', '2020-05-14 21:01:27', NULL, NULL);
INSERT INTO `wct_course` VALUES (10, '少儿舞蹈', '少儿舞蹈的教学', NULL, 2, '少儿动画教育', 500, 2, '2020-05-29', 101, '张洋', '张无忌', '2020-05-14 21:02:32', NULL, NULL);
INSERT INTO `wct_course` VALUES (11, '少儿舞蹈', '活跃少儿的性格', '/profile/upload/2020/05/14/fbbcaae69cbb22bada17ef3b782dcc37.jpg', 2, '少儿动画教育', 500, 1, '2020-05-28', 101, '张洋', '张无忌', '2020-05-14 21:03:34', NULL, NULL);

-- ----------------------------
-- Table structure for wct_course_apply
-- ----------------------------
DROP TABLE IF EXISTS `wct_course_apply`;
CREATE TABLE `wct_course_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `student_id` int(11) NULL DEFAULT NULL COMMENT '学员编号',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学员姓名',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学员电话',
  `class_id` int(11) NULL DEFAULT NULL COMMENT '班级编号',
  `class_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级名称',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程编号',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `price` double NULL DEFAULT NULL COMMENT '报名的价格',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '报名时间',
  `course_time` date NULL DEFAULT NULL COMMENT '预计上课时间',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '支付状态(1待支付 2 支付成功 3退款)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程报名号信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_course_apply
-- ----------------------------
INSERT INTO `wct_course_apply` VALUES (1, 1, '李明', '13514587421', 1, '画画一班', 1, '积木教程', 180, '2020-04-23 20:23:32', '2020-04-23', 1);

-- ----------------------------
-- Table structure for wct_course_appointment
-- ----------------------------
DROP TABLE IF EXISTS `wct_course_appointment`;
CREATE TABLE `wct_course_appointment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程编号',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `student_id` int(11) NULL DEFAULT NULL COMMENT '学员编号',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学员名称',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态（1.预约成功 2取消预约 3.删除预约）',
  `course_time` date NULL DEFAULT NULL COMMENT '预约上课时间',
  `actual_course_time` date NULL DEFAULT NULL COMMENT '实际上课时间',
  `course_status` tinyint(4) NULL DEFAULT NULL COMMENT '上课状态(1未上课 2已上课)',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上课老师',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上课地点',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `type` tinyint(255) NULL DEFAULT NULL COMMENT '1是精品课程 2是普通课程',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程预约信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_course_appointment
-- ----------------------------
INSERT INTO `wct_course_appointment` VALUES (1, 1, '积木教程', 1, '李明', 1, '2020-04-29', '2020-04-30', NULL, '张明', '北京市朝阳区金融广场', '2020-04-23 20:24:53', 2);
INSERT INTO `wct_course_appointment` VALUES (2, 2, '积木2教程', 3, 's1', 1, NULL, NULL, 1, NULL, NULL, '2020-05-13 22:53:57', 2);
INSERT INTO `wct_course_appointment` VALUES (3, 2, '积木2教程', 3, 's1', 1, NULL, NULL, 1, NULL, NULL, '2020-05-13 22:54:58', 2);
INSERT INTO `wct_course_appointment` VALUES (4, 2, '积木2教程', 3, 's1', 2, NULL, NULL, 1, NULL, NULL, '2020-05-13 22:55:00', 2);
INSERT INTO `wct_course_appointment` VALUES (5, 2, '积木2教程', 3, 's1', 2, NULL, NULL, 1, NULL, NULL, '2020-05-13 22:55:01', 2);
INSERT INTO `wct_course_appointment` VALUES (6, 2, '积木2教程', 3, 's1', 1, NULL, NULL, 1, NULL, NULL, '2020-05-13 22:55:03', 2);
INSERT INTO `wct_course_appointment` VALUES (7, 1, '积木教学', 4, 'BI', 2, '2020-05-16', NULL, 1, NULL, NULL, '2020-05-14 20:50:37', 1);
INSERT INTO `wct_course_appointment` VALUES (8, 3, '绘画课程', 4, 'BI', 1, '2020-05-16', NULL, 1, NULL, NULL, '2020-05-14 21:19:00', 1);

-- ----------------------------
-- Table structure for wct_course_evaluation
-- ----------------------------
DROP TABLE IF EXISTS `wct_course_evaluation`;
CREATE TABLE `wct_course_evaluation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `course_code` int(11) NULL DEFAULT NULL COMMENT '课程编号',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授课教师',
  `score` int(11) NULL DEFAULT NULL COMMENT '评价分数',
  `student_id` int(11) NULL DEFAULT NULL COMMENT '学员编号',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学员姓名',
  `evaluation_time` timestamp(0) NULL DEFAULT NULL COMMENT '评价时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程评价信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_course_evaluation
-- ----------------------------
INSERT INTO `wct_course_evaluation` VALUES (1, 1, '积木教学', '张洋', 5, 1, '张明', '2020-04-01 00:00:00');

-- ----------------------------
-- Table structure for wct_course_type
-- ----------------------------
DROP TABLE IF EXISTS `wct_course_type`;
CREATE TABLE `wct_course_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程类型名称',
  `create_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `course_type_name_index`(`type_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程类别信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_course_type
-- ----------------------------
INSERT INTO `wct_course_type` VALUES (1, '积木', 'courseadmin', '2020-04-19 22:41:16', NULL, NULL);
INSERT INTO `wct_course_type` VALUES (2, '少儿动画教育', 'courseadmin', '2020-04-19 22:41:35', 'courseadmin', '2020-04-19 22:42:30');
INSERT INTO `wct_course_type` VALUES (3, '绘画', 'courseadmin', '2020-04-19 22:41:50', NULL, NULL);
INSERT INTO `wct_course_type` VALUES (4, '编程', 'courseadmin', '2020-04-19 22:41:59', NULL, NULL);
INSERT INTO `wct_course_type` VALUES (5, '其他', 'courseadmin', '2020-04-19 22:42:04', NULL, NULL);

-- ----------------------------
-- Table structure for wct_discussion
-- ----------------------------
DROP TABLE IF EXISTS `wct_discussion`;
CREATE TABLE `wct_discussion`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '讨论的标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '讨论的内容',
  `user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发布人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '讨论信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_discussion
-- ----------------------------
INSERT INTO `wct_discussion` VALUES (1, '画画课程非常的不错', '今天老师讲的画画的课程非常好,感觉收货非常多', '张洋', '2020-04-20 08:31:19');

-- ----------------------------
-- Table structure for wct_excellent_course
-- ----------------------------
DROP TABLE IF EXISTS `wct_excellent_course`;
CREATE TABLE `wct_excellent_course`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '精品课程名称',
  `picture` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程图片',
  `video` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程视频',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '课程简介',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '课程状态（1正常 2停课 3失效）',
  `price` double NULL DEFAULT NULL COMMENT '课程价格',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程老师',
  `create_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '精品课程信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_excellent_course
-- ----------------------------
INSERT INTO `wct_excellent_course` VALUES (1, '积木教学', '/profile/upload/2020/04/21/8c85cd5df6f1d281681d01fb088abd59.jfif', '/profile/upload/2020/04/21/0184dffc882d29e44b12b51d1815600e.mkv', '精品优秀的课程，值得拥有~', 1, 650, '张三丰', '张无忌', '2020-04-21 14:42:58');
INSERT INTO `wct_excellent_course` VALUES (2, '机器人精品课程', '/profile/upload/2020/05/14/1dfaad433b66c0a5760e85d27022bf2d.jpg', NULL, '精品课程的结束...', 1, 500, '李方', '张无忌', '2020-05-14 21:07:04');
INSERT INTO `wct_excellent_course` VALUES (3, '绘画课程', '/profile/upload/2020/05/14/1062d5fe4afd9610e27914ac843fb168.jpg', NULL, '精品绘画课程', 1, 800, '李广', '张无忌', '2020-05-14 21:08:04');
INSERT INTO `wct_excellent_course` VALUES (4, '精品积木课程', '/profile/upload/2020/05/14/96253a7477b432d81a17bf8caa569a6d.jpg', NULL, '精品积木课程', 1, 800, '李方', '张无忌', '2020-05-14 21:08:44');

-- ----------------------------
-- Table structure for wct_message
-- ----------------------------
DROP TABLE IF EXISTS `wct_message`;
CREATE TABLE `wct_message`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '留言信内容',
  `student_id` int(11) NULL DEFAULT NULL COMMENT '学员编号',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学员姓名',
  `message_object` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '留言对象',
  `message_time` timestamp(0) NULL DEFAULT NULL COMMENT '留言时间',
  `back_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '留言回复信息',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回复留言老师',
  `back_time` timestamp(0) NULL DEFAULT NULL COMMENT '回复时间',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '留言状态(1.未读2.已读3.已回复4.删除)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学员留言信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_message
-- ----------------------------
INSERT INTO `wct_message` VALUES (1, '今天是否上课', 1, '张明', '系统管理员', '2020-03-04 00:00:00', 'eeeeee', '张无忌', '2020-04-22 21:12:14', 3);
INSERT INTO `wct_message` VALUES (2, '画画课程有点听不懂', 1, '张明', '肖伟老师', '2020-04-06 15:34:58', '后续会再讲解一次', '张无忌', '2020-04-21 15:36:07', 3);
INSERT INTO `wct_message` VALUES (3, '测试留言', 2, '李广', '小五老师', '2020-04-07 15:35:03', '好的，感谢留言，稍后我们会关注并给予处理~', '张无忌', '2020-04-21 15:35:44', 3);
INSERT INTO `wct_message` VALUES (4, '今天学习状态不好', 2, '李想', '小五老师', '2020-04-21 15:36:58', '好好加油努力~~', '张无忌', '2020-04-21 15:37:17', 3);
INSERT INTO `wct_message` VALUES (5, '       今天好无聊~', 3, 's1', NULL, '2020-05-13 10:39:57', NULL, NULL, NULL, 1);
INSERT INTO `wct_message` VALUES (6, '      今天天气很热~', 3, 's1', NULL, '2020-05-13 10:44:40', NULL, NULL, NULL, 1);
INSERT INTO `wct_message` VALUES (7, '       今天天气好吗', 3, 's1', NULL, '2020-05-13 23:53:22', NULL, NULL, NULL, 1);
INSERT INTO `wct_message` VALUES (8, '       今天的天气很好~', 4, 'BI', NULL, '2020-05-14 20:51:30', '继续加油~', '张无忌', '2020-05-14 20:52:34', 3);
INSERT INTO `wct_message` VALUES (9, '       留言信息2', 4, 'BI', NULL, '2020-05-14 20:51:41', NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for wct_person_course_info
-- ----------------------------
DROP TABLE IF EXISTS `wct_person_course_info`;
CREATE TABLE `wct_person_course_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `student_code` int(11) NULL DEFAULT NULL COMMENT '学生编号',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学生名称',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程编号',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '课程状态(1正常 2听课 3失效)',
  `apply_time` timestamp(0) NULL DEFAULT NULL COMMENT '报名时间',
  `type` tinyint(255) NULL DEFAULT NULL COMMENT '1是精品课程2是普通课程',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '个人课程信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_person_course_info
-- ----------------------------
INSERT INTO `wct_person_course_info` VALUES (1, 3, 's1', 1, '积木教学', 1, '2020-05-13 22:53:24', 1);
INSERT INTO `wct_person_course_info` VALUES (4, 4, 'BI', 1, '积木初级教学', 1, '2020-05-14 20:50:08', 2);
INSERT INTO `wct_person_course_info` VALUES (5, 4, 'BI', 9, '机器人', 1, '2020-05-14 21:18:36', 2);

-- ----------------------------
-- Table structure for wct_student
-- ----------------------------
DROP TABLE IF EXISTS `wct_student`;
CREATE TABLE `wct_student`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `email` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮件',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `qq` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'QQ号',
  `wechat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信号',
  `sex` tinyint(4) NULL DEFAULT NULL COMMENT '性别',
  `img` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `birthday` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生日',
  `idcard` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `score` int(11) NULL DEFAULT NULL COMMENT '积分',
  `money` double NULL DEFAULT NULL COMMENT '账户余额',
  `class_id` int(11) NULL DEFAULT NULL COMMENT '班级编号',
  `class_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级名称',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态(1正常,2冻结,3.失效,0删除)',
  `register_time` timestamp(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学员信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_student
-- ----------------------------
INSERT INTO `wct_student` VALUES (1, '李明', 'liming', '123456', 'sadasd@163.com', '16541254784', '124456', '123456', 0, NULL, '1995-10-15', '1234567687984542523', '北京市西城区', 0, 0, 1, '画画一班', 1, '2020-04-22 09:00:49');
INSERT INTO `wct_student` VALUES (2, '张明', 'zhangming', '123456', 'zhangming@163.com', '15478541241', '12457874141', '12457874141', 0, '/profile/avatar/2020/04/22/e3875894e8d8354a4f584605f666733a.jfif', '1990-02-08', '133333333333', 'sdfasfasfasf', 0, 0, 1, '画画一班', 1, '2020-04-22 09:03:45');
INSERT INTO `wct_student` VALUES (3, 's1', 'huyang', '123456', 's1@163.com', '12345678', '111', '2222', 1, NULL, '2008-02-07', '12345678901234567', '北京市朝阳区', NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `wct_student` VALUES (4, 'BI', 'z1', '123456', 'z1@163.com', '12345678', '12321214', '12344545', 1, NULL, '2006-02-16', '12345678999', '河北省石家庄的撒到撒大事大叔大叔的', NULL, NULL, NULL, NULL, 1, NULL);

-- ----------------------------
-- Table structure for wct_student_fee
-- ----------------------------
DROP TABLE IF EXISTS `wct_student_fee`;
CREATE TABLE `wct_student_fee`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付方式',
  `money` double NULL DEFAULT NULL COMMENT '支付金额',
  `pay_time` timestamp(0) NULL DEFAULT NULL COMMENT '支付时间',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程编号',
  `course_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `student_id` int(11) NULL DEFAULT NULL COMMENT '学员编号',
  `student_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学员的名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学员缴费信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_student_fee
-- ----------------------------
INSERT INTO `wct_student_fee` VALUES (1, 'ZFB', 158, '2020-03-30 00:00:00', 1, '积木教程', 1, '李明');
INSERT INTO `wct_student_fee` VALUES (2, 'WX', 190, '2020-04-07 00:00:00', 2, '积木2', 1, '李明');

-- ----------------------------
-- Table structure for wct_teacher_style
-- ----------------------------
DROP TABLE IF EXISTS `wct_teacher_style`;
CREATE TABLE `wct_teacher_style`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `picture` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '风采的图片',
  `video` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '风采的视频',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '说明',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态（1正常 2失效 ）',
  `create_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '师生风采信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wct_teacher_style
-- ----------------------------
INSERT INTO `wct_teacher_style` VALUES (1, '优秀 学员风采', '/profile/upload/2020/04/24/e0de4def6198986e3e419e1ffec48b10.png', '/profile/upload/2020/04/24/7185b61b0ea3e3ad29a6d4635d432fea.mkv', '优秀学员的风采展示', 1, '若依', '2020-04-24 08:17:58', NULL, NULL);
INSERT INTO `wct_teacher_style` VALUES (2, '东海大道', '/profile/upload/2020/05/14/9ee5a174bee72cc0129cf4872433fd12.jpg', NULL, '教师介绍Angel老师 英语专业学士,英语专业八级。从事少儿英语教育数年,有丰富的教学经验。性格开朗活泼,语感和表达能力较强。富有亲和力、耐心及责任感,课堂气氛活跃,...', 1, '张无忌', '2020-05-14 21:12:15', NULL, NULL);
INSERT INTO `wct_teacher_style` VALUES (3, '一起参加演出活动', '/profile/upload/2020/05/14/c0d42ca462dfcb5586a17fac20e77b94.jpg', NULL, '大使大赛的发生发达萨法萨法萨法萨法是否萨法萨法发萨法萨法撒发阿发阿萨法分撒分撒阿萨法萨法萨法舒服', 1, '张无忌', '2020-05-14 21:44:48', NULL, NULL);
INSERT INTO `wct_teacher_style` VALUES (4, '郊外旅游图片', '/profile/upload/2020/05/14/8248e7b584852b0087a852df08c8bbe4.jpg', NULL, '去啊沙发舒服\r\n为个sgsagassa\r\nsfasfasfsafw\r\nfawfafasfasfas\r\n\r\nfasfafasffs', 1, '张无忌', '2020-05-14 21:48:44', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
